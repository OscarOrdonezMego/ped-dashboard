﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controller.functions
{
    public class util
    {
        public static string getStringFechaYYMMDD(string fecha)
        {
            string fechafinal;
            DateTime date;
            DateTime enteredDate = DateTime.Parse(fecha);
            date = enteredDate.Date;
            fechafinal = date.ToString("MM/dd/yyyy");
            return fechafinal;
        }
    }
}
