﻿using Model;
using Model.bean;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Controller
{
    public class EfectivoController
    {
        public static List<BarraGraficoItemBean> obtenerPedidosEfectivos(string fechaInicio, string fechaFin, string usuario)
        {
            List<BarraGraficoItemBean> list = new List<BarraGraficoItemBean>();
            DataTable dt = EfectivoModel.obtenerPedidosEfectivos(fechaInicio, fechaFin, usuario);

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    BarraGraficoItemBean bean = new BarraGraficoItemBean();
                    bean.name = row["nombre"].ToString();
                    bean.y = decimal.Parse(row["valor"].ToString());
                    list.Add(bean);
                }
            }

            return list;
        }

        public static List<BarraGraficoItemBean> obtenerPedidosEfectivosGrupo(string fechaInicio, string fechaFin, string grupo)
        {
            List<BarraGraficoItemBean> list = new List<BarraGraficoItemBean>();
            DataTable dt = EfectivoModel.obtenerPedidosEfectivosGrupo(fechaInicio, fechaFin, grupo);

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    BarraGraficoItemBean bean = new BarraGraficoItemBean();
                    bean.name = row["nombre"].ToString();
                    bean.y = decimal.Parse(row["valor"].ToString());
                    list.Add(bean);
                }
            }

            return list;
        }

        public static DataTable obtenerPedidosEfectivosGrupoXVendedor(string fechaInicio, string fechaFin, string grupo)
        {
            return EfectivoModel.obtenerPedidosEfectivosGrupoXVendedor(fechaInicio, fechaFin, grupo); ;
        }
    }
}
