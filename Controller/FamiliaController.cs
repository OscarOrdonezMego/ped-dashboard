﻿using Model;
using Model.bean;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Controller
{
    public class FamiliaController
    {
        public static List<PieBeanItem> obtenerFamilias(string fechaInicio, string fechaFin, string tipo, string usuario, string moneda)
        {
            List<PieBeanItem> list = new List<PieBeanItem>();
            DataTable dt = FamiliaModel.obtenerReporteFamilia(fechaInicio, fechaFin, tipo, usuario, moneda);
            DataTable dtv;
            String simbolomoneda = "";
            if (tipo == "M")
            {
                if (moneda == "S")
                {
                    simbolomoneda = "S/.";
                }
                else
                {
                    simbolomoneda = "$ ";
                }
            }
            else
            {
                simbolomoneda = "";
            }
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    PieBeanItem bean = new PieBeanItem();
                    bean.ultimastransacciones = new List<PieBeanUltimasTransaccionesFecha>();
                    bean.name = row["FAMILIA"].ToString() + " (" + simbolomoneda + "" + row["MONTOCANTIDAD"].ToString() + ")";
                    bean.y = Convert.ToDecimal(row["PORCENTAJE"]);
                    bean.montocantidadtotal = float.Parse((row["MONTOCANTIDAD"].ToString()));
                    bean.pk = row["pk"].ToString();
                    dtv = FamiliaModel.obtenerUltimasVentas(bean.pk, fechaInicio,fechaFin, tipo, moneda);
                    foreach (DataRow rowv in dtv.Rows)
                    {
                        PieBeanUltimasTransaccionesFecha  beanv = new PieBeanUltimasTransaccionesFecha();
                        beanv.pk = rowv["pk"].ToString();
                        beanv.nombre = rowv["marca"].ToString();
                        beanv.valor = rowv["valormarca"].ToString();
                        bean.ultimastransacciones.Add(beanv);
                    }
                    list.Add(bean);
                }
            }

            return list;
        }
        public static List<PieBeanItem> obtenerTopFamiliaGrupo(string fechaInicio, string fechaFin, string tipo, string grupo, string moneda)
        {
            List<PieBeanItem> list = new List<PieBeanItem>();
            DataTable dt = ProductoModel.obtenerTopFamiliaGrupo(fechaInicio, fechaFin, tipo, grupo, moneda);
            DataTable dtv;
            String simbolomoneda = "";
            if (tipo == "M")
            {
                if (moneda == "S")
                {
                    simbolomoneda = "S/.";
                }
                else if (moneda == "D")
                {
                    simbolomoneda = "$ ";
                }
            }
            else
            {
                simbolomoneda = "";
            }
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    PieBeanItem bean = new PieBeanItem();
                    bean.ultimastransacciones = new List<PieBeanUltimasTransaccionesFecha>();
                    bean.name = row["FAMILIA"].ToString() + " (" + simbolomoneda + "" + row["MONTOCANTIDAD"].ToString() + ")";
                    bean.y = Convert.ToDecimal(row["PORCENTAJE"]);
                    bean.montocantidadtotal = float.Parse((row["MONTOCANTIDAD"].ToString()));
                    bean.pk = row["pk"].ToString();
                    dtv = FamiliaModel.obtenerUltimasVentasGrupo(bean.pk, fechaInicio, fechaFin, tipo, grupo, moneda);
                    foreach (DataRow rowv in dtv.Rows)
                    {
                        PieBeanUltimasTransaccionesFecha beanv = new PieBeanUltimasTransaccionesFecha();
                        beanv.pk = rowv["pk"].ToString();
                        beanv.nombre = rowv["marca"].ToString();
                        beanv.valor = rowv["valormarca"].ToString();
                        bean.ultimastransacciones.Add(beanv);
                    }
                    list.Add(bean);
                }
            }

            return list;
        }
        public static string dibujarReporteFamiliaGrupo(string fechaInicio, string fechaFin, string usuario, string perfil)
        {
            StringBuilder builder = new StringBuilder();
            //List<GrupoBean> list = UsuarioController.obtenerGrupos(usuario, perfil);

            //for (int i = 0; i < list.Count; i++)
            //{
            //    GrupoBean grupo = list[i];

            //    List<PieBeanItem> filasM = obtenerTopFamiliaGrupo(fechaInicio, fechaFin, "M", grupo.id);
            //    List<PieBeanItem> filasC = obtenerTopFamiliaGrupo(fechaInicio, fechaFin, "C", grupo.id);

            //    string nombreDivMonto = "divMonto" + i;
            //    string radioButtonMonto = "rbMonto" + i;
            //    string nombreDivCantidad = "divCantidad" + i;
            //    string radioButtoCantidad = "rbCantidad" + i;

            //    string strDiv = ReporteController.dibujarReporteFamiliaGrupo("Producto",grupo, filasM, filasC, nombreDivMonto, nombreDivCantidad, radioButtonMonto, radioButtoCantidad, "Monto total de Venta", "Cantidad de items",i);

            //    builder.Append(strDiv);
            //}


            return builder.ToString();
        }
        public static bool existeFamilia()
        {
            bool existe = false;
            DataTable dt = FamiliaModel.obtenerFamiliaexiste();
            if (dt != null && dt.Rows.Count > 0)
            {
                existe = true;
            }
                return existe;
        }
    }
}
