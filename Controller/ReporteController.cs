﻿using Model;
using Model.bean;
using System.Collections.Generic;
using System.Text;
using System;

namespace Controller
{
    public class ReporteController
    {

        public static string dibujarReporteGridGrupo(string nombre, GrupoBean grupo, List<FilaBean> filasM, List<FilaBean> filasC, string nombreDivMonto, string nombreDivCantidad, string radioButtonMonto, string radioButtoCantidad, string textMonto, string textCantidad)
        {
            StringBuilder builder = new StringBuilder();

            string strMonto = generarDivGridDinamico(nombre, grupo, filasM, nombreDivMonto, radioButtonMonto, true,textMonto,textCantidad);
            string strCantidad = generarDivGridDinamico(nombre, grupo, filasC, nombreDivCantidad, radioButtoCantidad, false,textMonto,textCantidad);
            string strScript = generarScripGridDinamico(nombreDivMonto, nombreDivCantidad,string.Empty, radioButtonMonto, radioButtoCantidad,string.Empty, false);

            builder.Append(strMonto);
            builder.Append(strCantidad);
            builder.Append(strScript);

            return builder.ToString();
        }
        public static string dibujarReporteFamiliaGrupo(string nombre, GrupoBean grupo, List<PieBeanItem> filasM, List<PieBeanItem> filasC, List<PieBeanItem> filasM_d, string nombreDivMonto, string nombreDivCantidad, string nombreDivMonto_d, string radioButtonMonto, string radioButtoCantidad, string radioButtonMonto_d, string textMonto, string textCantidad, int i)
        {
            StringBuilder builder = new StringBuilder();

            string strMonto = generarDivGridDinamicoFamilia(nombre, grupo, filasM, nombreDivMonto, radioButtonMonto, true, textMonto, textCantidad,i, "S");
            string strCantidad = generarDivGridDinamicoFamilia(nombre, grupo, filasC, nombreDivCantidad, radioButtoCantidad, false, textMonto, textCantidad, i, string.Empty);
            string strMonto_d = generarDivGridDinamicoFamilia(nombre, grupo, filasM_d, nombreDivMonto_d, radioButtonMonto_d, true, textMonto, textCantidad, i, "D");
            string strScript = generarScripGridDinamico(nombreDivMonto, nombreDivCantidad, nombreDivMonto_d, radioButtonMonto, radioButtoCantidad, radioButtonMonto_d, true);

            builder.Append(strMonto);
            builder.Append(strCantidad);
            builder.Append(strMonto_d);

            builder.Append(strScript);

            return builder.ToString();
        }

        public static string generarDivDinamicoTiempoPromedio(string id,GrupoBean grupo, int i)
        {
            StringBuilder builder = new StringBuilder();
            StringBuilder data = new StringBuilder();


            builder.Append("<div  class='cz-form-content cz-content-extended' style='height: 360px; width: 350px'>");
            builder.Append("<div class='cz-form-subcontent' style='background-color: white'>");
            builder.Append("<div class='cz-form-subcontent-title'><p runat='server' style='text-align: center; font-size: 15px'>" + grupo.nombre + "</p></div>");
             builder.Append("<div class='container' id='container" + i + "' style='width: 150px; height: 250px; margin: 0 auto'</div>");
            builder.Append("</div>");
            builder.Append("<div align='center' style='height: 250px;'>");
            builder.Append("<div style='width: 100%; float: right; background-color: white'>");
            builder.Append("</div>");
            builder.Append("<input type='button' id='bDetPromedio' class='cz-form-content-input-button form-button' onclick='irGrilla(" + id + ")'");
            builder.Append("value='Detalle' />");
            builder.Append("</div>");
            builder.Append("</div>");
            builder.Append("</div>");


            return builder.ToString();
        }

        public static string dibujarReportePromedioHighchart(int i, string tiempo)
        {
            String min = "";
            StringBuilder builder = new StringBuilder();
            int time = Convert.ToInt32(tiempo);
            if(time == 1)
            {
                min = "minuto";
            }else if(time > 1)
            {
                min = "minutos";
            }
            builder.Append("$(function(){var gaugeOptions={chart:{type:'solidgauge'},title:{text:null,},subtitle:{text:'Tiempo referencial de  20  minutos'},pane:{center:['50%','50%'],size:'100%',startAngle:0,endAngle:360,background:{backgroundColor:(Highcharts.theme && Highcharts.theme.background2) || '#EEE',innerRadius:'60%',outerRadius:'100%',shape:'arc',borderWidth:3,}},tooltip:{enabled:true},yAxis:{stops:[[0.1,'#55BF3B'],[0.5,'#DDDF0D'],[0.9,'#DF5353']],lineWidth:0,minorTickInterval:null,tickPixelInterval:400,tickWidth:0,title:{y:-70},labels:{enabled:false}},plotOptions:{solidgauge:{dataLabels:{y:-10,borderWidth:0,useHTML:false}}}};$('#container"+i+ "').highcharts(Highcharts.merge(gaugeOptions,{yAxis:{min:0,max:20,title:{text:''}},credits:{enabled:false},series:[{name:'Promedio ',data:["+ time + "],dataLabels:{format:'<div style=\"text-align:center\"><span style=\"font-size:25px;color:'+((Highcharts.theme && Highcharts.theme.contrastTextColor)) +'\">{y}</span><br/></div>'},tooltip:{valueSuffix:'"+min+"'}}]}));});");
            return builder.ToString();
        }

        public static string dibujarReporteColumnGrupo(string nombre, GrupoBean grupo, List<FilaBean> filasM, List<FilaBean> filasC, string nombreDivMonto, string nombreDivCantidad, string radioButtonMonto, string radioButtoCantidad, string textMonto, string textCantidad, int i)
        {
            StringBuilder builder = new StringBuilder();

            string strMonto = generarDivGridDinamicoColumnHighChart(nombre, grupo, filasM, nombreDivMonto, radioButtonMonto, true, textMonto, textCantidad, i);
            string strCantidad = generarDivGridDinamicoColumnHighChart(nombre, grupo, filasC, nombreDivCantidad, radioButtoCantidad, false, textMonto, textCantidad, i);
            string strScript = generarScripGridDinamico(nombreDivMonto, nombreDivCantidad , string.Empty, radioButtonMonto, radioButtoCantidad , string.Empty, false);

            builder.Append(strMonto);
            builder.Append(strCantidad);
            builder.Append(strScript);

            return builder.ToString();
        }
        public static string generarDivGridDinamico(string nombre, GrupoBean grupo, List<FilaBean> filas, string nombreDiv, string radioButton, bool isMonto, string textMonto , string textCantidad)
        {
            StringBuilder builder = new StringBuilder();

            builder.Append("<div id='" + nombreDiv + "' class='cz-form-content cz-content-extended' style='height: 360px; width: 350px");

            if (!isMonto)
            {
                builder.Append(";display:none");
            }


            builder.Append("'>");
            builder.Append("<div class='cz-form-subcontent' style='background-color: white'>");
            builder.Append("<div class='cz-form-subcontent-title'><p runat='server' style='text-align: center; font-size: 15px'>" + grupo.nombre + "</p></div>");

            builder.Append("<div align='center' style='height: 250px'>");

            if (filas.Count > 0)
            {
                builder.Append("<table class='grilla table table-bordered table-striped' style='overflow:auto'>");
                builder.Append("<tbody>");
                builder.Append("<tr>");
                builder.Append("<th>" + nombre + "</th>");
                if (isMonto)
                {
                    builder.Append("<th>Monto</th>");
                }
                else
                {
                    builder.Append("<th>Cantidad</th>");
                }

                builder.Append("</tr>");

                foreach (FilaBean fila in filas)
                {
                    builder.Append("<tr>");
                    builder.Append("<td align=\"center\">" + fila.nombre + "</td>");
                    builder.Append("<td align=\"center\">" + fila.valor + "</td>");
                    builder.Append("</tr>");
                }

                builder.Append("</tbody>");
                builder.Append("</table>");
            }
            else
            {
                builder.Append("<div class='gridNoData'>" +
                               "<p style='color:red'>No se encontraron datos para mostrar</p>" +
                               "</div>");
            }
            builder.Append("</div>");

            builder.Append("<div style='width: 50%; float: left; background-color: white'>");
            builder.Append("<table id='" + radioButton + "' border='0'>");
            builder.Append("<tbody><tr>");
            builder.Append("<td><input id='" + radioButton + "_0' type='radio' name='" + radioButton + "' value='M' checked='checked'><label for='" + radioButton + "_0'>"+textMonto+"</label></td>");
            builder.Append("</tr><tr>");
            builder.Append("<td><input id='" + radioButton + "_1' type='radio' name='" + radioButton + "' value='C'><label for='" + radioButton + "_1'> "+ textCantidad +"</label></td>");
            builder.Append("</tr>");
            builder.Append("</tbody></table>");
            builder.Append("</div>");

            builder.Append("<div style='width: 50%; float: left; background-color: white'>");
            builder.Append("<input type='button' id='bDetProducto1' class='cz-form-content-input-button form-button' onclick='");
            if (isMonto)
            {
                builder.Append("irGrillaMonto");
            }
            else
            {
                builder.Append("irGrillaCantidad");
            }
            builder.Append("(" + grupo.id + ")'");
            builder.Append("value='Detalle' />");
            builder.Append("</div>");

            builder.Append("</div>");
            builder.Append("</div>");


            return builder.ToString();
        }

        static string rbDolares(string texto){ return texto;/*.Split('_')[0]*/;}

       public static string generarScripGridDinamico(string nombreDivMonto, string nombreDivCantidad, string nombreDivMonto_d, string radioButtonMonto, string radioButtoCantidad, string radioButtonMonto_d, bool modificacion)
        {
            StringBuilder builder = new StringBuilder();
            
            builder.Append("<script>");

            builder.Append("$(document).ready(function () {");

            builder.Append("$('#" + radioButtonMonto + "').change(function () {");
            builder.Append("document.getElementById('" + nombreDivMonto + "').style.display = 'none';");
            builder.Append("document.getElementById('" + nombreDivCantidad + "').style.display = '';");
            if (modificacion) builder.Append("document.getElementById('" + nombreDivMonto_d + "').style.display = 'none';");
            builder.Append("setRadVal('" + radioButtonMonto + "', 'Monto');");
            builder.Append("setRadVal('" + radioButtoCantidad + "', 'Cantidad');");
            if (modificacion) builder.Append("setRadVal('" + radioButtonMonto_d + "', 'Monto');");
            if (modificacion) builder.Append("setRadVal('m" + rbDolares(radioButtonMonto) + "', 'S');");
            if (modificacion) builder.Append("setRadVal('m_d" + rbDolares(radioButtonMonto) + "', 'D');");
            builder.Append("});");

            builder.Append("$('#" + radioButtoCantidad + "').change(function () {");
            builder.Append("document.getElementById('" + nombreDivCantidad + "').style.display = 'none';");
            builder.Append("document.getElementById('" + nombreDivMonto + "').style.display = '';");
            if (modificacion) builder.Append("document.getElementById('" + nombreDivMonto_d + "').style.display = 'none';");
            builder.Append("setRadVal('" + radioButtonMonto + "', 'Monto');");
            builder.Append("setRadVal('" + radioButtoCantidad + "', 'Cantidad');");
            if (modificacion) builder.Append("setRadVal('" + radioButtonMonto_d + "', 'Monto');");
            if (modificacion) builder.Append("setRadVal('m" + rbDolares(radioButtonMonto) + "', 'S');");
            if (modificacion) builder.Append("setRadVal('m_d" + rbDolares(radioButtonMonto) + "', 'D');");
            builder.Append("});");

            if (modificacion)
            {
                builder.Append("$('#" + radioButtonMonto_d + "').change(function () {");
                builder.Append("document.getElementById('" + nombreDivMonto + "').style.display = 'none';");
                builder.Append("document.getElementById('" + nombreDivCantidad + "').style.display = '';");
                builder.Append("document.getElementById('" + nombreDivMonto_d + "').style.display = 'none';");
                builder.Append("setRadVal('" + radioButtonMonto + "', 'Monto');");
                builder.Append("setRadVal('" + radioButtoCantidad + "', 'Cantidad');");
                builder.Append("setRadVal('" + radioButtonMonto_d + "', 'Monto');");
                builder.Append("setRadVal('m" + rbDolares(radioButtonMonto) + "', 'S');");
                builder.Append("setRadVal('m_d" + rbDolares(radioButtonMonto) + "', 'D');");
                builder.Append("});");

                builder.Append("$('#m" + radioButtonMonto + "').change(function () {");
                builder.Append("document.getElementById('" + nombreDivMonto + "').style.display = 'none';");
                builder.Append("document.getElementById('" + nombreDivCantidad + "').style.display = 'none';");
                builder.Append("document.getElementById('" + nombreDivMonto_d + "').style.display = '';");
                builder.Append("setRadVal('" + radioButtonMonto + "', 'Monto');");
                builder.Append("setRadVal('" + radioButtoCantidad + "', 'Cantidad');");
                builder.Append("setRadVal('" + radioButtonMonto_d + "', 'Monto');");
                builder.Append("setRadVal('m" + rbDolares(radioButtonMonto) + "', 'S');");
                builder.Append("setRadVal('m_d" + rbDolares(radioButtonMonto) + "', 'D');");
                builder.Append("});");

                builder.Append("$('#m_d" + radioButtonMonto + "').change(function () {");
                builder.Append("document.getElementById('" + nombreDivMonto + "').style.display = '';");
                builder.Append("document.getElementById('" + nombreDivCantidad + "').style.display = 'none';");
                builder.Append("document.getElementById('" + nombreDivMonto_d + "').style.display = 'none';");
                builder.Append("setRadVal('" + radioButtonMonto + "', 'Monto');");
                builder.Append("setRadVal('" + radioButtoCantidad + "', 'Cantidad');");
                builder.Append("setRadVal('" + radioButtonMonto_d + "', 'Monto');");
                builder.Append("setRadVal('m" + rbDolares(radioButtonMonto) + "', 'S');");
                builder.Append("setRadVal('m_d" + rbDolares(radioButtonMonto) + "', 'D');");
                builder.Append("});");
            }

            builder.Append("});");

            builder.Append("</script>");

            return builder.ToString();
        }

   
    public static string generarDivLinealDinamico(int i, string nombre)
        {
            StringBuilder builder = new StringBuilder();


            builder.Append("<div class='cz-form-content cz-content-extended' style='height: 360px; width: 350px'>");
            builder.Append("<div class='cz-form-subcontent' style='background-color: white'>");
            builder.Append("<div class='cz-form-subcontent-title'>");
            builder.Append("<p runat='server' style='text-align: center; font-size: 15px'>" + nombre + "</p>");
            builder.Append("</div>");
            builder.Append("<div class='container' id='container" + i + "' style='width: 100px; height: 250px; margin: 0 auto'></div>");
            builder.Append("</div>");
            builder.Append("</div>");

            return builder.ToString();
        }
    public static string dibujarFamiliaGrupoHighcharts(int i, List<PieBeanItem> listam, List<PieBeanItem> listac, List<PieBeanItem> listam_d)
        {
            StringBuilder builder = new StringBuilder();
            StringBuilder dataMonto = new StringBuilder();
            StringBuilder dataMonto_d = new StringBuilder();
            StringBuilder dataMontodrilldown = new StringBuilder();
            StringBuilder dataMontodrilldown_d = new StringBuilder();
            dataMonto.Append("[");
            dataMonto_d.Append("[");
            dataMontodrilldown.Append("[");
            dataMontodrilldown_d.Append("[");
            decimal montototal = 0;
            decimal montototal_d = 0;
            decimal cantidad = 0;

            foreach (PieBeanItem filabean in listam)
            {
                montototal = montototal + Convert.ToDecimal(filabean.montocantidadtotal);
                dataMonto.Append("{name:'" + filabean.name + "',y:" + Convert.ToDecimal(filabean.y)/100 + ",drilldown:'" + filabean.pk + "'},");
                dataMontodrilldown.Append(",{id:'" + filabean.pk + "',");
                dataMontodrilldown.Append("data:[");
                foreach (PieBeanUltimasTransaccionesFecha drilldown in filabean.ultimastransacciones)
                {
                    dataMontodrilldown.Append("['" + drilldown.nombre + "( S/."+ drilldown.valor + ") '," + drilldown.valor   + "],");
                }
                dataMontodrilldown.Append("]}");
            }
            foreach (PieBeanItem filabean_d in listam_d)
            {
                montototal_d = montototal_d + Convert.ToDecimal(filabean_d.montocantidadtotal);
                dataMonto_d.Append("{name:'" + filabean_d.name + "',y:" + Convert.ToDecimal(filabean_d.y) / 100 + ",drilldown:'" + filabean_d.pk + "'},");
                dataMontodrilldown_d.Append(",{id:'" + filabean_d.pk + "',");
                dataMontodrilldown_d.Append("data:[");
                foreach (PieBeanUltimasTransaccionesFecha drilldown_d in filabean_d.ultimastransacciones)
                {
                    dataMontodrilldown_d.Append("['" + drilldown_d.nombre + "( $ " + drilldown_d.valor + ") '," + drilldown_d.valor + "],");
                }
                dataMontodrilldown_d.Append("]}");
            }
            dataMonto.Append("]");
            dataMonto_d.Append("]");
            dataMontodrilldown.Append("]");
            dataMontodrilldown_d.Append("]");

            //CANTIDAD
            StringBuilder dataCantidad = new StringBuilder();
            StringBuilder dataCantidaddrilldown = new StringBuilder();
            dataCantidad.Append("[");
            dataCantidaddrilldown.Append("[");


            foreach (PieBeanItem filabeanc in listac)
            {
                cantidad = cantidad + Convert.ToDecimal(filabeanc.montocantidadtotal);
                dataCantidad.Append("{name:'" + filabeanc.name + "',y:" + Convert.ToDecimal(filabeanc.y) / 100 + ",drilldown:'" + filabeanc.pk + "'},");
                dataCantidaddrilldown.Append(",{id:'" + filabeanc.pk + "',");
                dataCantidaddrilldown.Append("data:[");
                foreach (PieBeanUltimasTransaccionesFecha drilldownc in filabeanc.ultimastransacciones)
                {
                    dataCantidaddrilldown.Append("['" + drilldownc.nombre + "(" + drilldownc.valor + " Cant.) '," + drilldownc.valor + "],");
                }
                dataCantidaddrilldown.Append("]}");
            }
            dataCantidad.Append("]");
            
            dataCantidaddrilldown.Append("]");
            
            builder.Append("Highcharts.chart('container" + i + "m_',{lang:{drillUpText:'Retornar'},chart:{plotBackgroundColor:null,plotBorderWidth:null,plotShadow:false,type:'pie'},title:{text:''},subtitle:{text:'Total: S/.'+"+ montototal + "},tooltip:{pointFormat:'{series.name}:<b>{point.percentage:.1f}%</b>'},plotOptions:{pie:{allowPointSelect:true,cursor:'pointer',dataLabels:{enabled:false},showInLegend:true}},series:[{name:'Venta(%)',colorByPoint:true,data:" + dataMonto + "}],drilldown:{series:" + dataMontodrilldown + "}});");
            builder.Append("Highcharts.chart('container" + i + "c_',{lang:{drillUpText:'Retornar'},chart:{plotBackgroundColor:null,plotBorderWidth:null,plotShadow:false,type:'pie'},title:{text:''},subtitle:{text:''+" + cantidad + "+' Cant.'},tooltip:{pointFormat:'{series.name}:<b>{point.percentage:.1f}%</b>'},plotOptions:{pie:{allowPointSelect:true,cursor:'pointer',dataLabels:{enabled:false},showInLegend:true}},series:[{name:'Cantidad(%)',colorByPoint:true,data:" + dataCantidad + "}],drilldown:{series:" + dataCantidaddrilldown + "}});");
            builder.Append("Highcharts.chart('container_d" + i + "m_',{lang:{drillUpText:'Retornar'},chart:{plotBackgroundColor:null,plotBorderWidth:null,plotShadow:false,type:'pie'},title:{text:''},subtitle:{text:'Total: $ '+" + montototal_d + "},tooltip:{pointFormat:'{series.name}:<b>{point.percentage:.1f}%</b>'},plotOptions:{pie:{allowPointSelect:true,cursor:'pointer',dataLabels:{enabled:false},showInLegend:true}},series:[{name:'Venta(%)',colorByPoint:true,data:" + dataMonto_d + "}],drilldown:{series:" + dataMontodrilldown_d + "}});");
            return builder.ToString();
            
        }
        public static string dibujarColumnGrupoHighcharts(int i, List<FilaBean> listam, List<FilaBean> listac)
        {

            StringBuilder builder = new StringBuilder();
            StringBuilder dataMonto = new StringBuilder();
            StringBuilder dataMontoD = new StringBuilder();
            StringBuilder dataMontodrilldown = new StringBuilder();
            dataMonto.Append("[");
            dataMontoD.Append("[");
            dataMontodrilldown.Append("[");

            List<string> color_s = new List<string>(); List<string> color_d = new List<string>();
            color_s.Add("#7CB5EC"); color_s.Add("#434348"); color_s.Add("#90ED7D"); color_s.Add("#F7A35C"); color_s.Add("#8085E9");
            color_d.Add("#C4DDF7"); color_d.Add("#919199"); color_d.Add("#CCF7C4"); color_d.Add("#FBC99F"); color_d.Add("#B5B8F2");
            int xi = 0;
            foreach (FilaBean filabean in listam)
               {
                dataMonto.Append("{name:'" + filabean.nombre + "',y:" + filabean.valor_Soles + ",drilldown:'" + filabean.pk + "_s" + "',color: '" + color_s[xi] + "'},");
                dataMontoD.Append("{name:'" + filabean.nombre + "',y:" + filabean.valor_Dolares + ",drilldown:'" + filabean.pk + "_d" + "',color: '" + color_d[xi] + "'},");
                xi++;
                dataMontodrilldown.Append(",{name:'Monto Soles',id:'" + filabean.pk + "_s" + "',");
                dataMontodrilldown.Append("data:[");
                foreach (FilaBeanUltimasTransaccionesFecha drilldown in filabean.ultimastransacciones)
                {
                    dataMontodrilldown.Append("['"+ drilldown.fecha + "',"+ drilldown.valorxfecha_Soles+ "],");
                }
                dataMontodrilldown.Append("]}");


                dataMontodrilldown.Append(",{name: 'Monto Dólares',id:'" + filabean.pk + "_d" + "',");
                dataMontodrilldown.Append("data:[");
                foreach (FilaBeanUltimasTransaccionesFecha drilldown in filabean.ultimastransacciones)
                {
                    dataMontodrilldown.Append("['" + drilldown.fecha + "'," + drilldown.valorxfecha_Dolares + "],");
                }
                dataMontodrilldown.Append("]}");
            }
            dataMonto.Append("]");
            dataMontoD.Append("]");
            dataMontodrilldown.Append("]");

            //CANTIDAD
            StringBuilder dataCantidad = new StringBuilder();
            StringBuilder dataCantidaddrilldown = new StringBuilder();
            dataCantidad.Append("[");
            dataCantidaddrilldown.Append("[");


            foreach (FilaBean filabeanc in listac)
            {
                dataCantidad.Append("{name:'" + filabeanc.nombre + "',y:" + filabeanc.valor + ",drilldown:'" + filabeanc.pk + "'},");
                dataCantidaddrilldown.Append(",{id:'" + filabeanc.pk + "',");
                dataCantidaddrilldown.Append("data:[");
                foreach (FilaBeanUltimasTransaccionesFecha drilldownc in filabeanc.ultimastransacciones)
                {
                    dataCantidaddrilldown.Append("['" + drilldownc.fecha + "'," + drilldownc.valorxfecha + "],");
                }
                dataCantidaddrilldown.Append("]}");
            }
            dataCantidad.Append("]");
            dataCantidaddrilldown.Append("]");
            
            builder.Append("Highcharts.chart('container" + i + "m_',{lang:{drillUpText:'Retornar'},chart:{type:'column',inverted:true},title:{text:''},xAxis:{type:'category'},legend:{enabled:false},plotOptions:{series:{borderWidth:0,dataLabels:{enabled: true}}},series:[{name:'Monto Soles',colorByPoint:true,data:"+ dataMonto + "},{name:'Monto Dólares',colorByPoint:true,data:" + dataMontoD + "}],drilldown:{activeAxisLabelStyle:{textDecoration:'none',color:'#666',},activeDataLabelStyle:{color:'#666'},series:" + dataMontodrilldown + "}});");
            builder.Append("Highcharts.chart('container" + i + "c_',{lang:{drillUpText:'Retornar'},chart:{type:'column',inverted:true},title:{text:''},xAxis:{type:'category'},legend:{enabled:false},plotOptions:{series:{borderWidth:0,dataLabels:{enabled: true}}},series:[{name:'Cantidad',colorByPoint:true,data:" + dataCantidad + "}],drilldown:{activeAxisLabelStyle:{textDecoration:'none',color:'#666',},activeDataLabelStyle:{color:'#666'},series:" + dataCantidaddrilldown + "}});");
            return builder.ToString();

        }
  

        public static string generarScriptLinealDinamico(int i, List<LinearGraficoBean> graficos)
        {
            StringBuilder builder = new StringBuilder();
            StringBuilder data = new StringBuilder();
            StringBuilder data2 = new StringBuilder();

            foreach (LinearGraficoBean grafico in graficos)
            {
                string[] fechas = grafico.fecha.Split('/');
                int mes = int.Parse(fechas[1]) - 1;

                data.Append("[Date.UTC(" + fechas[2] + ", " + mes + ", " + fechas[0] + "), " + grafico.valor + "],");
                data2.Append("[Date.UTC(" + fechas[2] + ", " + mes + ", " + fechas[0] + "), " + grafico.valor2 + "],");
            }

            if (data.Length > 0)
            {
                data.Remove(data.Length - 1, 1);
            }

            if (data2.Length > 0)
            {
                data2.Remove(data2.Length - 1, 1);
            }

            builder.Append("Highcharts.chart('container" + i + "',{chart:{type:'spline'},title:{text:''},subtitle:{text:''},xAxis:{type:'datetime',dateTimeLabelFormats:{month:'%e %b',year:'%b'}},yAxis:{title:{text:''},min:0},tooltip:{headerFormat:'<b>{series.name}</b><br>',pointFormat:'{point.x:%e %b}: {point.y:.2f}'},plotOptions:{spline:{marker:{enabled:true}}},series:[{name:'Ventas Soles',data:[" + data.ToString() + "]},{name:'Ventas Dólares',data:[" + data2.ToString() + "]}]});");
            
            return builder.ToString();
        }

        public static string generarDivBarraDinamico(int i, string id, string nombre)
        {
            StringBuilder builder = new StringBuilder();


            builder.Append("<div class='cz-form-content cz-content-extended' style='height: 360px; width: 350px'>");
            builder.Append("<div class='cz-form-subcontent' style='background-color: white'>");
            builder.Append("<div class='cz-form-subcontent-title'>");
            builder.Append("<p runat='server' style='text-align: center; font-size: 15px'>" + nombre + "</p>");
            builder.Append("</div>");
            builder.Append("<div class='container' id='container" + i + "' style='width: 150px; height: 250px; margin: 0 auto'></div>");
            builder.Append("</div>");
            builder.Append("<div style='width: 100%; float: right; background-color: white'>");
            builder.Append("<input type='button' id='bDetEfectivos' class='cz-form-content-input-button form-button' onclick='irGrilla(" + id + ")'");
            builder.Append("value='Detalle' />");
            builder.Append("</div>");
            builder.Append("</div>");

            return builder.ToString();
        }


        public static string generarScriptBarraDinamico(int i, List<BarraGraficoItemBean> graficos)
        {
            StringBuilder builder = new StringBuilder();
            StringBuilder data = new StringBuilder();


            foreach (BarraGraficoItemBean grafico in graficos)
            {
                data.Append("{name:'" + grafico.name + "',y:" + grafico.y + "},");
            }

            if (data.Length > 0)
            {
                data.Remove(data.Length - 1, 1);
            }


            builder.Append("Highcharts.chart('container"+i+"',{chart:{type:'column'},title:{text:''},subtitle:{text:''},xAxis:{type:'category'},yAxis:{title:{text:''}},legend:{enabled:false},plotOptions:{series:{borderWidth:0,dataLabels:{enabled:true,format:'{point.y}'}}},tooltip:{headerFormat:'<span style=\"font - size:11px\" >{series.name}</span><br>',pointFormat:'<span style=\"color:{ point.color}\">{point.name}</span>: <b>{point.y}</b> total<br/>'},series:[{name:'Efectivos',colorByPoint:true,data:["+ data.ToString() + "]}]});");

            return builder.ToString();
        }

        public static string dibujarReporteSimpleGrupo(string id, string nombre, string tiempo)
        {
            StringBuilder builder = new StringBuilder();


            builder.Append("<div class='cz-form-content cz-content-extended' style='height: 360px; width: 350px'>");
            builder.Append("<div class='cz-form-subcontent' style='background-color: white'>");
            builder.Append("<div class='cz-form-subcontent-title'>");
            builder.Append("<p runat='server' style='text-align: center; font-size: 15px'>" + nombre + "</p>");
            builder.Append("</div>");
            builder.Append("<div align='center' style='height: 250px;'>");
            builder.Append(tiempo);
            builder.Append("</div>");
            builder.Append("<div style='width: 100%; float: right; background-color: white'>");
            builder.Append("<input type='button' id='bDetPromedio' class='cz-form-content-input-button form-button' onclick='irGrilla(" + id + ")'");
            builder.Append("value='Detalle' />");
            builder.Append("</div>");
            builder.Append("</div>");
            builder.Append("</div>");

            return builder.ToString();
        }

        public static string generarDivGridDinamicoFamilia(string nombre, GrupoBean grupo, List<PieBeanItem> filas, string nombreDiv, string radioButton, bool isMonto, string textMonto, string textCantidad, int i, string moneda)
        {
            StringBuilder builder = new StringBuilder();
            StringBuilder data = new StringBuilder();
            String tipo = "m_";
            String tipo2 = string.Empty;
           builder.Append("<div id='" + nombreDiv + "' class='cz-form-content cz-content-extended' style='height: 520px; width: 350px");

            if (!isMonto)
            {
                tipo = "c_";
                builder.Append(";display:none");
            }
            else if (moneda == "D")
            {
                tipo2 = "_d";
                builder.Append(";display:none");
            }

            builder.Append("'>");
            builder.Append("<div class='cz-form-subcontent' style='background-color: white'>");
            builder.Append("<div class='cz-form-subcontent-title'><p runat='server' style='text-align: center; font-size: 15px'>" + grupo.nombre + "</p></div>");



           /* if (filas.Count > 0)
            {*/

                builder.Append("<div class='container' id='container"+ tipo2 + i + ""+tipo+"' style='width: 310px; height: 400px; margin: 0 auto'></div>");


          /*    }
          else
            {
                builder.Append("<div align='center' style='height: 400px'>");
                builder.Append("<div class='gridNoData'>" +
                               "<p style='color:red'>No se encontraron datos para mostrar</p>" +
                               "</div>");
                builder.Append("</div>");
            }*/
            builder.Append("</div>");

            if (isMonto) builder.Append("<div style='width: 50%; float: left; background-color: white'>");
            else builder.Append("<div style='width: 100%; float: left; background-color: white'>");
            builder.Append("<table id='" + radioButton + "' border='0'>");
            builder.Append("<tbody><tr>");
            builder.Append("<td><input id='" + radioButton + "_0' type='radio' name='" + radioButton + "' value='M' checked='checked'><label for='" + radioButton + "_0'>" + textMonto + "</label></td>");
            builder.Append("</tr><tr>");
            builder.Append("<td><input id='" + radioButton + "_1' type='radio' name='" + radioButton + "' value='C'><label for='" + radioButton + "_1'>" + textCantidad + "</label></td>");
            builder.Append("</tr>");
            builder.Append("</tbody></table>");
            builder.Append("</div>");

            if (isMonto)
            {
                if (moneda == "S")
                {
                    builder.Append("<div style='width: 50%; float: left; background-color: white'>");
                    builder.Append("<table id='mrbMonto" + i + "' border='0'><tbody><tr><td>");
                    builder.Append("<input id='mrbMonto" + i + "_0' type='radio' name='mrbMonto" + i + "' value='S' checked='checked'>");
                    builder.Append("<label for='mrbMonto" + i + "_0'>Soles</label></td></tr><tr><td>");
                    builder.Append("<input id='mrbMonto" + i + "_1' type='radio' name='mrbMonto" + i + "' value='D'>");
                    builder.Append("<label for='mrbMonto" + i + "_1'>Dólares</label></td></tr></tbody></table></div>");
                }
                else if (moneda == "D")
                {
                    builder.Append("<div style='width: 50%; float: left; background-color: white'>");
                    builder.Append("<table id='m_drbMonto" + i + "' border='0'><tbody><tr><td>");
                    builder.Append("<input id='m_drbMonto" + i + "_0' type='radio' name='m_drbMonto" + i + "' value='S'>");
                    builder.Append("<label for='m_drbMonto" + i + "_0'>Soles</label></td></tr><tr><td>");
                    builder.Append("<input id='m_drbMonto" + i + "_1' type='radio' name='m_drbMonto" + i + "' value='D' checked='checked'>");
                    builder.Append("<label for='m_drbMonto" + i + "_1'>Dólares</label></td></tr></tbody></table></div>");
                }
            }
            
            builder.Append("</div>");


            return builder.ToString();
        }
        public static string generarDivGridDinamicoColumnHighChart(string nombre, GrupoBean grupo, List<FilaBean> filas, string nombreDiv, string radioButton, bool isMonto, string textMonto, string textCantidad, int i)
        {
            StringBuilder builder = new StringBuilder();
            StringBuilder data = new StringBuilder();
            String tipo = "m_";
            builder.Append("<div id='" + nombreDiv + "' class='cz-form-content cz-content-extended' style='height: 520px; width: 350px");

            if (!isMonto)
            {
                tipo = "c_";
                builder.Append(";display:none");
            }

            builder.Append("'>");
            builder.Append("<div class='cz-form-subcontent' style='background-color: white'>");
            builder.Append("<div class='cz-form-subcontent-title'><p runat='server' style='text-align: center; font-size: 15px'>" + grupo.nombre + "</p></div>");



           /* if (filas.Count > 0)
            {*/

                builder.Append("<div class='container' id='container" + i + "" + tipo + "' style='width: 310px; height: 400px; margin: 0 auto'></div>");


         /*   }
            else
            {
                builder.Append("<div align='center' style='height: 400px'>");
                builder.Append("<div class='gridNoData'>" +
                               "<p style='color:red'>No se encontraron datos para mostrar</p>" +
                               "</div>");
                builder.Append("</div>");
            }*/
            builder.Append("</div>");

            builder.Append("<div style='width: 50%; float: left; background-color: white'>");
            builder.Append("<table id='" + radioButton + "' border='0'>");
            builder.Append("<tbody><tr>");
            builder.Append("<td><input id='" + radioButton + "_0' type='radio' name='" + radioButton + "' value='M' checked='checked'><label for='" + radioButton + "_0'>" + textMonto + "</label></td>");
            builder.Append("</tr><tr>");
            builder.Append("<td><input id='" + radioButton + "_1' type='radio' name='" + radioButton + "' value='C'><label for='" + radioButton + "_1'> " + textCantidad + "</label></td>");
            builder.Append("</tr>");
            builder.Append("</tbody></table>");
            builder.Append("</div>");
            builder.Append("<div style='width: 50%; float: left; background-color: white'>");
            builder.Append("<input type='button' id='bDetProducto1' class='cz-form-content-input-button form-button' onclick='");
            if (isMonto)
            {
                builder.Append("irGrillaMonto");
            }
            else
            {
                builder.Append("irGrillaCantidad");
            }
            builder.Append("(" + grupo.id + ")'");
            builder.Append("value='Detalle' />");
            builder.Append("</div>");
            builder.Append("</div>");


            return builder.ToString();
        }
    }
}
