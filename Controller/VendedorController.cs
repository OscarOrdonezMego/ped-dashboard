﻿using Model;
using Model.bean;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Controller;
using Controller.functions;

public class VendedorController
{
    public static List<FilaBean> obtenerTopVendedores(string fechaInicio, string fechaFin, string tipo, string usuario)
    {
        List<FilaBean> list = new List<FilaBean>();
                      
        DataTable dt = VendedorModel.obtenerTopVendedores(fechaInicio, fechaFin, tipo, usuario);
        DataTable dtv;
        if (dt != null && dt.Rows.Count > 0)
        {
            foreach (DataRow row in dt.Rows)
            {
                FilaBean bean = new FilaBean();
                bean.ultimastransacciones = new List<FilaBeanUltimasTransaccionesFecha>();
                bean.nombre = row["nombre"].ToString();
                if (tipo == "M")
                {
                    bean.valor_Soles = row["valor_Soles"].ToString();
                    bean.valor_Dolares = row["valor_Dolares"].ToString();
                }
                else if (tipo == "C")
                {
                    bean.valor = row["valor"].ToString();
                }
                bean.pk = row["pk"].ToString();
                dtv=VendedorModel.obtenerUltimosPedidos(bean.pk, fechaFin, tipo);
                foreach (DataRow rowv in dtv.Rows)
                {
                    FilaBeanUltimasTransaccionesFecha beanv = new FilaBeanUltimasTransaccionesFecha();
                    beanv.pk = rowv["pk"].ToString();
                    beanv.fecha = util.getStringFechaYYMMDD(rowv["fecha"].ToString());
                    if (tipo == "M")
                    {
                        beanv.valorxfecha_Soles = rowv["valorfecha_Soles"].ToString();
                        beanv.valorxfecha_Dolares = rowv["valorfecha_Dolares"].ToString();
                    }
                    else if (tipo == "C")
                    {
                        beanv.valorxfecha = rowv["valorfecha"].ToString();
                    }
                    bean.ultimastransacciones.Add(beanv);
                }
                list.Add(bean);
            }
        }

        return list;
    }

    public static List<FilaBean> obtenerTopVendedoresGrupo(string fechaInicio, string fechaFin, string tipo, string grupo)
    {
        List<FilaBean> list = new List<FilaBean>();
        DataTable dt = VendedorModel.obtenerTopVendedoresGrupo(fechaInicio, fechaFin, tipo, grupo);
        DataTable dtv;
        if (dt != null && dt.Rows.Count > 0)
        {
            foreach (DataRow row in dt.Rows)
            {
                FilaBean bean = new FilaBean();
                bean.ultimastransacciones = new List<FilaBeanUltimasTransaccionesFecha>();
                bean.nombre = row["nombre"].ToString();
                if (tipo == "C")
                {
                    bean.valor = row["valor"].ToString();
                }
                else if (tipo == "M")
                {
                    bean.valor_Soles = row["valor_Soles"].ToString();
                    bean.valor_Dolares = row["valor_Dolares"].ToString();
                }
                bean.pk = row["pk"].ToString();
                dtv = VendedorModel.obtenerUltimosPedidos(bean.pk, fechaFin, tipo);
                foreach (DataRow rowv in dtv.Rows)
                {
                    FilaBeanUltimasTransaccionesFecha beanv = new FilaBeanUltimasTransaccionesFecha();
                    beanv.pk = rowv["pk"].ToString();
                    beanv.fecha = util.getStringFechaYYMMDD(rowv["fecha"].ToString());
                    if (tipo == "C")
                    {
                        beanv.valorxfecha = rowv["valorfecha"].ToString();
                    }
                    else if (tipo == "M")
                    {
                        beanv.valorxfecha_Soles = rowv["valorfecha_Soles"].ToString();
                        beanv.valorxfecha_Dolares = rowv["valorfecha_Dolares"].ToString();
                    }
                    bean.ultimastransacciones.Add(beanv);
                }
                list.Add(bean);
            }
        }

        return list;
    }

    public static DataTable obtenerVendedoresGrupo(string fechaInicio, string fechaFin, string tipo, string grupo)
    {
        return VendedorModel.obtenerVendedoresGrupo(fechaInicio, fechaFin, tipo, grupo); ;
    }

    public static string dibujarReporteTopVendedorGrupo(string fechaInicio, string fechaFin, string usuario, string perfil)
    {
        StringBuilder builder = new StringBuilder();
        List<GrupoBean> list = UsuarioController.obtenerGrupos(usuario, perfil);

        for (int i = 0; i < list.Count; i++)
        {
            GrupoBean grupo = list[i];

            List<FilaBean> filasM = obtenerTopVendedoresGrupo(fechaInicio, fechaFin, "M", grupo.id);
            List<FilaBean> filasC = obtenerTopVendedoresGrupo(fechaInicio, fechaFin, "C", grupo.id);

            string nombreDivMonto = "divMonto" + i;
            string radioButtonMonto = "rbMonto" + i;
            string nombreDivCantidad = "divCantidad" + i;
            string radioButtoCantidad = "rbCantidad" + i;

            string strDiv = ReporteController.dibujarReporteGridGrupo("Vendedor",grupo, filasM, filasC, nombreDivMonto, nombreDivCantidad, radioButtonMonto, radioButtoCantidad,"Monto total de Venta","Cantidad de Pedidos");

            builder.Append(strDiv);
        }


        return builder.ToString();
    }

}

