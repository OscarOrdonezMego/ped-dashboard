﻿using Model;
using Model.bean;
using System.Collections.Generic;
using System.Data;
using System;
using System.Text;

namespace Controller
{
    public class AvanceController
    {
        public static List<LinearGraficoBean> obtenerAvanceVentas(string fechaInicio, string fechaFin, string usuario)
        {
            List<LinearGraficoBean> list = new List<LinearGraficoBean>();
            DataTable dt = AvanceModel.obtenerAvanceVentas(fechaInicio, fechaFin, usuario);

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    LinearGraficoBean bean = new LinearGraficoBean();
                    bean.fecha = row["fecha"].ToString();
                    bean.valor = row["valor"].ToString();
                    bean.valor2 = row["valor2"].ToString();
                    list.Add(bean);
                }
            }

            return list;
        }

        public static List<LinearGraficoBean> obtenerAvanceVentasGrupo(string fechaInicio, string fechaFin, string grupo)
        {
            List<LinearGraficoBean> list = new List<LinearGraficoBean>();
            DataTable dt = AvanceModel.obtenerAvanceGrupos(fechaInicio, fechaFin, grupo);

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    LinearGraficoBean bean = new LinearGraficoBean();
                    bean.fecha = row["fecha"].ToString();
                    bean.valor = row["valor"].ToString();
                    bean.valor2 = row["valor2"].ToString();
                    list.Add(bean);
                }
            }

            return list;
        }


    }
}
