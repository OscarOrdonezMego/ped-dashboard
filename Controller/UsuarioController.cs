﻿using Model;
using Model.bean;
using System.Collections.Generic;
using System.Data;
using System.Web.Security;

namespace Controller
{
    public class UsuarioController
    {
        public static UsuarioBean validarUsuario(string login, string clave)
        {
            string claveCifrada = FormsAuthentication.HashPasswordForStoringInConfigFile(clave, "sha1");

            UsuarioBean loBeanUsuario = new UsuarioBean();
            DataTable dt = UsuarioModel.validarUsuario(login, claveCifrada);

            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                loBeanUsuario.codigo = row["codigo"].ToString();
                loBeanUsuario.nombre = row["nombre"].ToString();
                loBeanUsuario.perfil = row["perfil"].ToString();
                loBeanUsuario.id = row["id"].ToString();
            }

            return loBeanUsuario;
        }

        public static List<GrupoBean> obtenerGrupos(string usuario, string perfil)
        {
            List<GrupoBean> list = new List<GrupoBean>();

            DataTable dt = UsuarioModel.obtenerGrupos(usuario);

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    GrupoBean bean = new GrupoBean();
                    bean.nombre = row["nombre"].ToString();
                    bean.id = row["id"].ToString();
                    list.Add(bean);
                }
            }


            if (!perfil.Equals("4"))
            {
                GrupoBean bean = new GrupoBean();
                bean.nombre = "Sin Grupo";
                bean.id = "-1";
                list.Add(bean);
            }


            return list;
        }
    }
}
