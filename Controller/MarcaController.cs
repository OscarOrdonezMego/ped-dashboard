﻿using Model;
using Model.bean;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Controller
{
  public  class MarcaController
    {
        public static List<PieBeanItem> obtenerMarcas(string fechaInicio, string fechaFin, string tipo, string usuario, string moneda)
        {
            List<PieBeanItem> list = new List<PieBeanItem>();
            DataTable dt = MarcaModel.obtenerReporteMarca(fechaInicio, fechaFin, tipo, usuario, moneda);
            DataTable dtv;
            String simbolomoneda = "";
            if (tipo == "M")
            {
                if (moneda == "S")
                {
                    simbolomoneda = "S/.";
                }
                else if (moneda == "D")
                {
                    simbolomoneda = "$ ";
                }
            }
            else
            {
                simbolomoneda = "";
            }
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    PieBeanItem bean = new PieBeanItem();
                    bean.ultimastransacciones = new List<PieBeanUltimasTransaccionesFecha>();
                    bean.name = row["MARCA"].ToString() + " (" + simbolomoneda + "" + row["MONTOCANTIDAD"].ToString() + ")";
                    bean.y = Convert.ToDecimal(row["PORCENTAJE"]);
                    bean.montocantidadtotal = float.Parse((row["MONTOCANTIDAD"].ToString()));
                    bean.pk = row["pk"].ToString();
                    dtv = MarcaModel.obtenerUltimasVentas(bean.pk, fechaInicio, fechaFin, tipo, moneda);
                    foreach (DataRow rowv in dtv.Rows)
                    {
                        PieBeanUltimasTransaccionesFecha beanv = new PieBeanUltimasTransaccionesFecha();
                        beanv.pk = rowv["pk"].ToString();
                        beanv.nombre = rowv["familia"].ToString();
                        beanv.valor = rowv["valorfamilia"].ToString();
                        bean.ultimastransacciones.Add(beanv);
                    }
                    list.Add(bean);
                }
            }

            return list;
        }
        public static List<PieBeanItem> obtenerTopMarcaGrupo(string fechaInicio, string fechaFin, string tipo, string grupo, string moneda)
        {
            List<PieBeanItem> list = new List<PieBeanItem>();
            DataTable dt = MarcaModel.obtenerTopMarcaGrupo(fechaInicio, fechaFin, tipo, grupo, moneda);
            DataTable dtv;
            String simbolomoneda = "";
            if (tipo == "M")
            {
                if (moneda == "S")
                {
                    simbolomoneda = "S/.";
                }
                else if (moneda == "D")
                {
                    simbolomoneda = "$ ";
                }
            }
            else
            {
                simbolomoneda = "";
            }
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    PieBeanItem bean = new PieBeanItem();
                    bean.ultimastransacciones = new List<PieBeanUltimasTransaccionesFecha>();
                    bean.name = row["MARCA"].ToString() + " (" + simbolomoneda + "" + row["MONTOCANTIDAD"].ToString() + ")";
                    bean.y = Convert.ToDecimal(row["PORCENTAJE"]);
                    bean.montocantidadtotal = float.Parse((row["MONTOCANTIDAD"].ToString()));
                    bean.pk = row["pk"].ToString();
                    dtv = MarcaModel.obtenerUltimasVentasxGrupo(bean.pk, fechaInicio, fechaFin, tipo , grupo, moneda);
                    foreach (DataRow rowv in dtv.Rows)
                    {
                        PieBeanUltimasTransaccionesFecha beanv = new PieBeanUltimasTransaccionesFecha();
                        beanv.pk = rowv["pk"].ToString();
                        beanv.nombre = rowv["familia"].ToString();
                        beanv.valor = rowv["valorfamilia"].ToString();
                        bean.ultimastransacciones.Add(beanv);
                    }
                    list.Add(bean);
                }
            }

            return list;
        }

        public static bool existeMarca()
        {
            bool existe = false;
            DataTable dt = MarcaModel.obtenerMarcaexiste();
            if (dt != null && dt.Rows.Count > 0)
            {
                existe = true;
            }
            return existe;
        }
    }
    
}
