﻿using Model;
using System.Data;
using System;
using System.Text;
using System.Collections.Generic;

namespace Controller
{
    public class PromedioController
    {

        public static string obtenerTiempoPromedio(string fechaInicio, string fechaFin, string usuario)
        {
            string resultado = "<div class='gridNoData'>" +
                                    "<p style='color:red'>No se encontraron datos para mostrar</p>" +
                                    "</div>";
            DataTable dt = PromedioModel.obtenerTiempoPromedio(fechaInicio, fechaFin, usuario);

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {

                    string promedio = row["promedio"].ToString();
                    if (promedio != null && promedio != "")
                    {
                        resultado = "<div class='gridNoData'>" +
                                        "<br/><p style='font-size: 26px;'>" + row["promedio"].ToString() + " minutos</p>" +
                                        "</div>";

                    }


                }
            }

            return resultado;
        }
        public static string obtenerTiempoPromediosinhtml(string fechaInicio, string fechaFin, string usuario)
        {
            string resultado ="0";
            DataTable dt = PromedioModel.obtenerTiempoPromedio(fechaInicio, fechaFin, usuario);

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {

                    string promedio = row["promedio"].ToString();
                    if (promedio != null && promedio != "")
                    {
                        resultado = (row["promedio"].ToString());
                    }


                }
            }

            return resultado;
        }
        public static string obtenerTiempoPromedioGrupo(string fechaInicio, string fechaFin, string grupo)
        {
            string resultado = "<div class='gridNoData'>" +
                                    "<p style='color:red'>No se encontraron datos para mostrar</p>" +
                                    "</div>";
            DataTable dt = PromedioModel.obtenerTiempoPromedioGrupo(fechaInicio, fechaFin, grupo);

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {

                    string promedio = row["promedio"].ToString();
                    if (promedio != null && promedio != "")
                    {
                        resultado = "<div class='gridNoData'>" +
                                        "<br/><p style='font-size: 26px;'>" + row["promedio"].ToString() + " minutos</p>" +
                                        "</div>";

                    }


                }
            }

            return resultado;
        }
        public static string obtenerTiempoPromedioGruposinhtml(string fechaInicio, string fechaFin, string grupo)
        {
            string resultado = "0";
            DataTable dt = PromedioModel.obtenerTiempoPromedioGrupo(fechaInicio, fechaFin, grupo);

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {

                    string promedio = row["promedio"].ToString();
                    if (promedio != null && promedio != "")
                    {
                        resultado =  row["promedio"].ToString();

                    }


                }
            }

            return resultado;
        }


        public static DataTable obtenerTiempoPromedioGrupoXVendedor(string fechaInicio, string fechaFin, string grupo)
        {
            return PromedioModel.obtenerTiempoPromedioGrupoXVendedor(fechaInicio, fechaFin, grupo); ;
        }

        public static string dibujarReporteTiempoPromedioGrupo(string fechaInicio, string fechaFin, string idUsuario, string perfil)
        {
            StringBuilder builder = new StringBuilder();
            List<GrupoBean> list = UsuarioController.obtenerGrupos(idUsuario, perfil);


            for (int i = 0; i < list.Count; i++)
            {
                GrupoBean grupo = list[i];
                string tiempo = obtenerTiempoPromedioGrupo(fechaInicio, fechaFin, grupo.id);
                string strDiv = ReporteController.dibujarReporteSimpleGrupo(grupo.id, grupo.nombre, tiempo);
                builder.Append(strDiv);
            }

            return builder.ToString();
        }


    }
}
