﻿using Controller;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;

namespace server_reportes.Efectivo
{
    public partial class Grid : System.Web.UI.Page
    {
        string tipo = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();
            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

            string grupo = dataJSON["grupo"];
            string fechaInicio = dataJSON["fechaInicio"];
            string fechaFin = dataJSON["fechaFin"];

            DataTable dt = EfectivoController.obtenerPedidosEfectivosGrupoXVendedor(fechaInicio, fechaFin, grupo);

            if (dt.Rows.Count > 0)
            {
                grdHistorial.DataSource = dt;
                grdHistorial.DataBind();
            }
            else
            {
                litNoData.Text = "No hay datos";
            }
        }

        protected void grdHistorial_RowDataBound(object sender, GridViewRowEventArgs e)
        {
          
        }
    }
}