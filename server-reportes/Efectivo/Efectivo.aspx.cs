﻿using Controller;
using Model;
using Model.bean;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;

namespace server_reportes.Efectivo
{
    public partial class Efectivo : System.Web.UI.Page
    {
        public string fechaInicio;
        public string fechaFin;
        public string idUsuario;
        public string perfil;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                iniciarDatos();
                dibujarGraficos();

            }
            catch (Exception ex)
            {
                string myScript = "parent.document.location.href = '../default.aspx?acc=EXT';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, true);
            }
        }

        private void iniciarDatos()
        {
            idUsuario = Session["lgn_id"].ToString();
            perfil = Session["lgn_perfil"].ToString();

            if (!Page.IsPostBack)
            {
                fechaInicio = Request.QueryString["fechaInicio"];
                fechaFin = Request.QueryString["fechaFin"];
                txtFecInicio.Text = Request.QueryString["fechaInicio"];
                txtFecFin.Text = Request.QueryString["fechaFin"];
            }
        }


        private void dibujarGraficos()
        {
            StringBuilder builder = new StringBuilder();
            StringBuilder builderScript = new StringBuilder();
            List<GrupoBean> list = UsuarioController.obtenerGrupos(idUsuario, perfil);

            for (int i = 0; i < list.Count; i++)
            {
                GrupoBean grupo = list[i];

                List<BarraGraficoItemBean> graficos = EfectivoController.obtenerPedidosEfectivosGrupo(fechaInicio, fechaFin, grupo.id);

                string strGrafico = ReporteController.generarDivBarraDinamico(i,grupo.id, grupo.nombre);
                string strScript = ReporteController.generarScriptBarraDinamico(i, graficos);

                builder.Append(strGrafico);
                builderScript.Append(strScript);

            }

            ltrEfectivo.Text = builder.ToString();
            Page.ClientScript.RegisterStartupScript(GetType(), "close panel", builderScript.ToString(), true);


        }
    }
}
