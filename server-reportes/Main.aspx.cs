﻿using System;
using System.Web;

namespace server_reportes
{
    public partial class Main : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            inicializarValores();
        }

        public void inicializarValores()
        {
            string urlhome = "Inicio.aspx";

            if (Session["lgn_nombre"] != null)
            { lbNomUsuario.Text = Session["lgn_nombre"].ToString(); }
            else
            { Response.Redirect("Default.aspx?acc=SES"); }

            Page.ClientScript.RegisterStartupScript(this.GetType(), "initialize", "$('#centerFrame').attr('src', '" + urlhome + "');", true);
        }

        protected void opcSalir_Click(object sender, EventArgs e)
        {
            if (Request.Cookies["usr"] != null && Request.Cookies["psw"] != null)
            {
                HttpCookie myCookie = new HttpCookie("usr");
                myCookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(myCookie);

                HttpCookie myCookie2 = new HttpCookie("psw");
                myCookie2.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(myCookie2);

            }
            Session.Clear();
            Session.Abandon();
            Response.Redirect("Default.aspx?acc=EXT");
        }
    }
}