﻿using Controller;
using Model;
using Model.bean;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;

namespace server_reportes.Cliente
{
    public partial class Cliente : Page
    {
        public string fechaInicio;
        public string fechaFin;
        public string idUsuario;
        public string perfil;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                iniciarDatos();
                dibujarGraficos();

            }
            catch (Exception ex)
            {
                string myScript = "parent.document.location.href = '../default.aspx?acc=EXT';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, true);
            }
        }

        private void iniciarDatos()
        {
            idUsuario = Session["lgn_id"].ToString();
            perfil = Session["lgn_perfil"].ToString();

            if (!Page.IsPostBack)
            {
                fechaInicio = Request.QueryString["fechaInicio"];
                fechaFin = Request.QueryString["fechaFin"];
                txtFecInicio.Text = Request.QueryString["fechaInicio"];
                txtFecFin.Text = Request.QueryString["fechaFin"];
            }
        }


        private void dibujarGraficos()
        {
            //  ltrCliente.Text = ClienteController.dibujarReporteTopClienteGrupo(fechaInicio, fechaFin, idUsuario, perfil);
            // ltrVendedor.Text = VendedorController.dibujarReporteTopVendedorGrupo(fechaInicio, fechaFin, idUsuario, perfil);
            StringBuilder builder = new StringBuilder();
            StringBuilder builderScript = new StringBuilder();

            List<GrupoBean> list = UsuarioController.obtenerGrupos(idUsuario, perfil);

            for (int i = 0; i < list.Count; i++)
            {
                GrupoBean grupo = list[i];
                //FALTA
                List<FilaBean> filasM = ClienteController.obtenerTopClienteGrupo(fechaInicio, fechaFin, "M", grupo.id);
                List<FilaBean> filasC = ClienteController.obtenerTopClienteGrupo(fechaInicio, fechaFin, "C", grupo.id);
                string nombreDivMonto = "divMonto" + i;
                string radioButtonMonto = "rbMonto" + i;
                string nombreDivCantidad = "divCantidad" + i;
                string radioButtoCantidad = "rbCantidad" + i;

                string strDiv = ReporteController.dibujarReporteColumnGrupo("Cliente", grupo, filasM, filasC, nombreDivMonto, nombreDivCantidad, radioButtonMonto, radioButtoCantidad, "Monto total de Venta", "Cantidad de items", i);

                string strScript = ReporteController.dibujarColumnGrupoHighcharts(i, filasM, filasC);

                builder.Append(strDiv);
                builderScript.Append(strScript);

            }

            ltrCliente.Text = builder.ToString();
            Page.ClientScript.RegisterStartupScript(GetType(), "close panel", builderScript.ToString(), true);



        }
    }
}