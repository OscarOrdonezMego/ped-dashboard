﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Grid.aspx.cs" Inherits="server_reportes.Cliente.Grid" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="../js/bootstrap.min.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server" style="background: white; border-radius: 6px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                ×</button>
            <h3 id="myModalLabel" runat="server">Detalle Top Clientes</h3>

        </div>
        <div class="row" style="border-radius: 6px; padding: 5px; max-height: 300px; overflow: auto; overflow-x: hidden;">
            <%--<div style="text-align: center;" class="hidden-xs col-sm-4 col-md-4 col-lg-4"></div>--%>
            <div style="padding-left: 16px; padding-right: 16px">
                <asp:Literal ID="litNoData" runat="server"></asp:Literal>
                <div id="divGridView1" runat="server">
                    <asp:GridView ID="grdHistorial" GridLines="None" AlternatingRowStyle-CssClass="alt"
                        runat="server" AutoGenerateColumns="False" CssClass="grilla table table-bordered table-striped" Style="width: 100%;" OnRowDataBound="grdHistorial_RowDataBound">
                        <Columns>
                            <asp:BoundField DataField="nombre" HeaderText="Cliente" ControlStyle-CssClass="center" />
                            <asp:BoundField DataField="valor" HeaderText="Valor" ControlStyle-CssClass="center" />
                            <asp:BoundField DataField="valor2" HeaderText="Valor2" ControlStyle-CssClass="center" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <div class="hidden-xs col-sm-4 col-md-4 col-lg-4"></div>
        </div>
        <div class="row" style="background-color: #e5e5e5; border-radius: 6px; padding: 6px;">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="text-align: center; width: 175%">
                <button id="btnCancelar" type="button" class="form-button cz-form-content-input-button" data-dismiss="modal">Cerrar</button>
            </div>

        </div>

    </form>
</body>
</html>
