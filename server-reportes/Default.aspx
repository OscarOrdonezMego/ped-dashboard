﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="server_reportes._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Pedidos - Dashboard</title>
    <link rel="shortcut icon" href="images/icons/shortcuticon0.ico" />
    <link href="css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.cookie.js" type="text/javascript"></script>
    <script src="js/cz_main.js" type="text/javascript"></script>
    <link href="css/Forms.css" type="text/css" rel="stylesheet" />
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
</head>
  
<body>
    <form id="form1" runat="server" class="cz-main" autocomplete="off">
        <div class="cz-submain cz-login-submain">
            <div id="cz-box-header" class="cz-login-box-header">
                <div id="img_logo_top"></div>
            </div>
            <div id="cz-box-body" class="cz-login-box-body">
                <div class="cz-box-part2">
                    <div id="cz-login-text" class="cz-util-center-box" style="width: 550px;">
                        <p>Bienvenido a Entel.</p>
                        <p>La señal que estabas esperando.</p>
                    </div>
                </div>
                <div class="cz-box-part2">
                    <div id="cz-login-form" class="cz-util-center-box">
                        <div class="cz-login-lat-left">
                            <div class="img"></div>
                        </div>

                        <div class="cz-login-lat-center">
                            <asp:Literal ID="txtmsg" runat="server"></asp:Literal>
                            <div id="cz-login-head">
                                <asp:Image ID="Image1" ImageUrl="~/imagery/login/icons/users.png" runat="server" />
                                <div id="cz-login-title">Bienvenido - Inicie sesión</div>
                            </div>
                            <div id="cz-login-body">
                                <div id="cz-login-user">
                                    <label>Login</label>
                                    <div id="cz-login-input-user" class="cz-login-input">
                                        <asp:TextBox ID="txtUsuario" placeholder="Login" runat="server" MaxLength="10"></asp:TextBox>
                                    </div>
                                </div>
                                <div id="cz-login-pass">
                                    <label>Contraseña</label>
                                    <div id="cz-login-input-pass" class="cz-login-input">
                                        <asp:TextBox ID="txtClave" placeholder="Contraseña" TextMode="Password" runat="server" MaxLength="10"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div id="cz-login-footer">
                                <asp:Button ID="btnIngresar" class="cz-util-slow" runat="server" Text="Ingresar" OnClick="btnIngresar_Click" />
                            </div>
                        </div>
                        <div class="cz-login-lat-right">
                            <div class="img"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="cz-box-footer" class="cz-login-box-footer cz-util-center-text">
                <p>
                    Soluciones de datos / Pedidos - Reportes
                    © Entel Perú. Derechos Reservados.
                    Por favor, sirvase leer nuestro convenio para el uso y privacidad del sitio. 
                </p>
            </div>
        </div>

    </form>
</body>
</html>
