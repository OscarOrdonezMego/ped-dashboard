﻿using Controller;
using Controller.functions;
using Model.bean;
using System;
using System.Web;
using System.Web.UI;

namespace server_reportes
{
    public partial class _Default : Page
    {
        string usr;
        string psw;
        protected void Page_Load(object sender, EventArgs e)
        {
   
            if (Request.Cookies["usr"] != null && Request.Cookies["psw"] != null)
            {
                 usr = Request.Cookies["usr"].Value;
                 psw = Request.Cookies["psw"].Value;
            }

            if(usr!=null && psw != null)
            {

                UsuarioBean bean = UsuarioController.validarUsuario(usr, psw);

                if (bean.codigo != null)
                {
                    Session["lgn_id"] = bean.id;
                    Session["lgn_codigo"] = bean.codigo;
                    Session["lgn_nombre"] = bean.nombre;
                    Session["lgn_perfil"] = bean.perfil;

                    Response.Redirect("Main.aspx");
                }
            }

            string accion = Request["acc"];

            if (accion == null)
            { accion = ""; }

            if (accion.Equals("SES"))
            {
                txtmsg.Text = "<script>window.onload = function() {addnotify(\"alert\", \"Su sesion ha expirado, ingrese nuevamente.\", \"usernregister\");};</script>";
            }

            if (accion.Equals("EXT"))
            {
                txtmsg.Text = "<script>window.onload = function() {addnotify(\"alert\", \"Ha cerrado sesión satistactoriamente.\", \"usernregister\");};</script>";
            }
        }

        protected void btnIngresar_Click(object sender, EventArgs e)
        {
            try
            {
                string login = txtUsuario.Text;
                string clave = txtClave.Text;

 
                UsuarioBean bean = UsuarioController.validarUsuario(login, clave);

                if (bean.codigo != null)
                {
                    Session["lgn_id"] = bean.id;
                    Session["lgn_codigo"] = bean.codigo;
                    Session["lgn_nombre"] = bean.nombre;
                    Session["lgn_perfil"] = bean.perfil;

                    Response.Redirect("Main.aspx");
                }
                else
                {
                    txtmsg.Text = "<script>window.onload = function() {addnotify(\"alert\", \"Usuario o contraseña incorrecta\", \"usernregister\");};</script>";
                }

            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Write(ExceptionUtils.getHtmlErrorPage(ex));
                HttpContext.Current.Response.End();
            }
        }
    }
}