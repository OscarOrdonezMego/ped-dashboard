﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="inicio.aspx.cs" Inherits="server_reportes.inicio" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<head id="Head1" runat="server">
    <script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="js/Validacion.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jsmodule.js" type="text/javascript"></script>
    <link href="css/jquery.autocomplete.css" type="text/css" rel="stylesheet" />
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <link href="css/PopupCalendar.css" type="text/css" rel="stylesheet" />
    <script src="js/PopupCalendar.js" type="text/javascript"></script>
    <link href="css/jquery.timepicker.css" type="text/css" rel="stylesheet" />
    <script src="js/jquery.timepicker.js" type="text/javascript"></script>
    <link href="css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="js/cz_main.js" type="text/javascript"></script>

    <script src="js/highcharts.js"></script>
    <script src="js/highcharts-more.js"></script>
    <script src="js/highcharts-gauge.js"></script>
    <script src="js/highcharts-drilldown.js"></script>
    <script src="js/exporting.js"></script>
    <script>
        var urlRefrescar = "inicio.aspx";
        

        $(document).ready(function () {
    
            $('#fechaefectivos').text(obtenerFecha());
            dibujar();
            
            $("#rbFamilia1").change(function () {
                //Monto Soles
                document.getElementById("divFamilia1").style.display = "none";
                document.getElementById("divFamilia2").style.display = "";
                document.getElementById("divFamilia3").style.display = "none";
                setRadVal("rbFamilia1", "Monto");
                setRadVal("rbFamilia2", "Cantidad");
                setRadVal("rbFamilia3", "Monto");
                setRadVal("rbFamiliaMoneda1", "S");
                setRadVal("rbFamiliaMoneda2", "D");
            });
            $("#rbFamilia2").change(function () {
                //Cantidad
                document.getElementById("divFamilia2").style.display = "none";
                document.getElementById("divFamilia1").style.display = "";
                document.getElementById("divFamilia3").style.display = "none";
                setRadVal("rbFamilia1", "Monto");
                setRadVal("rbFamilia2", "Cantidad");
                setRadVal("rbFamilia3", "Monto");
                setRadVal("rbFamiliaMoneda1", "S");
                setRadVal("rbFamiliaMoneda2", "D");
            });
            $("#rbFamilia3").change(function () {
                //Monto Dolares
                document.getElementById("divFamilia1").style.display = "none";
                document.getElementById("divFamilia2").style.display = "";
                document.getElementById("divFamilia3").style.display = "none";
                setRadVal("rbFamilia1", "Monto");
                setRadVal("rbFamilia2", "Cantidad");
                setRadVal("rbFamilia3", "Monto");
                setRadVal("rbFamiliaMoneda1", "S");
                setRadVal("rbFamiliaMoneda2", "D");
            });
            $("#rbFamiliaMoneda1").change(function () {
                //Monto Soles
                document.getElementById("divFamilia1").style.display = "none";
                document.getElementById("divFamilia2").style.display = "none";
                document.getElementById("divFamilia3").style.display = "";
                setRadVal("rbFamilia1", "Monto");
                setRadVal("rbFamilia2", "Cantidad");
                setRadVal("rbFamilia3", "Monto");
                setRadVal("rbFamiliaMoneda1", "S");
                setRadVal("rbFamiliaMoneda2", "D");
            });
            $("#rbFamiliaMoneda2").change(function () {
                //Monto Dolares
                document.getElementById("divFamilia1").style.display = "";
                document.getElementById("divFamilia2").style.display = "none";
                document.getElementById("divFamilia3").style.display = "none";
                setRadVal("rbFamilia1", "Monto");
                setRadVal("rbFamilia2", "Cantidad");
                setRadVal("rbFamilia3", "Monto");
                setRadVal("rbFamiliaMoneda1", "S");
                setRadVal("rbFamiliaMoneda2", "D");
            });


            $("#rbMarca1").change(function () {
                document.getElementById("divMarca1").style.display = "none";
                document.getElementById("divMarca2").style.display = "";
                document.getElementById("divMarca3").style.display = "none";
                setRadVal("rbMarca1", "Monto");
                setRadVal("rbMarca2", "Cantidad");
                setRadVal("rbMarca3", "Monto");
                setRadVal("rbMarcaMoneda1", "S");
                setRadVal("rbMarcaMoneda2", "D");

            });
            $("#rbMarca2").change(function () {
                document.getElementById("divMarca2").style.display = "none";
                document.getElementById("divMarca1").style.display = "";
                document.getElementById("divMarca3").style.display = "none";
                setRadVal("rbMarca1", "Monto");
                setRadVal("rbMarca2", "Cantidad");
                setRadVal("rbMarca3", "Monto");
                setRadVal("rbMarcaMoneda1", "S");
                setRadVal("rbMarcaMoneda2", "D");
            });
            $("#rbMarca3").change(function () {
                document.getElementById("divMarca1").style.display = "none";
                document.getElementById("divMarca2").style.display = "";
                document.getElementById("divMarca3").style.display = "none";
                setRadVal("rbMarca1", "Monto");
                setRadVal("rbMarca2", "Cantidad");
                setRadVal("rbMarca3", "Monto");
                setRadVal("rbMarcaMoneda1", "S");
                setRadVal("rbMarcaMoneda2", "D");
            });
            $("#rbMarcaMoneda1").change(function () {
                document.getElementById("divMarca1").style.display = "none";
                document.getElementById("divMarca2").style.display = "none";
                document.getElementById("divMarca3").style.display = "";
                setRadVal("rbMarca1", "Monto");
                setRadVal("rbMarca2", "Cantidad");
                setRadVal("rbMarca3", "Monto");
                setRadVal("rbMarcaMoneda1", "S");
                setRadVal("rbMarcaMoneda2", "D");
            });
            $("#rbMarcaMoneda2").change(function () {
                document.getElementById("divMarca1").style.display = "";
                document.getElementById("divMarca2").style.display = "none";
                document.getElementById("divMarca3").style.display = "none";
                setRadVal("rbMarca1", "Monto");
                setRadVal("rbMarca2", "Cantidad");
                setRadVal("rbMarca3", "Monto");
                setRadVal("rbMarcaMoneda1", "S");
                setRadVal("rbMarcaMoneda2", "D");
            });

          /*  $("#rbVendedor1").change(function () {
                document.getElementById("divVendedor1").style.display = "none";
                document.getElementById("divVendedor2").style.display = "";
                setRadVal("rbVendedor1", "Monto");
                setRadVal("rbVendedor2", "Cantidad");

            });

            $("#rbVendedor2").change(function () {
                document.getElementById("divVendedor2").style.display = "none";
                document.getElementById("divVendedor1").style.display = "";
                setRadVal("rbVendedor1", "Monto");
                setRadVal("rbVendedor2", "Cantidad");
            });*/

            $("#rbvendedorh1").change(function () {
                document.getElementById("divvendedorh1").style.display = "none";
                document.getElementById("divvendedorh2").style.display = "";
                setRadVal("rbvendedorh1", "Monto");
                setRadVal("rbvendedorh2", "Cantidad");

            });

            $("#rbvendedorh2").change(function () {
                document.getElementById("divvendedorh2").style.display = "none";
                document.getElementById("divvendedorh1").style.display = "";
                setRadVal("rbvendedorh1", "Monto");
                setRadVal("rbvendedorh2", "Cantidad");
            });

          /*  $("#rbCliente1").change(function () {
                document.getElementById("divCliente1").style.display = "none";
                document.getElementById("divCliente2").style.display = "";
                setRadVal("rbCliente1", "Monto");
                setRadVal("rbCliente2", "Cantidad");
            });

            $("#rbcliente2").change(function () {
                document.getElementById("divClienteh2").style.display = "none";
                document.getElementById("divClienteh1").style.display = "";
                setRadVal("rbcliente1", "Monto");
                setRadVal("rbcliente2", "Cantidad");
            });*/
            $("#rbProductoh1").change(function () {
                document.getElementById("divProductoh1").style.display = "none";
                document.getElementById("divProductoh2").style.display = "";
                setRadVal("rbProductoh1", "Monto");
                setRadVal("rbProductoh2", "Cantidad");
            });

            $("#rbProductoh2").change(function () {
                document.getElementById("divProductoh2").style.display = "none";
                document.getElementById("divProductoh1").style.display = "";
                setRadVal("rbProductoh1", "Monto");
                setRadVal("rbProductoh2", "Cantidad");
            });
            $("#rbclienteh1").change(function () {
                document.getElementById("divClienteh1").style.display = "none";
                document.getElementById("divClienteh2").style.display = "";
                setRadVal("rbclienteh1", "Monto");
                setRadVal("rbclienteh2", "Cantidad");
            });

            $("#rbclienteh2").change(function () {
                document.getElementById("divClienteh2").style.display = "none";
                document.getElementById("divClienteh1").style.display = "";
                setRadVal("rbclienteh1", "Monto");
                setRadVal("rbclienteh2", "Cantidad");
            });
          /*  $("#rbProducto1").change(function () {
                document.getElementById("divProducto1").style.display = "none";
                document.getElementById("divProducto2").style.display = "";
                setRadVal("rbProducto1", "Monto");  
                setRadVal("rbProducto2", "Cantidad");
            });

            $("#rbProducto2").change(function () {
                document.getElementById("divProducto2").style.display = "none";
                document.getElementById("divProducto1").style.display = "";
                setRadVal("rbProducto1", "Monto");
                setRadVal("rbProducto2", "Cantidad");
            });*/
        });

        function obtenerFecha() {
   
                var dateObj = new Date();
                var month = dateObj.getUTCMonth() + 1;
                var day = dateObj.getUTCDate();
                var year = dateObj.getUTCFullYear();

                return newdate = day + "/" + month + "/" + year ;
        }

        function dibujar() {
      //      mostrarEspera();
            var tipoMonto = "M";
            var tipoCantidad = "C";
            var montoSoles = "S";
            var montoDolares = "D";
            topvendedorMontoUrl = 'functions/DibujoTopVendedor.ashx?fechaInicio=' + $('#txtFecInicio').val() + '&fechaFin=' + $("#txtFecFin").val() + '&tipo=' + tipoMonto;
            topvendedorCantidadUrl = 'functions/DibujoTopVendedor.ashx?fechaInicio=' + $('#txtFecInicio').val() + '&fechaFin=' + $("#txtFecFin").val() + '&tipo=' + tipoCantidad;
            topClienteMontoUrl = 'functions/DibujoTopCliente.ashx?fechaInicio=' + $('#txtFecInicio').val() + '&fechaFin=' + $("#txtFecFin").val() + '&tipo=' + tipoMonto;
            topClienteCantidadUrl = 'functions/DibujoTopCliente.ashx?fechaInicio=' + $('#txtFecInicio').val() + '&fechaFin=' + $("#txtFecFin").val() + '&tipo=' + tipoCantidad;
            topProductoMontoUrl = 'functions/DibujoTopProducto.ashx?fechaInicio=' + $('#txtFecInicio').val() + '&fechaFin=' + $("#txtFecFin").val() + '&tipo=' + tipoMonto;
            topProductoCantidadUrl = 'functions/DibujoTopProducto.ashx?fechaInicio=' + $('#txtFecInicio').val() + '&fechaFin=' + $("#txtFecFin").val() + '&tipo=' + tipoCantidad;
            linearUrl = 'functions/DibujoLinear.ashx?fechaInicio=' + $('#txtFecInicio').val() + '&fechaFin=' + $("#txtFecFin").val();
            barraUrl = 'functions/DibujoBarra.ashx?fechaInicio=' + $('#txtFecInicio').val() + '&fechaFin=' + $("#txtFecFin").val();
            tiempoPromedioUrl = 'functions/DibujoTiempoPromedio.ashx?fechaInicio=' + $('#txtFecInicio').val() + '&fechaFin=' + $("#txtFecFin").val();
            pieFamiliaMontoSolesUrl = 'functions/DibujoPieFamilia.ashx?fechaInicio=' + $('#txtFecInicio').val() + '&fechaFin=' + $("#txtFecFin").val() + '&tipo=' + tipoMonto + '&moneda=' + montoSoles;
            pieFamiliaMontoDolaresUrl = 'functions/DibujoPieFamilia.ashx?fechaInicio=' + $('#txtFecInicio').val() + '&fechaFin=' + $("#txtFecFin").val() + '&tipo=' + tipoMonto + '&moneda=' + montoDolares;
            pieFamiliaCantidadUrl = 'functions/DibujoPieFamilia.ashx?fechaInicio=' + $('#txtFecInicio').val() + '&fechaFin=' + $("#txtFecFin").val() + '&tipo=' + tipoCantidad + '&moneda=' + '';
            pieMarcaMontoSolesUrl = 'functions/DibujoPieMarca.ashx?fechaInicio=' + $('#txtFecInicio').val() + '&fechaFin=' + $("#txtFecFin").val() + '&tipo=' + tipoMonto + '&moneda=' + montoSoles;
            pieMarcaMontoDolaresUrl = 'functions/DibujoPieMarca.ashx?fechaInicio=' + $('#txtFecInicio').val() + '&fechaFin=' + $("#txtFecFin").val() + '&tipo=' + tipoMonto + '&moneda=' + montoDolares;
            pieMarcaCantidadUrl = 'functions/DibujoPieMarca.ashx?fechaInicio=' + $('#txtFecInicio').val() + '&fechaFin=' + $("#txtFecFin").val() + '&tipo=' + tipoCantidad + '&moneda=' + '';
          

            $.ajax({
                type: 'POST',
                url: topvendedorMontoUrl,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: PintarGraficoTopVendedorMonto,
                error: function (xhr, status, error) {
                    alert(error);
                }
            });
            $.ajax({
                type: 'POST',
                url: topvendedorCantidadUrl,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: PintarGraficoTopVendedoCantidad,
                error: function (xhr, status, error) {
                    alert(error);
                }
            });
            $.ajax({
                type: 'POST',
                url: topClienteMontoUrl,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: PintarGraficoTopClienteMonto,
                error: function (xhr, status, error) {
                    alert(error);
                }
            });
            $.ajax({
                type: 'POST',
                url: topClienteCantidadUrl,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: PintarGraficoTopClienteCantidad,
                error: function (xhr, status, error) {
                    alert(error);
                }
            });
            $.ajax({
                type: 'POST',
                url: topProductoMontoUrl,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: PintarGraficoTopProductoMonto,
                error: function (xhr, status, error) {
                    alert(error);
                }
            });
            $.ajax({
                type: 'POST',
                url: topProductoCantidadUrl,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: PintarGraficoTopProductoCantidad,
                error: function (xhr, status, error) {
                    alert(error);
                }
            });
            $.ajax({
                type: 'POST',
                url: linearUrl,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: PintarGraficoLinear,
                error: function (xhr, status, error) {
                    alert(error);
                }
            });
           $.ajax({
                type: 'POST',
                url: barraUrl,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: PintarGraficoBarras,
                error: function (xhr, status, error) {
                    alert(error);
                }
            });
            $.ajax({
                type: 'POST',
                url: pieFamiliaMontoSolesUrl,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: PintarGraficoFamiliaMontoSoles,
                error: function (xhr, status, error) {
                    alert(error);
                }
            });
            $.ajax({
                type: 'POST',
                url: pieFamiliaMontoDolaresUrl,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: PintarGraficoFamiliaMontoDolares,
                error: function (xhr, status, error) {
                    alert(error);
                }
            });
            $.ajax({
                type: 'POST',
                url: pieFamiliaCantidadUrl,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: PintarGraficoFamiliaCantidad,
                error: function (xhr, status, error) {
                    alert(error);
                }
            });
            $.ajax({
                type: 'POST',
                url: pieMarcaMontoSolesUrl,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: PintarGraficoMarcaMontoSoles,
                error: function (xhr, status, error) {
                    alert(error);
                }
            });
            $.ajax({
                type: 'POST',
                url: pieMarcaMontoDolaresUrl,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: PintarGraficoMarcaMontoDolares,
                error: function (xhr, status, error) {
                    alert(error);
                }
            });
            $.ajax({
                type: 'POST',
                url: pieMarcaCantidadUrl,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: PintarGraficoMarcaCantidad,
                error: function (xhr, status, error) {
                    alert(error);
                }
            });
      
            $.ajax({
                type: 'POST',
                url: tiempoPromedioUrl,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: PintarGraficoTiempoPromedio,
                error: function (xhr, status, error) {
                    alert(error);
                }
            });
        }
        function PintarGraficoTiempoPromedio(data) {
            $("#loadingimgtiempoefectivo").hide();
            //quitarEspera();
           var tiempo=parseInt(data);
            var cadena;
            if (data > 1) {
                cadena = " minutos";
            } else {
                cadena = " minuto";
            }
            console.log(data);
            $(function () {

                var gaugeOptions = {

                    chart: {
                        type: 'solidgauge'
                    },

                    title: {
                        text: null,

                    },
                    subtitle: {
                        text: 'Tiempo referencial de ' + 20 + ' minutos'
                    },
                    pane: {
                        center: ['50%', '50%'],
                        size: '100%',
                        startAngle: 0,
                        endAngle: 360,
                        background: {
                            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                            innerRadius: '60%',
                            outerRadius: '100%',
                            shape: 'arc',
                            borderWidth: 3,
                        }
                    },

                    tooltip: {
                        enabled: true
                    },

                    // the value axis
                    yAxis: {
                        stops: [
                            [0.1, '#55BF3B'], // green
                            [0.5, '#DDDF0D'], // yellow
                            [0.9, '#DF5353'] // red
                        ],
                        lineWidth: 0,
                        minorTickInterval: null,
                        tickPixelInterval: 400,
                        tickWidth: 0,
                        title: {
                            y: -70
                        },
                        labels: {
                            enabled: false
                        }
                    },

                    plotOptions: {
                        solidgauge: {
                            dataLabels: {
                                y: -10,
                                borderWidth: 0,
                                useHTML: false
                            }
                        }
                    }
                };

                // The speed gauge
                $('#container-tiempo').highcharts(Highcharts.merge(gaugeOptions, {
                    yAxis: {
                        min: 0,
                        max: 20,
                        title: {
                            text: ''
                        }
                    },

                    credits: {
                        enabled: false
                    },

                    series: [{
                        name: 'Promedio ',
                        data: [tiempo],
                        dataLabels: {
                            format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                                ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                                   '</div>'
                        },
                        tooltip: {
                            valueSuffix: cadena
                        }
                    }]

                }));
            });
        }
        
        function PintarGraficoTopProductoMonto(data) {
            $("#loadingimgtopproductomonto").hide();

            var colores_s = ['#7CB5EC', '#434348', '#90ED7D', '#F7A35C', '#8085E9']
            var colores_d = ['#C4DDF7', '#919199', '#CCF7C4', '#FBC99F', '#B5B8F2']

                var arrProductoM = [];
                var arrProductoPedidos = [];
                var objetoProductoPedidos = new Object();
                objetoProductoPedidos.data = [];

                var arrProductoM2 = [];
                var objetoProductoPedidos2 = new Object();
                objetoProductoPedidos2.data = [];

                for (var xi = 0; xi < data.item.length; xi++) {
                    objetoProductoPedidos.data = [];
                    objetoProductoPedidos2.data = [];

                    var objtopproductomonto = {
                        name: data.item[xi].nombre,
                        y: parseFloat(data.item[xi].valor_Soles),
                        drilldown: data.item[xi].pk + '_s',
                        color: colores_s[xi]
                    }
                    arrProductoM.push(objtopproductomonto);
                    objetoProductoPedidos.id = data.item[xi].pk + '_s';

                    for (xa = 0; xa < data.item[xi].ultimastransacciones.length ; xa++) {
                        objetoProductoPedidos.data.push([data.item[xi].ultimastransacciones[xa].fecha, parseFloat(data.item[xi].ultimastransacciones[xa].valorxfecha_Soles)]);
                    }
                    var final = {
                        id: objetoProductoPedidos.id,
                        data: objetoProductoPedidos.data,
                        name: "Monto Soles"
                    }

                    arrProductoPedidos.push(final);


                    //-->
                    var objtopproductomonto2 = {
                        name: data.item[xi].nombre,
                        y: parseFloat(data.item[xi].valor_Dolares),
                        drilldown: data.item[xi].pk + '_d',
                        color: colores_d[xi]
                    }
                    arrProductoM2.push(objtopproductomonto2);
                    objetoProductoPedidos2.id = data.item[xi].pk + '_d';

                    for (xa = 0; xa < data.item[xi].ultimastransacciones.length ; xa++) {
                        objetoProductoPedidos2.data.push([data.item[xi].ultimastransacciones[xa].fecha, parseFloat(data.item[xi].ultimastransacciones[xa].valorxfecha_Dolares)]);
                    }
                    var final2 = {
                        id: objetoProductoPedidos2.id,
                        data: objetoProductoPedidos2.data,
                        name: "Monto Dólares"
                    }

                    arrProductoPedidos.push(final2);
                    //<--
                }

                Highcharts.chart('regionProductoh1', {
                    lang: {
                        drillUpText: 'Retornar'
                    },
                    chart: {
                        type: 'column',
                        inverted: true
                    },
                    title: {
                        text: ''
                    },
                    xAxis: {
                        type: 'category'
                    },

                    legend: {
                        enabled: false
                    },

                    plotOptions: {
                        series: {
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },

                    series: [{
                        name: 'Monto Soles',
                        colorByPoint: true,
                        data: arrProductoM
                    },
                    {
                        name: 'Monto Dólares',
                        colorByPoint: true,
                        data: arrProductoM2
                    }],
                    drilldown: {
                        activeAxisLabelStyle: {
                            textDecoration: 'none',
                            color: '#666',
                        },
                        activeDataLabelStyle: {
                            color: '#666'
                        },
                        series: arrProductoPedidos
                    }
                });

            }
        function PintarGraficoTopProductoCantidad(data) {
            $("#loadingimgtopproductocantidad").hide();
                var arrProductoC = [];
                var arrProductoPedidosC = [];
                var objetoProductoPedidosC = new Object();
                objetoProductoPedidosC.data = [];
                for (var xi = 0; xi < data.item.length; xi++) {
                    objetoProductoPedidosC.data = [];
                    var objtopproductocantidad = {
                        name: data.item[xi].nombre,
                        y: parseFloat(data.item[xi].valor),
                        drilldown: data.item[xi].pk
                    }
                    arrProductoC.push(objtopproductocantidad);
                    objetoProductoPedidosC.id = data.item[xi].pk;

                    for (xa = 0; xa < data.item[xi].ultimastransacciones.length ; xa++) {
                        objetoProductoPedidosC.data.push([data.item[xi].ultimastransacciones[xa].fecha, parseFloat(data.item[xi].ultimastransacciones[xa].valorxfecha)]);
                    }
                    var final = {
                        id: objetoProductoPedidosC.id,
                        data: objetoProductoPedidosC.data,
                        name: "Cantidad"
                    }

                    arrProductoPedidosC.push(final);

                }

                Highcharts.chart('regionProductoh2', {
                    lang: {
                        drillUpText: 'Retornar'
                    },
                    chart: {
                        type: 'column',
                        inverted: true
                    },
                    title: {
                        text: ''
                    },
                    xAxis: {
                        type: 'category'
                    },

                    legend: {
                        enabled: false
                    },

                    plotOptions: {
                        series: {
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },

                    series: [{
                        name: 'Cantidad',
                        colorByPoint: true,
                        data: arrProductoC
                    }],
                    drilldown: {
                        activeAxisLabelStyle: {
                            textDecoration: 'none',
                            color: '#666',
                        },
                        activeDataLabelStyle: {
                            color: '#666'
                        },
                        series: arrProductoPedidosC
                    }
                });

            }
            function PintarGraficoTopClienteMonto(data) {
                $("#loadingimgtopclientemonto").hide();

                var colores_s = ['#7CB5EC', '#434348', '#90ED7D', '#F7A35C', '#8085E9']
                var colores_d = ['#C4DDF7', '#919199', '#CCF7C4', '#FBC99F', '#B5B8F2']

                var arrClienteM = [];
                var arrClientePedidos = [];
                var objetoClientePedidos = new Object();
                objetoClientePedidos.data = [];

                var arrClienteM2 = [];
                var objetoClientePedidos2 = new Object();
                objetoClientePedidos2.data = [];

                for (var xi = 0; xi < data.item.length; xi++) {
                    objetoClientePedidos.data = [];
                    objetoClientePedidos2.data = [];

                    var objtopclientermonto = {
                        name: data.item[xi].nombre,
                        y: parseFloat(data.item[xi].valor_Soles),
                        drilldown: data.item[xi].pk + '_s',
                         color: colores_s[xi]
                    }
                    arrClienteM.push(objtopclientermonto);
                    objetoClientePedidos.id = data.item[xi].pk + '_s';

                    for (xa = 0; xa < data.item[xi].ultimastransacciones.length ; xa++) {
                        objetoClientePedidos.data.push([data.item[xi].ultimastransacciones[xa].fecha, parseFloat(data.item[xi].ultimastransacciones[xa].valorxfecha_Soles)]);
                    }
                    var final = {
                        id: objetoClientePedidos.id,
                        data: objetoClientePedidos.data,
                        name: "Monto Soles"
                    }

                    arrClientePedidos.push(final);

                    //-->
                    var objtopclientermonto2 = {
                        name: data.item[xi].nombre,
                        y: parseFloat(data.item[xi].valor_Dolares),
                        drilldown: data.item[xi].pk + '_d',
                        color: colores_d[xi]
                    }
                    arrClienteM2.push(objtopclientermonto2);
                    objetoClientePedidos2.id = data.item[xi].pk + '_d';

                    for (xa = 0; xa < data.item[xi].ultimastransacciones.length ; xa++) {
                        objetoClientePedidos2.data.push([data.item[xi].ultimastransacciones[xa].fecha, parseFloat(data.item[xi].ultimastransacciones[xa].valorxfecha_Dolares)]);
                    }
                    var final2 = {
                        id: objetoClientePedidos2.id,
                        data: objetoClientePedidos2.data,
                        name: "Monto Dólares"
                    }

                    arrClientePedidos.push(final2);
                    //<--

                }

                Highcharts.chart('regionClienteh1', {
                    lang: {
                        drillUpText: 'Retornar'
                    },
                    chart: {
                        type: 'column',
                        inverted: true
                    },
                    title: {
                        text: ''
                    },
                    xAxis: {
                        type: 'category'
                    },

                    legend: {
                        enabled: false
                    },

                    plotOptions: {
                        series: {
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },

                    series: [{
                        name: 'Monto Soles',
                        colorByPoint: true,
                        data: arrClienteM
                    },
                    {
                        name: 'Monto Dólares',
                        colorByPoint: true,
                        data: arrClienteM2
                    }],
                    drilldown: {
                        activeAxisLabelStyle: {
                            textDecoration: 'none',
                            color: '#666',
                        },
                        activeDataLabelStyle: {
                            color: '#666'
                        },
                        series: arrClientePedidos
                    }
                });
            }
            function PintarGraficoTopClienteCantidad(data) {
                $("#loadingimgtopclientecantidad").hide();
                var arrClienteC = [];
                var arrClientePedidosC = [];
                var objetoClientePedidosC = new Object();
                objetoClientePedidosC.data = [];
                for (var xi = 0; xi < data.item.length; xi++) {
                    objetoClientePedidosC.data = [];
                    var objtopclientecantidad = {
                        name: data.item[xi].nombre,
                        y: parseFloat(data.item[xi].valor),
                        drilldown: data.item[xi].pk
                    }
                    arrClienteC.push(objtopclientecantidad);
                    objetoClientePedidosC.id = data.item[xi].pk;

                    for (xa = 0; xa < data.item[xi].ultimastransacciones.length ; xa++) {
                        objetoClientePedidosC.data.push([data.item[xi].ultimastransacciones[xa].fecha, parseFloat(data.item[xi].ultimastransacciones[xa].valorxfecha)]);
                    }
                    var finalc = {
                        id: objetoClientePedidosC.id,
                        data: objetoClientePedidosC.data,
                        name: "Cantidad"
                    }

                    arrClientePedidosC.push(finalc);

                }

                Highcharts.chart('regionClienteh2', {
                    lang: {
                        drillUpText: 'Retornar'
                    },
                    chart: {
                        type: 'column',
                        inverted: true
                    },
                    title: {
                        text: ''
                    },
                    xAxis: {
                        type: 'category'
                    },

                    legend: {
                        enabled: false
                    },

                    plotOptions: {
                        series: {
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },

                    series: [{
                        name: 'Cantidad',
                        colorByPoint: true,
                        data: arrClienteC
                    }],
                    drilldown: {
                        activeAxisLabelStyle: {
                            textDecoration: 'none',
                            color: '#666',
                        },
                        activeDataLabelStyle: {
                            color: '#666'
                        },
                        series: arrClientePedidosC
                    }
                });

            }

            function PintarGraficoTopVendedorMonto(data) {
                $("#loadingimgtopvendedormonto").hide();

                var colores_s = ['#7CB5EC', '#434348', '#90ED7D', '#F7A35C', '#8085E9']
                var colores_d = ['#C4DDF7', '#919199', '#CCF7C4', '#FBC99F', '#B5B8F2']

                var arrVendedorPedidos = [];

                var arrVendedorM = [];
                var objetoPedidos = new Object();
                objetoPedidos.data = [];

                var arrVendedorM2 = [];
                var objetoPedidos2 = new Object();
                objetoPedidos2.data = [];

                for (var xi = 0; xi < data.item.length; xi++) {
                    objetoPedidos.data = [];
                    objetoPedidos2.data = [];

                    var objtopvendedormonto = {
                        name: data.item[xi].nombre,
                        y: parseFloat(data.item[xi].valor_Soles),
                        drilldown: data.item[xi].pk + '_s',
                        color: colores_s[xi]
                    }
                    arrVendedorM.push(objtopvendedormonto);

                    objetoPedidos.id = data.item[xi].pk + '_s';
                    for (xa = 0; xa < data.item[xi].ultimastransacciones.length ; xa++) {
                        objetoPedidos.data.push([data.item[xi].ultimastransacciones[xa].fecha, parseFloat(data.item[xi].ultimastransacciones[xa].valorxfecha_Soles)]);
                    }
                    var final = {
                        id: objetoPedidos.id,
                        data: objetoPedidos.data,
                        name: "Monto Soles"
                    }
                    arrVendedorPedidos.push(final);

                    //--->
                    var objtopvendedormonto2 = {
                        name: data.item[xi].nombre,
                        y: parseFloat(data.item[xi].valor_Dolares),
                        drilldown: data.item[xi].pk + '_d',
                        color: colores_d[xi]
                    }
                    arrVendedorM2.push(objtopvendedormonto2);

                    objetoPedidos2.id = data.item[xi].pk + '_d';
                    for (xa = 0; xa < data.item[xi].ultimastransacciones.length ; xa++) {
                        objetoPedidos2.data.push([data.item[xi].ultimastransacciones[xa].fecha, parseFloat(data.item[xi].ultimastransacciones[xa].valorxfecha_Dolares)]);
                    }
                    var final2 = {
                        id: objetoPedidos2.id,
                        data: objetoPedidos2.data,
                        name: "Monto Dólares"
                    }
                    arrVendedorPedidos.push(final2);
                    //<---
                }

                Highcharts.chart('regionVendedor1', {
                    lang: {
                        drillUpText: 'Retornar'
                    },
                    chart: {
                        type: 'column',
                        inverted: true
                    },
                    title: {
                        text: ''
                    },
                    xAxis: {
                        type: 'category'
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },
                    series: [{
                        name: 'Monto Soles',
                        colorByPoint: true,
                        data: arrVendedorM
                    },
                    {
                        name: 'Monto Dólares',
                        colorByPoint: true,
                        data: arrVendedorM2
                    }],
                    drilldown:  {
                        activeAxisLabelStyle: {
                            textDecoration: 'none',
                            color: '#666',
                        },
                        activeDataLabelStyle: {
                            color: '#666'
                        },
                        series: arrVendedorPedidos
                    }
                });
            }

            function PintarGraficoTopVendedoCantidad(data) {
                $("#loadingimgtopvendedorcantidad").hide();
                var arrVendedorC = [];
                var arrVendedorPedidosC = [];
                var objetoPedidosC = new Object();
                objetoPedidosC.data = [];
                for (var xi = 0; xi < data.item.length; xi++) {
                    objetoPedidosC.data = [];
                    var objtopvendedormontoC = {
                        name: data.item[xi].nombre,
                        y: parseFloat(data.item[xi].valor),
                        drilldown: data.item[xi].pk
                    }
                    arrVendedorC.push(objtopvendedormontoC);
                    objetoPedidosC.id = data.item[xi].pk;

                    for (xa = 0; xa < data.item[xi].ultimastransacciones.length ; xa++) {
                        objetoPedidosC.data.push([data.item[xi].ultimastransacciones[xa].fecha, parseFloat(data.item[xi].ultimastransacciones[xa].valorxfecha)]);
                    }
                    var finalC = {
                        id: objetoPedidosC.id,
                        data: objetoPedidosC.data,
                        name: "Nro Pedidos"
                    }

                    arrVendedorPedidosC.push(finalC);

                }

                Highcharts.chart('regionVendedor2', {
                    lang: {
                        drillUpText: 'Retornar'
                    },
                    chart: {
                        type: 'column',
                        inverted: true
                    },
                    title: {
                        text: ''
                    },
                    xAxis: {
                        type: 'category'
                    },

                    legend: {
                        enabled: false
                    },

                    plotOptions: {
                        series: {
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },

                    series: [{
                        name: 'Nro Pedidos',
                        colorByPoint: true,
                        data: arrVendedorC
                    }],
                    drilldown: {
                        activeAxisLabelStyle: {
                            textDecoration: 'none',
                            color: '#666',
                        },
                        activeDataLabelStyle: {
                            color: '#666'
                        },
                        series: arrVendedorPedidosC
                    }
                });

            }

            function PintarGraficoLinear(data) {

                $("#loadingimgavanceventa").hide();
                console.log(data);

                var arr = [];
                var arr2 = [];
                var items = data.split("|");
                items.forEach(function (element) {
                    bean = element.split("#");
                    fecha = bean[0].split("/");
                    dia = fecha[0];
                    mes = fecha[1] - 1;
                    ano = fecha[2];
                    valor = bean[1]
                    valor2 = bean[2]
                    arr.push([Date.UTC(ano, mes, dia), parseFloat(valor)]);
                    arr2.push([Date.UTC(ano, mes, dia), parseFloat(valor2)]);
                });

                console.log(arr);

                Highcharts.chart('container', {
                    chart: {
                        type: 'spline'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        type: 'datetime',
                        dateTimeLabelFormats: { // don't display the dummy year
                            month: '%e %b',
                            year: '%b'
                        }
                    },
                    yAxis: {
                        title: {
                            text: ''
                        },
                        min: 0
                    },
                    tooltip: {
                        headerFormat: '<b>{series.name}</b><br>',
                        pointFormat: '{point.x:%e %b}: {point.y:.2f}'
                    },

                    plotOptions: {
                        spline: {
                            marker: {
                                enabled: true
                            }
                        }
                    },

                    series: [{
                        name: 'Ventas Soles',
                        data: arr
                    }
                    ,
                    {
                        name: 'Ventas Dólares',
                        data: arr2
                    }

                    ]
                });

            }

            function PintarGraficoBarras(data) {
                $("#loadingimgpedidoefectivo").hide();
                Highcharts.chart('regiones', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        type: 'category'
                    },
                    yAxis: {
                        title: {
                            text: ''
                        }

                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true,
                                format: '{point.y}'
                            }
                        }
                    },

                    tooltip: {
                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
                    },

                    series: [{
                        name: 'Efectivos',
                        colorByPoint: true,
                        data: data.items
                    }]
                });


                $("#modalGrafic").modal('show');

            }

            function PintarGraficoFamiliaMontoSoles(data) {

                debugger;

                $("#loadingimgfamiliamontosoles").hide();
                var arrFamiliaM = [];
                var arrFamiliaPedidosM = [];
                var objetoFamiliasM = new Object();
                objetoFamiliasM.data = [];
                var montototal = 0;
                var montoxfamilia = 0;
                for (var monto = 0 ; monto < data.items.length; monto++) {
                    montoxfamilia = montoxfamilia + data.items[monto].montocantidadtotal;
                }
                montototal = montoxfamilia.toFixed(2);

                for (var xi = 0; xi < data.items.length; xi++) {
                    objetoFamiliasM.data = [];
                    var objtopvendedormontoC = {
                        name: data.items[xi].name,
                        y: parseFloat(data.items[xi].montocantidadtotal) / 100,
                        drilldown: data.items[xi].pk
                    }
                    arrFamiliaM.push(objtopvendedormontoC);
                    objetoFamiliasM.id = data.items[xi].pk;

                    for (xa = 0; xa < data.items[xi].ultimastransacciones.length ; xa++) {
                        objetoFamiliasM.data.push([data.items[xi].ultimastransacciones[xa].nombre + "(S/." + parseFloat(data.items[xi].ultimastransacciones[xa].valor) + " )", parseFloat(data.items[xi].ultimastransacciones[xa].valor)]);
                    }
                    var finalfm = {
                        id: objetoFamiliasM.id,
                        data: objetoFamiliasM.data,
                        name: "Venta(%)"
                    }

                    arrFamiliaPedidosM.push(finalfm);

                }

                Highcharts.chart('regionFamilia1', {
                    lang: {
                        drillUpText: 'Retornar'
                    },
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: 'Total: S/.' + montototal
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: true
                        }
                    },
                    series: [{
                        name: 'Venta(%)',
                        colorByPoint: true,
                        data: arrFamiliaM
                    }],
                    drilldown: {
                        series: arrFamiliaPedidosM
                    }
                });

            }

            function PintarGraficoFamiliaMontoDolares(data) {

                debugger;

                $("#loadingimgfamiliamontodolares").hide();
                var arrFamiliaM = [];
                var arrFamiliaPedidosM = [];
                var objetoFamiliasM = new Object();
                objetoFamiliasM.data = [];
                var montototal = 0;
                var montoxfamilia = 0;
                for (var monto = 0 ; monto < data.items.length; monto++) {
                    montoxfamilia = montoxfamilia + data.items[monto].montocantidadtotal;
                }
                montototal = montoxfamilia.toFixed(2);

                for (var xi = 0; xi < data.items.length; xi++) {
                    objetoFamiliasM.data = [];
                    var objtopvendedormontoC = {
                        name: data.items[xi].name,
                        y: parseFloat(data.items[xi].montocantidadtotal) / 100,
                        drilldown: data.items[xi].pk
                    }
                    arrFamiliaM.push(objtopvendedormontoC);
                    objetoFamiliasM.id = data.items[xi].pk;

                    for (xa = 0; xa < data.items[xi].ultimastransacciones.length ; xa++) {
                        objetoFamiliasM.data.push([data.items[xi].ultimastransacciones[xa].nombre + "($ " + parseFloat(data.items[xi].ultimastransacciones[xa].valor) + " )", parseFloat(data.items[xi].ultimastransacciones[xa].valor)]);
                    }
                    var finalfm = {
                        id: objetoFamiliasM.id,
                        data: objetoFamiliasM.data,
                        name: "Venta(%)"
                    }

                    arrFamiliaPedidosM.push(finalfm);

                }

                Highcharts.chart('regionFamilia3', {
                    lang: {
                        drillUpText: 'Retornar'
                    },
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: 'Total: $ ' + montototal
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: true
                        }
                    },
                    series: [{
                        name: 'Venta(%)',
                        colorByPoint: true,
                        data: arrFamiliaM
                    }],
                    drilldown: {
                        series: arrFamiliaPedidosM
                    }
                });

            }

           function PintarGraficoFamiliaCantidad(data) {
                $("#loadingimgfamiliacantidad").hide();
                var arrFamiliaC = [];
                var arrFamiliaPedidosC = [];
                var objetoFamiliasC = new Object();
                objetoFamiliasC.data = [];
                var montototal = 0;
                var montocantidad = 0;
                for (var monto = 0 ; monto < data.items.length; monto++) {
                    montocantidad = montocantidad + data.items[monto].montocantidadtotal;
                }
                montototal = montocantidad.toFixed(2)

                for (var xi = 0; xi < data.items.length; xi++) {
                    objetoFamiliasC.data = [];
                    var objtopvendedormontoC = {
                        name: data.items[xi].name,
                        y: parseFloat(data.items[xi].montocantidadtotal) / 100,
                        drilldown: data.items[xi].pk
                    }
                    arrFamiliaC.push(objtopvendedormontoC);
                    objetoFamiliasC.id = data.items[xi].pk;

                    for (xa = 0; xa < data.items[xi].ultimastransacciones.length ; xa++) {
                        objetoFamiliasC.data.push([data.items[xi].ultimastransacciones[xa].nombre + "(" + parseFloat(data.items[xi].ultimastransacciones[xa].valor) + " Cant.)", parseFloat(data.items[xi].ultimastransacciones[xa].valor)]);
                    }
                    var finalfc = {
                        id: objetoFamiliasC.id,
                        data: objetoFamiliasC.data,
                        name: "Cantidades(%)"
                    }

                    arrFamiliaPedidosC.push(finalfc);

                }

                Highcharts.chart('regionFamilia2', {
                    lang: {
                        drillUpText: 'Retornar'
                    },
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: montototal + ' Cant.'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: true
                        }
                    },
                    series: [{
                        name: 'Cantidades(%)',
                        colorByPoint: true,
                        data: arrFamiliaC
                    }],
                    drilldown: {
                        series: arrFamiliaPedidosC
                    }
                });
            }
            function PintarGraficoMarcaMontoSoles(data) {
                $("#loadingimgmarcamontosoles").hide();
                var arrMarcaM = [];
                var arrMarcaPedidosM = [];
                var objetoMarcasM = new Object();
                objetoMarcasM.data = [];
                var montototal = 0;
                var montomarca = 0;
                for (var monto = 0 ; monto < data.items.length; monto++) {
                    montomarca = montomarca + data.items[monto].montocantidadtotal;
                }
                montototal = montomarca.toFixed(2);
                for (var xi = 0; xi < data.items.length; xi++) {
                    objetoMarcasM.data = [];
                    var objtopvendedormontoC = {
                        name: data.items[xi].name,
                        y: parseFloat(data.items[xi].montocantidadtotal) / 100,
                        drilldown: data.items[xi].pk
                    }
                    arrMarcaM.push(objtopvendedormontoC);
                    objetoMarcasM.id = data.items[xi].pk;

                    for (xa = 0; xa < data.items[xi].ultimastransacciones.length ; xa++) {
                        objetoMarcasM.data.push([data.items[xi].ultimastransacciones[xa].nombre + "(S/." + parseFloat(data.items[xi].ultimastransacciones[xa].valor) + " )", parseFloat(data.items[xi].ultimastransacciones[xa].valor)]);
                    }
                    var finalmm = {
                        id: objetoMarcasM.id,
                        data: objetoMarcasM.data,
                        name: "Venta(%)"
                    }

                    arrMarcaPedidosM.push(finalmm);

                }

                Highcharts.chart('regionMarca1', {
                    lang: {
                        drillUpText: 'Retornar'
                    },
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: 'Total: S/.' + montototal
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: true
                        }
                    },
                    series: [{
                        name: 'Venta(%)',
                        colorByPoint: true,
                        data: arrMarcaM
                    }],
                    drilldown: {
                        series: arrMarcaPedidosM
                    }
                });

            }

            function PintarGraficoMarcaMontoDolares(data) {
                $("#loadingimgmarcamontodolares").hide();
                var arrMarcaM = [];
                var arrMarcaPedidosM = [];
                var objetoMarcasM = new Object();
                objetoMarcasM.data = [];
                var montototal = 0;
                var montomarca = 0;
                for (var monto = 0 ; monto < data.items.length; monto++) {
                    montomarca = montomarca + data.items[monto].montocantidadtotal;
                }
                montototal = montomarca.toFixed(2);
                for (var xi = 0; xi < data.items.length; xi++) {
                    objetoMarcasM.data = [];
                    var objtopvendedormontoC = {
                        name: data.items[xi].name,
                        y: parseFloat(data.items[xi].montocantidadtotal) / 100,
                        drilldown: data.items[xi].pk
                    }
                    arrMarcaM.push(objtopvendedormontoC);
                    objetoMarcasM.id = data.items[xi].pk;

                    for (xa = 0; xa < data.items[xi].ultimastransacciones.length ; xa++) {
                        objetoMarcasM.data.push([data.items[xi].ultimastransacciones[xa].nombre + "($ " + parseFloat(data.items[xi].ultimastransacciones[xa].valor) + " )", parseFloat(data.items[xi].ultimastransacciones[xa].valor)]);
                    }
                    var finalmm = {
                        id: objetoMarcasM.id,
                        data: objetoMarcasM.data,
                        name: "Venta(%)"
                    }

                    arrMarcaPedidosM.push(finalmm);

                }

                Highcharts.chart('regionMarca3', {
                    lang: {
                        drillUpText: 'Retornar'
                    },
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: 'Total: $ ' + montototal
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: true
                        }
                    },
                    series: [{
                        name: 'Venta(%)',
                        colorByPoint: true,
                        data: arrMarcaM
                    }],
                    drilldown: {
                        series: arrMarcaPedidosM
                    }
                });

            }

            function PintarGraficoMarcaCantidad(data) {
                $("#loadingimgmarcamonto").hide();
                var arrMarcaC = [];
                var arrMarcaPedidosC = [];
                var objetoMarcasC = new Object();
                objetoMarcasC.data = [];
                var montototal = 0;
                var montocmarcaantidad = 0;
                for (var monto = 0 ; monto < data.items.length; monto++) {
                    montocmarcaantidad = montocmarcaantidad + data.items[monto].montocantidadtotal;
                }
                montototal = montocmarcaantidad.toFixed(2)
                for (var xi = 0; xi < data.items.length; xi++) {
                    objetoMarcasC.data = [];
                    var objtopvendedormontoC = {
                        name: data.items[xi].name,
                        y: parseFloat(data.items[xi].montocantidadtotal) / 100,
                        drilldown: data.items[xi].pk
                    }
                    arrMarcaC.push(objtopvendedormontoC);
                    objetoMarcasC.id = data.items[xi].pk;

                    for (xa = 0; xa < data.items[xi].ultimastransacciones.length ; xa++) {
                        objetoMarcasC.data.push([data.items[xi].ultimastransacciones[xa].nombre + "(" + parseFloat(data.items[xi].ultimastransacciones[xa].valor) + " Cant.)", parseFloat(data.items[xi].ultimastransacciones[xa].valor)]);
                    }
                    var finalmc = {
                        id: objetoMarcasC.id,
                        data: objetoMarcasC.data,
                        name: "Cantidad(%)"
                    }

                    arrMarcaPedidosC.push(finalmc);

                }

                Highcharts.chart('regionMarca2', {
                    lang: {
                        drillUpText: 'Retornar'
                    },
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text:   montototal + 'Cant.'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: true
                        }
                    },
                    series: [{
                        name: 'Cantidades(%)',
                        colorByPoint: true,
                        data: arrMarcaC
                    }],
                    drilldown: {
                        series: arrMarcaPedidosC
                    }
                });

            }

            function irProductoGrupo() {
                mostrarEspera();
                $(location).attr('href', "Producto/Producto.aspx" +
                   "?fechaInicio=" + $("#txtFecInicio").val() +
                   "&fechaFin=" + $("#txtFecFin").val()
                   );
            }

            function irVendedorGrupo() {
                mostrarEspera();
                $(location).attr('href', "Vendedor/Vendedor.aspx" +
                   "?fechaInicio=" + $("#txtFecInicio").val() +
                   "&fechaFin=" + $("#txtFecFin").val()
                   );
            }

            function irClienteGrupo() {
                mostrarEspera();
                $(location).attr('href', "Cliente/Cliente.aspx" +
                   "?fechaInicio=" + $("#txtFecInicio").val() +
                   "&fechaFin=" + $("#txtFecFin").val()
                   );
            }

            function irAvanceGrupo() {
                mostrarEspera();
                $(location).attr('href', "Avance/Avance.aspx" +
                   "?fechaInicio=" + $("#txtFecInicio").val() +
                   "&fechaFin=" + $("#txtFecFin").val()
                   );
            }

            function irEfectivoGrupo() {
                mostrarEspera();
                $(location).attr('href', "Efectivo/Efectivo.aspx" +
                   "?fechaInicio=" + $("#txtFecInicio").val() +
                   "&fechaFin=" + $("#txtFecFin").val()
                   );
            }

            function irPromedioGrupo() {
                mostrarEspera();
                $(location).attr('href', "Promedio/Promedio.aspx" +
                   "?fechaInicio=" + $("#txtFecInicio").val() +
                   "&fechaFin=" + $("#txtFecFin").val()
                   );  
            }

            function irFamiliaGrupo() {
                mostrarEspera();
                $(location).attr('href', "Familia/familia.aspx" +
                   "?fechaInicio=" + $("#txtFecInicio").val() +
                   "&fechaFin=" + $("#txtFecFin").val()
                   );
            
            }
            function irMarcaGrupo() {
                mostrarEspera();
                $(location).attr('href', "Marca/marca.aspx" +
                   "?fechaInicio=" + $("#txtFecInicio").val() +
                   "&fechaFin=" + $("#txtFecFin").val()
                   );

            }
    </script>


    <style>
        #container {
            min-width: 310px;
            max-width: 800px;
            height: 400px;
            margin: 0 auto;
        }
    </style>

</head>
<body class="formularyW">
    <form id="form1" runat="server">

        <div class="cz-submain cz-submain-form-background">
            <div id="cz-form-box">
                <div class="cz-form-box-content">
                    <div class="cz-form-content">
                        <p>Fecha Inicio</p>
                        <asp:TextBox ID="txtFecInicio" class="cz-form-content-input-calendar" MaxLength="10"
                            onkeypress="javascript:fc_Slash(this.id, '/');" onblur="javascript:fc_ValidaFechaOnblur(this.id);"
                            runat="server"></asp:TextBox>
                        <div class="cz-form-content-input-calendar-visible">
                            <div class="cz-form-content-input-calendar-visible-button">
                                <img alt="<>" id="Img1" name="fecini-img" class="form-input-date-image cz-form-content-input-text-calendar"
                                    src="images/icons/calendar.png" />
                            </div>
                        </div>
                    </div>

                    <div class="cz-form-content">
                        <p>Fecha Fin</p>
                        <asp:TextBox ID="txtFecFin" class="cz-form-content-input-calendar" MaxLength="10"
                            onkeypress="javascript:fc_Slash(this.id, '/');" onblur="javascript:fc_ValidaFechaOnblur(this.id);"
                            runat="server"></asp:TextBox>
                        <div class="cz-form-content-input-calendar-visible">
                            <div class="cz-form-content-input-calendar-visible-button">
                                <img alt="<>" id="Img1" name="fecini-img" class="form-input-date-image cz-form-content-input-text-calendar"
                                    src="images/icons/calendar.png" />
                            </div>
                        </div>
                    </div>

                    <div class="cz-form-content cz-util-right cz-util-right-text">
                        <input type="button" id="btnBuscar" class="cz-form-content-input-button form-button" onclick="refrescar()"
                            value="Actualizar" />
                    </div>
                </div>
                <!--TOP VENDEDOR REMODELADO-->
                 <div id="divvendedorh1" class="cz-form-content cz-content-extended" style="height: 360px; width: 350px" runat="server">
                    <div class="cz-form-subcontent" style="background-color: white">
                        <div class="cz-form-subcontent-title">
                            <p runat="server" style="text-align: center; font-size: 15px">Top Vendedor</p>
                        </div>
                   
                       <div id="regionVendedor1"  style="width: 300px; height: 250px; margin: 0 auto">
                             <p  id="loadingimgtopvendedormonto" style="text-align:center;"><img   src="images/Rolling.gif"/><br/>Cargando..</p>
                       </div>
               
                        <div style="width: 50%; float: left; background-color: white">
                            <asp:RadioButtonList ID="rbvendedorh1" runat="server">
                                <asp:ListItem Text="Monto total de Venta" Value="M" />
                                <asp:ListItem Text="Cantidad de Pedidos" Value="C" />
                            </asp:RadioButtonList>
                        </div>
                        <div style="width: 50%; float: left; background-color: white">
                            <input type="button" id="bDetVendedorh1" class="cz-form-content-input-button form-button" onclick="irVendedorGrupo()"
                                value="Detalle" />
                        </div>

                    </div>
                </div>

                <div id="divvendedorh2" class="cz-form-content cz-content-extended" style="height: 360px; width: 350px; display: none" runat="server">
                    <div class="cz-form-subcontent" style="background-color: white">
                        <div class="cz-form-subcontent-title">
                            <p runat="server" style="text-align: center; font-size: 15px">Top Vendedor</p>
                        </div>

                        <div id="regionVendedor2" style="width: 300px; height: 250px; margin: 0 auto">
                             <p  id="loadingimgtopvendedorcantidad" style="text-align:center;"><img   src="images/Rolling.gif"/><br/>Cargando..</p>
                        </div>

                        <div style="width: 50%; float: left; background-color: white">
                            <asp:RadioButtonList ID="rbvendedorh2" runat="server">
                                <asp:ListItem Text="Monto  total de Venta" Value="M" />
                                <asp:ListItem Text="Cantidad de Pedidos" Value="C" />
                            </asp:RadioButtonList>
                        </div>
                        <div style="width: 50%; float: left; background-color: white">
                            <input type="button" id="bDetVendedorhh2" class="cz-form-content-input-button form-button" onclick="irVendedorGrupo()"
                                value="Detalle" />
                        </div>

                    </div>
                </div>
                <!---->
                <!--TOP CLIENTE REMODELADO-->
                 <div id="divClienteh1" class="cz-form-content cz-content-extended" style="height: 360px; width: 350px" runat="server">
                    <div class="cz-form-subcontent" style="background-color: white">
                        <div class="cz-form-subcontent-title">
                            <p runat="server" style="text-align: center; font-size: 15px">Top Cliente</p>
                        </div>
                        <div id="regionClienteh1"  style="width: 300px; height: 250px; margin: 0 auto">
                            <p  id="loadingimgtopclientemonto" style="text-align:center;"><img   src="images/Rolling.gif"/><br/>Cargando..</p>
                        </div>
                        <div style="width: 50%; float: left; background-color: white">
                            <asp:RadioButtonList ID="rbclienteh1" runat="server">
                                <asp:ListItem Text="Monto total de Importe" Value="M" />
                                <asp:ListItem Text="Cantidad de Productos" Value="C" />
                            </asp:RadioButtonList>
                        </div>
                        <div style="width: 50%; float: left; background-color: white">
                            <input type="button" id="bDetClienteh1" class="cz-form-content-input-button form-button" onclick="irClienteGrupo()"
                                value="Detalle" />
                        </div>
                    </div>
                </div>

                <div id="divClienteh2" class="cz-form-content cz-content-extended" style="height: 360px; width: 350px; display: none" runat="server">
                    <div class="cz-form-subcontent" style="background-color: white">
                        <div class="cz-form-subcontent-title">
                            <p runat="server" style="text-align: center; font-size: 15px">Top Cliente</p>
                        </div>
                        <div id="regionClienteh2" style="width: 300px; height: 250px; margin: 0 auto">
                             <p  id="loadingimgtopclientecantidad" style="text-align:center;"><img   src="images/Rolling.gif"/><br/>Cargando..</p>
                        </div>
                        <div style="width: 50%; float: left; background-color: white">
                            <asp:RadioButtonList ID="rbclienteh2" runat="server">
                                <asp:ListItem Text="Monto  total de Importe" Value="M" />
                                <asp:ListItem Text="Cantidad de Productos" Value="C" />
                            </asp:RadioButtonList>
                        </div>
                        <div style="width: 50%; float: left; background-color: white">
                            <input type="button" id="bDetClienteh2" class="cz-form-content-input-button form-button" onclick="irClienteGrupo()"
                                value="Detalle" />
                        </div>
                    </div>
                </div>
                <!---->
                <!-- TOP PRODUCTOS-->
                   <div id="divProductoh1" class="cz-form-content cz-content-extended" style="height: 360px; width: 350px;" runat="server">
                    <div class="cz-form-subcontent" style="background-color: white">
                        <div class="cz-form-subcontent-title">
                            <p runat="server" style="text-align: center; font-size: 15px">Top Producto</p>
                        </div>

                       <div id="regionProductoh1"  style="width: 300px; height: 250px; margin: 0 auto">
                         <p  id="loadingimgtopproductomonto" style="text-align:center;"><img   src="images/Rolling.gif"/><br/>Cargando..</p>
                       </div>

                        <div style="width: 50%; float: left; background-color: white">
                            <asp:RadioButtonList ID="rbProductoh1" runat="server">
                                <asp:ListItem Text="Monto total de Venta" Value="M" />
                                <asp:ListItem Text="Cantidad de items" Value="C" />
                            </asp:RadioButtonList>
                        </div>
                        <div style="width: 50%; float: left; background-color: white">
                            <input type="button" id="bDetProductoh1" class="cz-form-content-input-button form-button" onclick="irProductoGrupo()"
                                value="Detalle" />
                        </div>

                    </div>
                </div>

                <div id="divProductoh2" class="cz-form-content cz-content-extended" style="height: 360px; width: 350px; display: none" runat="server">
                    <div class="cz-form-subcontent" style="background-color: white">
                        <div class="cz-form-subcontent-title">
                            <p runat="server" style="text-align: center; font-size: 15px">Top Producto</p>
                        </div>

                        <div id="regionProductoh2" style="width: 300px; height: 250px; margin: 0 auto">
                              <p  id="loadingimgtopproductocantidad" style="text-align:center;"><img   src="images/Rolling.gif"/><br/>Cargando..</p>
                        </div>

                        <div style="width: 50%; float: left; background-color: white">
                            <asp:RadioButtonList ID="rbProductoh2" runat="server">
                                <asp:ListItem Text="Monto  total de Venta" Value="M" />
                                <asp:ListItem Text="Cantidad  de items" Value="C" />
                            </asp:RadioButtonList>
                        </div>
                        <div style="width: 50%; float: left; background-color: white">
                            <input type="button" id="bDetProductoh2" class="cz-form-content-input-button form-button" onclick="irProductoGrupo()"
                                value="Detalle" />
                        </div>

                    </div>
                </div>

                <!---->
                <div id="divVendedor1" class="cz-form-content cz-content-extended" style="height: 360px; width: 350px;  display: none" runat="server">
                    <div class="cz-form-subcontent" style="background-color: white">
                        <div class="cz-form-subcontent-title">
                            <p runat="server" style="text-align: center; font-size: 15px">Top Vendedor</p>
                        </div>

                        <div align="center" style="height: 250px">
                            <asp:Literal ID="ltrVendedor1" runat="server"></asp:Literal>
                        </div>

                        <div style="width: 50%; float: left; background-color: white">
                            <asp:RadioButtonList ID="rbVendedor1" runat="server">
                                <asp:ListItem Text="Monto total de Venta" Value="M" />
                                <asp:ListItem Text="Cantidad de Pedidos" Value="C" />
                            </asp:RadioButtonList>
                        </div>
                        <div style="width: 50%; float: left; background-color: white">
                            <input type="button" id="bDetVendedor1" class="cz-form-content-input-button form-button" onclick="irVendedorGrupo()"
                                value="Detalle" />
                        </div>

                    </div>
                </div>

                <div id="divVendedor2" class="cz-form-content cz-content-extended" style="height: 360px; width: 350px; display: none" runat="server">
                    <div class="cz-form-subcontent" style="background-color: white">
                        <div class="cz-form-subcontent-title">
                            <p runat="server" style="text-align: center; font-size: 15px">Top Vendedor</p>
                        </div>

                        <div align="center" style="height: 250px">
                            <asp:Literal ID="ltrVendedor2" runat="server"></asp:Literal>
                        </div>

                        <div style="width: 50%; float: left; background-color: white">
                            <asp:RadioButtonList ID="rbVendedor2" runat="server">
                                <asp:ListItem Text="Monto  total de Venta" Value="M" />
                                <asp:ListItem Text="Cantidad de Pedidos" Value="C" />
                            </asp:RadioButtonList>
                        </div>
                        <div style="width: 50%; float: left; background-color: white">
                            <input type="button" id="bDetVendedor2" class="cz-form-content-input-button form-button" onclick="irVendedorGrupo()"
                                value="Detalle" />
                        </div>

                    </div>
                </div>

                <div id="divCliente1" class="cz-form-content cz-content-extended" style="height: 360px; width: 350px ;  display: none">
                    <div class="cz-form-subcontent" style="background-color: white">
                        <div class="cz-form-subcontent-title">
                            <p runat="server" style="text-align: center; font-size: 15px">Top Clientes</p>
                        </div>

                        <div align="center" style="height: 250px">
                            <asp:Literal ID="ltrCliente1" runat="server"></asp:Literal>
                        </div>

                        <div style="width: 50%; float: left; background-color: white">
                            <asp:RadioButtonList ID="rbCliente1" runat="server">
                                <asp:ListItem Text="Monto total de importe" Value="M" />
                                <asp:ListItem Text="Cantidad de productos" Value="C" />
                            </asp:RadioButtonList>
                        </div>
                        <div style="width: 50%; float: left; background-color: white">
                            <input type="button" id="bDetCliente1" class="cz-form-content-input-button form-button" onclick="irClienteGrupo()"
                                value="Detalle" />
                        </div>

                    </div>
                </div>

                <div id="divCliente2" class="cz-form-content cz-content-extended" style="height: 360px; width: 350px; display: none">
                    <div class="cz-form-subcontent" style="background-color: white">
                        <div class="cz-form-subcontent-title">
                            <p runat="server" style="text-align: center; font-size: 15px">Top Clientes</p>
                        </div>

                        <div align="center" style="height: 250px">
                            <asp:Literal ID="ltrCliente2" runat="server"></asp:Literal>
                        </div>

                        <div style="width: 50%; float: left; background-color: white">
                            <asp:RadioButtonList ID="rbCliente2" runat="server">
                                <asp:ListItem Text="Monto total de importe" Value="M" />
                                <asp:ListItem Text="Cantidad de productos" Value="C" />
                            </asp:RadioButtonList>
                        </div>
                        <div style="width: 50%; float: left; background-color: white">
                            <input type="button" id="bDetCliente2" class="cz-form-content-input-button form-button" onclick="irClienteGrupo()"
                                value="Detalle" />
                        </div>

                    </div>
                </div>

                <div id="divProducto1" class="cz-form-content cz-content-extended" style="height: 360px; width: 350px  ; display: none">
                    <div class="cz-form-subcontent" style="background-color: white">
                        <div class="cz-form-subcontent-title">
                            <p runat="server" style="text-align: center; font-size: 15px">Top Productos</p>
                        </div>

                        <div align="center" style="height: 250px">
                            <asp:Literal ID="ltrProducto1" runat="server"></asp:Literal>
                        </div>

                        <div style="width: 50%; float: left; background-color: white">
                            <asp:RadioButtonList ID="rbProducto1" runat="server">
                                <asp:ListItem Text="Monto total de Venta" Value="M" />
                                <asp:ListItem Text="Cantidad de items" Value="C" />
                            </asp:RadioButtonList>
                        </div>
                        <div style="width: 50%; float: left; background-color: white">
                            <input type="button" id="bDetProducto1" class="cz-form-content-input-button form-button" onclick="irProductoGrupo()"
                                value="Detalle" />
                        </div>

                    </div>
                </div>

                <div id="divProducto2" class="cz-form-content cz-content-extended" style="height: 360px; width: 350px; display: none">
                    <div class="cz-form-subcontent" style="background-color: white">
                        <div class="cz-form-subcontent-title">
                            <p runat="server" style="text-align: center; font-size: 15px">Top Productos</p>
                        </div>

                        <div align="center" style="height: 250px">
                            <asp:Literal ID="ltrProducto2" runat="server"></asp:Literal>
                        </div>

                        <div style="width: 50%; float: left; background-color: white">
                            <asp:RadioButtonList ID="rbProducto2" runat="server">
                                <asp:ListItem Text="Monto total de Venta" Value="M" />
                                <asp:ListItem Text="Cantidad de items" Value="C" />
                            </asp:RadioButtonList>
                        </div>
                        <div style="width: 50%; float: left; background-color: white">
                            <input type="button" id="bDetProducto2" class="cz-form-content-input-button form-button" onclick="irProductoGrupo()"
                                value="Detalle" />
                        </div>

                    </div>
                </div>
                <!--FAMILIA Y MARCA-->
               
                <!---->

                <!---->

                <div class="cz-form-content cz-content-extended" style="height: 360px; width: 350px">
                    <div class="cz-form-subcontent" style="background-color: white">
                        <div class="cz-form-subcontent-title">
                            <p runat="server" style="text-align: center; font-size: 15px">Avance Ventas</p>
                        </div>
                        <div id="container" style="width: 150px; height: 250px; margin: 0 auto">
                              <p  id="loadingimgavanceventa" style="text-align:center;"><img   src="images/Rolling.gif"/><br/>Cargando..</p>
                        </div>

                        <div style="width: 100%; float: left; background-color: white">
                            <input type="button" id="bDetAvance" class="cz-form-content-input-button form-button" onclick="irAvanceGrupo()"
                                value="Detalle" />
                        </div>

                    </div>
                </div>

                <div class="cz-form-content cz-content-extended" style="height: 360px; width: 350px">
                    <div class="cz-form-subcontent" style="background-color: white">
                        <div class="cz-form-subcontent-title">
                            <p runat="server" style="text-align: center; font-size: 15px">Pedidos Efectivos</p>
                              <p id="fechaefectivos" ALIGN=center></p>
                        </div>
                        <div id="regiones" style="width: 150px; height: 250px; margin: 0 auto">
                             <p  id="loadingimgpedidoefectivo" style="text-align:center;"><img   src="images/Rolling.gif"/><br/>Cargando..</p>
                        </div>
                        <div style="width: 100%; float: right; background-color: white">
                            <input type="button" id="bDetEfectivos" class="cz-form-content-input-button form-button" onclick="irEfectivoGrupo()"
                                value="Detalle" />
                        </div>
                    </div>
                </div>


                <div class="cz-form-content cz-content-extended" style="height: 360px; width: 350px">
                    <div class="cz-form-subcontent" style="background-color: white">
                        <div class="cz-form-subcontent-title">
                            <p runat="server" style="text-align: center; font-size: 15px">Tiempo Efectivo Promedio</p>
                        </div>

                        <div align="center" style="height: 250px;">
                            <asp:Literal ID="ltPromedio" runat="server"></asp:Literal>
                           	<div id="container-tiempo" style="width: 150px; height: 250px; margin: 0 auto">
                              <p  id="loadingimgtiempoefectivo" style="text-align:center;"><img   src="images/Rolling.gif"/><br/>Cargando..</p>
                           	</div>
                        </div>
                         <div style="width: 100%; float: right; background-color: white">
                        <input type="button" id="bDetPromedio" class="cz-form-content-input-button form-button" onclick="irPromedioGrupo()"
                            value="Detalle" /></div>

                    </div>
                </div>
    
                 <div id="divFamilia1" class="cz-form-content cz-content-extended" style="height: 520px; width: 525px" runat="server">
                    <div class="cz-form-subcontent" style="background-color: white">
                        <div class="cz-form-subcontent-title">
                            <p runat="server" style="text-align: center; font-size: 15px">Reporte Ventas Familia Productos</p>
                        </div>
                   
                            
                 <div id="regionFamilia1" style="width: 310px; height: 400px; margin: 0 auto">
                     <p  id="loadingimgfamiliamontosoles" style="text-align:center;"><img   src="images/Rolling.gif"/><br/>Cargando..</p>
                 </div>

                        <div style="width: 50%; float: left; background-color: white">
                            <asp:RadioButtonList ID="rbFamilia1" runat="server">
                                <asp:ListItem Text="Monto de Venta" Value="M" />
                                <asp:ListItem Text="Cantidad de Productos" Value="C" />
                            </asp:RadioButtonList>
                        </div>
                        <div style="width: 25%; float: left; background-color: white">
                            <input type="button" id="bDetFamilia1" class="cz-form-content-input-button form-button" onclick="irFamiliaGrupo()"
                                value="Detalle" />
                        </div>
                        <div style="width: 25%; float: left; background-color: white">
                            <asp:RadioButtonList ID="rbFamiliaMoneda1" runat="server">
                                <asp:ListItem Text="Soles" Value="S" />
                                <asp:ListItem Text="Dólares" Value="D" />
                            </asp:RadioButtonList>
                        </div>
                    </div>
                </div>

                <div id="divFamilia2" class="cz-form-content cz-content-extended" style="height: 520px; width: 525px;  display: none" runat="server">
                    <div class="cz-form-subcontent" style="background-color: white">
                        <div class="cz-form-subcontent-title">
                            <p runat="server" style="text-align: center; font-size: 15px">Reporte Ventas Familia Productos</p>
                        </div>

                     <div id="regionFamilia2" style="width: 310px; height: 400px; margin: 0 auto">
                        <p  id="loadingimgfamiliacantidad" style="text-align:center;"><img   src="images/Rolling.gif"/><br/>Cargando..</p>
                     </div>

                        <div style="width: 50%; float: left; background-color: white">
                            <asp:RadioButtonList ID="rbFamilia2" runat="server">
                                <asp:ListItem Text="Monto de Venta" Value="M" />
                                <asp:ListItem Text="Cantidad de Productos" Value="C" />
                            </asp:RadioButtonList>
                        </div>
                        <div style="width: 50%; float: left; background-color: white">
                            <input type="button" id="bDetFamilia2" class="cz-form-content-input-button form-button" onclick="irFamiliaGrupo()"
                                value="Detalle" />
                        </div>

                    </div>
                </div>

                <div id="divFamilia3" class="cz-form-content cz-content-extended" style="height: 520px; width: 525px; display: none" runat="server">
                    <div class="cz-form-subcontent" style="background-color: white">
                        <div class="cz-form-subcontent-title">
                            <p runat="server" style="text-align: center; font-size: 15px">Reporte Ventas Familia Productos</p>
                        </div>
                   
                            
                 <div id="regionFamilia3" style="width: 310px; height: 400px; margin: 0 auto">
                     <p  id="loadingimgfamiliamontodolares" style="text-align:center;"><img   src="images/Rolling.gif"/><br/>Cargando..</p>
                 </div>

                        <div style="width: 50%; float: left; background-color: white">
                            <asp:RadioButtonList ID="rbFamilia3" runat="server">
                                <asp:ListItem Text="Monto de Venta" Value="M" />
                                <asp:ListItem Text="Cantidad de Productos" Value="C" />
                            </asp:RadioButtonList>
                        </div>
                        <div style="width: 25%; float: left; background-color: white">
                            <input type="button" id="bDetFamilia3" class="cz-form-content-input-button form-button" onclick="irFamiliaGrupo()"
                                value="Detalle" />
                        </div>
                        <div style="width: 25%; float: left; background-color: white">
                            <asp:RadioButtonList ID="rbFamiliaMoneda2" runat="server">
                                <asp:ListItem Text="Soles" Value="S" />
                                <asp:ListItem Text="Dólares" Value="D" />
                            </asp:RadioButtonList>
                        </div>
                    </div>
                </div>

                <div id="divMarca1" class="cz-form-content cz-content-extended" style="height: 520px; width: 525px" runat="server">
                    <div class="cz-form-subcontent" style="background-color: white">
                        <div class="cz-form-subcontent-title">
                            <p runat="server" style="text-align: center; font-size: 15px">Reporte Ventas Marca Productos</p>
                        </div>

                       <div id="regionMarca1" style="width: 310px; height: 400px; margin: 0 auto">
                             <p  id="loadingimgmarcamontosoles" style="text-align:center;"><img   src="images/Rolling.gif"/><br/>Cargando..</p>
                       </div>

                        <div style="width: 50%; float: left; background-color: white">
                            <asp:RadioButtonList ID="rbMarca1" runat="server">
                                <asp:ListItem Text="Monto de Venta" Value="M" />
                                <asp:ListItem Text="Cantidad de Productos" Value="C" />
                            </asp:RadioButtonList>
                        </div>
                        <div style="width: 25%; float: left; background-color: white">
                            <input type="button" id="bDetMarca1" class="cz-form-content-input-button form-button" onclick="irMarcaGrupo()"
                                value="Detalle" />
                        </div>
                        <div style="width: 25%; float: left; background-color: white">
                            <asp:RadioButtonList ID="rbMarcaMoneda1" runat="server">
                                <asp:ListItem Text="Soles" Value="S" />
                                <asp:ListItem Text="Dólares" Value="D" />
                            </asp:RadioButtonList>
                        </div>
                    </div>
                </div>

                <div id="divMarca2" class="cz-form-content cz-content-extended" style="height: 520px; width: 525px; display: none" runat="server">
                    <div class="cz-form-subcontent" style="background-color: white">
                        <div class="cz-form-subcontent-title">
                            <p runat="server" style="text-align: center; font-size: 15px">Reporte Ventas Marca Productos</p>
                        </div>

                     <div id="regionMarca2" style="width: 310px; height: 400px; margin: 0 auto">
                         <p  id="loadingimgmarcacantidad" style="text-align:center;"><img   src="images/Rolling.gif"/><br/>Cargando..</p>
                     </div>

                        <div style="width: 50%; float: left; background-color: white">
                            <asp:RadioButtonList ID="rbMarca2" runat="server">
                                <asp:ListItem Text="Monto de Venta" Value="M" />
                                <asp:ListItem Text="Cantidad de Productos" Value="C" />
                            </asp:RadioButtonList>
                        </div>
                        <div style="width: 50%; float: left; background-color: white">
                            <input type="button" id="bDetMarca2" class="cz-form-content-input-button form-button" onclick="irMarcaGrupo()"
                                value="Detalle" />
                        </div>

                    </div>
                </div>


                <div id="divMarca3" class="cz-form-content cz-content-extended" style="height: 520px; width: 525px; display:none" runat="server">
                    <div class="cz-form-subcontent" style="background-color: white">
                        <div class="cz-form-subcontent-title">
                            <p runat="server" style="text-align: center; font-size: 15px">Reporte Ventas Marca Productos</p>
                        </div>

                       <div id="regionMarca3" style="width: 310px; height: 400px; margin: 0 auto">
                             <p  id="loadingimgmarcamontodolares" style="text-align:center;"><img   src="images/Rolling.gif"/><br/>Cargando..</p>
                       </div>

                        <div style="width: 50%; float: left; background-color: white">
                            <asp:RadioButtonList ID="rbMarca3" runat="server">
                                <asp:ListItem Text="Monto de Venta" Value="M" />
                                <asp:ListItem Text="Cantidad de Productos" Value="C" />
                            </asp:RadioButtonList>
                        </div>
                        <div style="width: 25%; float: left; background-color: white">
                            <input type="button" id="bDetMarca3" class="cz-form-content-input-button form-button" onclick="irMarcaGrupo()"
                                value="Detalle" />
                        </div>
                        <div style="width: 25%; float: left; background-color: white">
                            <asp:RadioButtonList ID="rbMarcaMoneda2" runat="server">
                                <asp:ListItem Text="Soles" Value="S" />
                                <asp:ListItem Text="Dólares" Value="D" />
                            </asp:RadioButtonList>
                        </div>
                    </div>
                </div>


            </div>
        </div>

        <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" runat="server"
            aria-hidden="true">
        </div>
        <div id="calendarwindow" class="calendar-window">
        </div>
    </form>
</body>
</html>
