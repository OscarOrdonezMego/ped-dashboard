﻿function alertHtml(tipo, error, titulo) {
    var strHtml = '';
    if (tipo == "delConfirm") {
        strHtml = '<div class="modal-header">';
        strHtml = strHtml + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
        strHtml = strHtml + '<h3 id="H2">Alerta</h3></div>';
        strHtml = strHtml + '<div class="modal-body">';
        strHtml = strHtml + '<img src="../../images/alert/ico_alert.png" style="float: left;height: 32px;"/> <p style="float: left;line-height: 32px;margin-left: 10px!important;">' + titulo + '</p></div>';
        strHtml = strHtml + '<div class="modal-footer">';
        strHtml = strHtml + '<button class="form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true" style="margin-right: 10px;">NO</button>'
        strHtml = strHtml + '<button class="btnDelSi form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true">SI</button></div>';
    }
    else if (tipo == "delConfirmFalta") {
        strHtml = '<div class="modal-header">';
        strHtml = strHtml + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
        strHtml = strHtml + '<h3 id="H1">Alerta</h3></div>';
        strHtml = strHtml + '<div class="modal-body">';
        strHtml = strHtml + '<img src="../../images/alert/ico_alert.png" style="float: left;height: 32px;"/> <p style="float: left;line-height: 32px;margin-left: 10px!important;">  Seleccione por lo menos un item.</p></div>';
        strHtml = strHtml + '<div class="modal-footer">';
        strHtml = strHtml + '<button class="form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true">CERRAR</button></div>';
    }
    else if (tipo == "resEmergencia") {
        strHtml = '<div class="modal-header">';
        strHtml = strHtml + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
        strHtml = strHtml + '<h3 id="H1">Alerta</h3></div>';
        strHtml = strHtml + '<div class="modal-body">';
        strHtml = strHtml + '<img src="../images/alert/ico_alert.png" style="float: left;height: 32px;"/> <p style="float: left;line-height: 32px;margin-left: 10px!important;">  Seleccione por lo menos un item.</p></div>';
        strHtml = strHtml + '<div class="modal-footer">';
        strHtml = strHtml + '<button class="form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true">CERRAR</button></div>';
    }
    else if (tipo == "error") {
        strHtml = '<div class="modal-header">';
        strHtml = strHtml + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
        strHtml = strHtml + '<h3 id="H1">Error</h3></div>';
        strHtml = strHtml + '<div class="modal-body">';
        strHtml = strHtml + error + '</div>';
        strHtml = strHtml + '<div class="modal-footer">';
        strHtml = strHtml + '<button class="form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true">CERRAR</button></div>';
    }

    else if (tipo == "errorCarga") {
        strHtml = '<div class="modal-header">';
        strHtml = strHtml + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
        strHtml = strHtml + '<h3 id="H1">' + titulo + '</h3></div>';
        strHtml = strHtml + '<div class="modal-body">';
        strHtml = strHtml + error + '</div>';
        strHtml = strHtml + '<div class="modal-footer">';
        strHtml = strHtml + '<button class="form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true">CERRAR</button></div>';
    }



    else if (tipo == "alertValidacion") {
        strHtml = '<div class="modal-header">';
        strHtml = strHtml + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
        strHtml = strHtml + '<h3 id="H1">Alerta</h3></div>';
        strHtml = strHtml + '<div class="modal-body">';
        strHtml = strHtml + '<img src="../../images/alert/ico_alert.png" style="float: left;height: 32px;"/> <p style="float: left;margin-top: 6px!important;margin-left: 7px!important;">' + error + '</div>';
        strHtml = strHtml + '<div class="modal-footer">';
        strHtml = strHtml + '<button class="form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true">CERRAR</button></div>';
    }

    return strHtml;

}

/*Elimina Registros*/
/*function deleteReg() {*/
function deleteReg(bParamAdic) {
    if (bParamAdic == undefined || bParamAdic == null)
        bParamAdic = false;
    else
        bParamAdic = bParamAdic;
    /**/
    $('.delReg').click(function (e) {
        var strReg = "";

        $('#divGridView input:checked').each(function () {
            if (this.name != "chkSelectAll")
            { strReg = strReg + this.value + "|"; }
        });

        if (strReg != "") {
            if ($('.delReg').val() == "Restaurar") {
                $('#myModal').html(alertHtml('delConfirm', "", "¿Esta seguro que desea <b>restaurar</b> los items seleccionados?"));
            } else {
                $('#myModal').html(alertHtml('delConfirm', "", "¿Esta seguro que desea <b>eliminar</b> los items seleccionados?"));
            }
            $('#myModal').modal('show');

            $('.btnDelSi').click(function (e) {

                var strReg = "";

                $('#divGridView input:checked').each(function () {
                    if (this.name != "chkSelectAll")
                    { strReg = strReg + this.value + "|"; }
                });
                var strData = new Object();
                strData.codigos = strReg;
                strData.flag = $('#chkflag').attr("checked") ? 'F' : 'T';

                $.ajax({
                    type: 'POST',
                    url: urldel,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    data: JSON.stringify(strData),
                    success: function (data) {
                        $('#buscar').trigger("click");
                    },
                    error: function (xhr, status, error) {
                        $("#myModal").html(alertHtml('error', xhr.responseText));
                        $('#myModal').modal('show');
                    }
                });
            });

        }
        else {
            $('#myModal').html(alertHtml('delConfirmFalta'));
            $('#myModal').modal('show');
        }


    });

    $('.delItemReg').live('click', function (e) {



        var strReg = $(this).attr('cod')
        if ($('.delReg').val() == "Restaurar") {
            $('#myModal').html(alertHtml('delConfirm', "", "¿Esta seguro que desea <b>restaurar</b> este registro?"));
        } else {
            $('#myModal').html(alertHtml('delConfirm', "", "¿Esta seguro que desea <b>eliminar</b> este registro?"));
        }
        $('#myModal').modal('show');

        $('.btnDelSi').click(function (e) {

            var strData = new Object();
            strData.codigos = strReg;
            strData.flag = $('#chkflag').attr("checked") ? 'F' : 'T';

            $.ajax({
                type: 'POST',
                url: urldel,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                data: JSON.stringify(strData),
                success: function (data) {
                    $('#buscar').trigger("click");
                    /*****************************/
                    if (bParamAdic)
                        window.location.reload();
                    /****************************/
                },
                error: function (xhr, status, error) {
                    $("#myModal").html(alertHtml('error', xhr.responseText));
                    $('#myModal').modal('show');
                }
            });
        });

    });

}


/**Para resolver emergencias*/
function solveEmergency() {

    $('.solEmGroup').click(function (e) {
        var strReg = "";
        var cont = 0;
        var filas = $('#grdEmergencia tr').length;
        console.log("Número de filas: " + filas);

        $('#divGridView input:checked').each(function () {
            if (this.name != "chkSelectAll") {
                strReg = strReg + this.value + "|";
                cont++;
            }
        });

        var pendientes = parseInt(filas) - parseInt(cont);
        console.log("Quedan: " + pendientes);

        if (strReg != "") {

            var strData = new Object();
            strData.codigos = strReg;

            $.ajax({
                type: 'POST',
                url: urlSolve,
                data: strData.codigos,
                success: function (data) {
                    $("#myModal").html(data);
                    $('#myModal').modal('show');

                    $('#saveReg').click(function (e) {
                        e.preventDefault();

                        $('#divError').attr('class', "alert fade");
                        var hModal = $("#myModal").height();

                        $.ajax({
                            type: 'POST',
                            url: urlsavN,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getData()),
                            success: function (data) {

                                addnotify("notify", "La operación se realizó con éxito.", "registeruser");

                                clearCampos();
                                $('#myModal').modal('hide');

                                console.log("Antes de llamar a buscar");
                                $('#buscar').trigger("click");

                                if (pendientes <= 1) {
                                    parent.PauseAndHide();
                                }

                            },
                            error: function (xhr, status, error) {
                                addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "registeruser");
                            }
                        });
                    });
                },
                error: function (xhr, status, error) {
                    $("#myModal").html(alertHtml('error', xhr.responseText));
                    $('#myModal').modal('show');
                }
            });
        }
        else {
            $('#myModal').html(alertHtml('resEmergencia'));
            $('#myModal').modal('show');
        }
    });

    $('.solEmItem').live('click', function (e) {

        var filas = $('#grdEmergencia tr').length;
        console.log("Número de filas: " + filas);
        var pendientes = parseInt(filas) - 1;
        console.log("Quedan: " + pendientes);

        var strReg = $(this).attr('cod')

        var strData = new Object();
        strData.codigos = strReg;

        $.ajax({
            type: 'POST',
            url: urlSolve,
            success: function (data) {
                $("#myModal").html(data);
                $('#myModal').modal('show');

                $('#saveReg').click(function (e) {
                    e.preventDefault();

                    $('#divError').attr('class', "alert fade");
                    var hModal = $("#myModal").height();

                    $.ajax({
                        type: 'POST',
                        url: urlsavN,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        cache: false,
                        data: JSON.stringify(getDataItem(strReg)),
                        success: function (data) {
                            addnotify("notify", "La operación se realizó con éxito.", "registeruser");
                            clearCampos();
                            $('#myModal').modal('hide');
                            $('#buscar').trigger("click");

                            if (pendientes <= 1) {
                                parent.PauseAndHide();
                            }

                        },
                        error: function (xhr, status, error) {
                            addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "registeruser");
                        }
                    });
                });
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });
    });
}


/*Restaura Registros*/
function restoreReg() {

    $('.restReg').click(function (e) {
        var strReg = "";

        $('#divGridView input:checked').each(function () {
            if (this.name != "chkSelectAll")
            { strReg = strReg + this.value + "|"; }
        });

        if (strReg != "") {
            $('#myModal').html(alertHtml('delConfirm', "", "¿Esta seguro que desea <b>restaurar</b> los items seleccionados?"));
            $('#myModal').modal('show');

            $('.btnDelSi').click(function (e) {

                var strReg = "";

                $('#divGridView input:checked').each(function () {
                    if (this.name != "chkSelectAll")
                    { strReg = strReg + this.value + "|"; }
                });
                var strData = new Object();
                strData.codigos = strReg;


                $.ajax({
                    type: 'POST',
                    url: urlrest,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    data: JSON.stringify(strData),
                    success: function (data) {
                        $('#buscar').trigger("click");

                    },
                    error: function (xhr, status, error) {
                        $("#myModal").html(alertHtml('error', xhr.responseText));
                        $('#myModal').modal('show');
                    }
                });
            });

        }
        else {
            $('#myModal').html(alertHtml('delConfirmFalta'));
            $('#myModal').modal('show');
        }


    });

    $('.restItemReg').live('click', function (e) {

        var strReg = $(this).attr('cod')
        $('#myModal').html(alertHtml('delConfirm', "", "¿Esta seguro que desea <b>restaurar</b> éste registro?"));
        $('#myModal').modal('show');

        $('.btnDelSi').click(function (e) {

            var strData = new Object();
            strData.codigos = strReg;


            $.ajax({
                type: 'POST',
                url: urlrest,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                data: JSON.stringify(strData),
                success: function (data) {
                    $('#buscar').trigger("click");
                },
                error: function (xhr, status, error) {
                    $("#myModal").html(alertHtml('error', xhr.responseText));
                    $('#myModal').modal('show');
                }
            });
        });

    });


}

/*Agrega Registros*/
function addReg() {
    $('.addReg').click(function (e) {
        $.ajax({
            type: 'POST',
            url: urlins,
            success: function (data) {
                $("#myModal").html(data);
                $('#myModal').modal('show');

                $('#saveReg').click(function (e) {
                    e.preventDefault();

                    var validateItems = true;

                    $('.requerid').each(function () {
                        $(this).parent().find('span').remove();
                        if ($(this).val() == "") {

                            $(this).before("<span style='color:#b94a48'>*</span>");
                            validateItems = false;
                        }
                    });

                    if (!validateItems) {
                        addnotify("notify", "Ingresar los campos obligatorios", "registeruser");
                    }
                    else {
                        $('#divError').attr('class', "alert fade");
                        var hModal = $("#myModal").height();

                        $.ajax({
                            type: 'POST',
                            url: urlsavN,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getData()),
                            success: function (data) {
                                addnotify("notify", "La operación se realizó con éxito.", "registeruser");
                                clearCampos();
                                $('#myModal').modal('hide');
                                $('#buscar').trigger("click");

                            },
                            error: function (xhr, status, error) {
                                addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "registeruser");
                            }
                        });

                    }
                });

            },
            error: function (xhr, status, error) {

                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');

            }
        });

    });


}


/*Agrega Registros*/
function addRegEmp() {
    $('.addReg').click(function (e) {

        $.ajax({
            type: 'POST',
            url: urlval,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            cache: false,

            success: function (data) {

                if (data.d == "T") {
                    $.ajax({
                        type: 'POST',
                        url: urlins,
                        success: function (data) {
                            $("#myModal").html(data);
                            $('#myModal').modal('show');

                            $('#saveReg').click(function (e) {
                                e.preventDefault();

                                var validateItems = true;

                                $('.requerid').each(function () {
                                    $(this).parent().find('span').remove();
                                    if ($(this).val() == "") {

                                        $(this).before("<span style='color:#b94a48'>*</span>");
                                        validateItems = false;
                                    }
                                });

                                if (!validateItems) {
                                    addnotify("notify", "Ingresar los campos obligatorios", "registeruser");
                                }
                                else {
                                    $('#divError').attr('class', "alert fade");
                                    var hModal = $("#myModal").height();

                                    $.ajax({
                                        type: 'POST',
                                        url: urlsavN,
                                        contentType: "application/json; charset=utf-8",
                                        dataType: "json",
                                        async: true,
                                        cache: false,
                                        data: JSON.stringify(getData()),
                                        success: function (data) {
                                            addnotify("notify", "La operación se realizó con éxito.", "registeruser");
                                            clearCampos();
                                            $('#myModal').modal('hide');
                                            $('#buscar').trigger("click");

                                        },
                                        error: function (xhr, status, error) {
                                            addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "registeruser");
                                        }
                                    });

                                }
                            });

                        },
                        error: function (xhr, status, error) {

                            $("#myModal").html(alertHtml('error', xhr.responseText));
                            $('#myModal').modal('show');

                        }
                    });
                }
                else {

                    addnotify("notify", "No es posible adicionar mas empresas, llego al limite de registros.", "registeruser");

                }

            },
            error: function (xhr, status, error) {
                addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "registeruser");
            }
        });



    });


}


function modReg() {

    $('.editItemReg').live('click', function (e) {

        var strReg = $(this).attr('cod')
        var strData = new Object();
        strData.codigo = strReg;

        $.ajax({
            type: 'POST',
            url: urlins,
            data: JSON.stringify(strData),
            success: function (data) {
                $('#myModal').modal('hide');
                $("#myModal").html(data);
                $('#myModal').modal('show');

                $('#saveReg').click(function (e) {
                    e.preventDefault();

                    var validateItems = true;
                    $('.requerid').each(function () {
                        $(this).parent().find('span').remove();
                        if ($(this).val() == "") {
                            $(this).before("<span style='color:#b94a48'>*</span>");
                            validateItems = false;
                        }
                    });
                    if (!validateItems) {
                        addnotify("notify", "Ingresar los campos obligatorios", "registeruser");
                    }
                    else {
                        $('#divError').attr('class', "alert fade");

                        var hModal = $("#myModal").height();

                        $.ajax({
                            type: 'POST',
                            url: urlsavE,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getData()),
                            success: function (data) {
                                addnotify("notify", "La operación se realizó con éxito.", "registeruser");
                                //$('#divError').attr('class', "alert alert-success");
                                //$("#mensajeError").text("Usuario registrado exitosamente");
                                //$("#tituloMensajeError").text('OK');
                                $('#myModal').modal('hide');
                                $('#buscar').trigger("click");

                            },
                            error: function (xhr, status, error) {
                                addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "registeruser");


                            }
                        });

                    }
                });

            },
            error: function (xhr, status, error) {

                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');

            }
        });

    });
}


/*Busqueda*/
function busReg() {

    $("input[id$='ChkAll']").live('click', function (e) {
        $(".grilla INPUT[type='checkbox']")
					 .attr('checked', $("input[id$='ChkAll']")
					 .is(':checked'));
    });

    $(".grilla INPUT[type='checkbox']").live('click', function (e) {
        if (!$(this)[0].checked) {
            $("input[id$='ChkAll']").attr("checked", false);
        }
    });

    $('#buscar').click(function (e) {
        //Check True/False

        if ($('#chkhabilitado').length) { //Si existe el elemento
            if ($('#chkhabilitado').attr("checked")) {
                $("#cz-form-box-del").val("Eliminar");

            } else { $("#cz-form-box-del").val("Restaurar"); }
        }

        //cargando la grilla
        var strData = getParametros();
        strData.pagina = 1;
        $('#hdnActualPage').val('1');
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $('.form-gridview-data').html('');
                $('.form-gridview-error').html('');
                $('.form-gridview-search').show();
            },
            success: function (data) {
                $('.form-gridview-search').hide();
                $('.form-gridview-error').html('');
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
            },
            error: function (xhr, status, error) {
                $('.form-gridview-search').hide();
                $('.form-gridview-data').html();
                $('.form-gridview-error').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                $("#myModal").html(alertHtml('error', xhr.responseText));

                $('#divError').click(function (e) {
                    $('#myModal').modal('show');
                });
            }
        });

    });

    $('.pagina-anterior').live('click', function (e) {

        var strData = getParametros();
        strData.pagina = parseInt(strData.pagina) - 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });

    $('.pagina-siguiente').live('click', function (e) {

        var strData = getParametros();
        strData.pagina = parseInt(strData.pagina) + 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });
}

function detReg(control, urlD) {
    $(control).live('click', function (e) {
        if (!$(this).parent().hasClass("nofoto") && !$(this).parent().hasClass("nogps")) {
            var strReg = $(this).attr('cod');
            var strData = new Object();
            strData.codigo = strReg
            $this = $(this);
            $.ajax({
                type: 'POST',
                url: urlD,
                data: JSON.stringify(strData),
                beforeSend: function () {
                    $("#myModal").html('');
                },
                success: function (data) {
                    ////console.log("return data");
                    $("#myModal").html(data);
                    var modali = 0;
                    $("#myModalContent div table").each(function () {
                        modali = 1;
                    });

                    if (modali == 0 && $this.hasClass("detReg")) {
                        $("#myModalContent div").addClass("cz-util-center-text").html("<p>No se registro ningun detalle.<p>");
                        $("#cz-form-modal-exportar").hide();
                    }

                    $('#myModal').on('shown', function () {
                        google.maps.event.trigger(map, "resize");
                    });

                    $('#myModal').modal('show');

                },
                error: function (xhr, status, error) {

                    $("#myModal").html(alertHtml('error', xhr.responseText));
                    $('#myModal').modal('show');

                }
            });
        }
    });
}

function detReg2(control, urlD) {



    $(control).live('click', function (e) {

        var strReg = "";

        $('#divGridView input:checked').each(function () {
            if (this.name != "chkSelectAll") {
                strReg = strReg + this.value + "|";
            }
        });

        if (strReg != "") {
            var strData = new Object();
            strData.codigo = strReg;
            $.ajax({
                type: 'POST',
                url: urlD,
                data: JSON.stringify(strData),
                beforeSend: function () {
                    $("#myModal").html('');
                },
                success: function (data) {
                    $("#myModal").html(data);

                    $('#myModal').on('shown', function () {
                        google.maps.event.trigger(map, "resize");
                    });

                    $('#myModal').modal('show');

                },
                error: function (xhr, status, error) {

                    $("#myModal").html(alertHtml('error', xhr.responseText));
                    $('#myModal').modal('show');

                }
            });
        } else {
            $("#myModal").html(alertHtml('erroCheck'));
            $('#myModal').modal('show');
        }
    });
}

function exportarReg(control, urlXLS, grilla) {
    $(control).live('click', function (e) {
        var filas = getNumRows(grilla);
        if (filas > 0) {
            window.location.href = urlXLS + '?' + getParametrosXLS();
        }
        else {
            $('#myModal').html(alertHtml('alertValidacion', "No existe data para exportar"));
            $('#myModal').modal('show');

        }
    });
}

function exportarRegDet(control, urlXLS, grilla, parameters) {
    $(control).live('click', function (e) {
        var filas = getNumRows(grilla);
        if (filas > 0) {
            window.location.href = urlXLS + '?' + parameters;
        }
        else {
            $('#myModal').html(alertHtml('alertValidacion', "No existe data para exportar"));
            $('#myModal').modal('show');
        }
    });
}

function getNumRows(grilla) {
    var numRows = "";
    numRows = $(grilla).find('tr').length;
    return numRows;
}

function cargacomboA(Padre, Hijo, pag) //funcion encargada de manejar combos anidados sin la necesidad de realizar algun postback
{
    ////console.log("cargacomboA");
    $(document).ready(function () {
        //elegido=$(Padre).val();
        elegido = Padre;

        $.post(pag, { elegido: elegido }, function (data) {
            $(Hijo).html(data);
        });
        //console.log("a-" + Hijo);
        $(Hijo).change();
    });
}

function cargacomboC(Padre, Hijo, pag, codigo, nombre) //funcion encargada de manejar combos anidados sin la necesidad de realizar algun postback
{

    $(document).ready(function () {

        elegido = Padre;

        $.post(pag, { elegido: elegido, codigo: codigo, nombre: nombre }, function (data) {
            $(Hijo).html(data);
        });

        //$(Hijo).change();
        $(Hijo).val(0);
        //$(Hijo).change();
    });
}

/*CARGA*/
function accionesCarga() {


    $('.btnError').live('click', function (e) {
        e.preventDefault();

        tit = $(this).attr("idC");
        $("#myModal").html(alertHtml("errorCarga", $(this).parent().find('.errmsg').html(), tit));
        $('#myModal').modal('show');

    });


    $('#buscar').live("click", function (e) {
        e.preventDefault();
        $('.form-box').show();
        $('#divGridViewData').html('');
        $('.divUploadNew').hide();

    });


    var bex = -1;
    var bey = 0;
    var beerror = 0;

    $('.boton-ejecutar').live("click", function (e) {
        e.preventDefault();
        //console.log(".boton-ejecutar");

        bex = -1;
        bey = 0;
        beerror = 0;

        $.ajax({
            type: "POST",
            url: "Carga.aspx/ejecutarArchivoDTS",

            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            cache: false,
            success: function (msg) {


                $('#file-uploader').find('.qq-upload-list li').remove();
                $('.boton-ejecutar').hide();
                $('#divGridViewData').html(msg.d);
                $('.form-box').hide();
                $('.divUploadNew').show();
                $("#myModal").html("");
                $("#divGridViewData table tr").each(function () {
                    bex++;
                    if (bex < $("#divGridViewData table tr").length) {
                        if (bex != 0) {
                            while (bey < $(this).find("td").length) {
                                //console.log("fila y columna:" + bex + "-" + bey);
                                if (bey == 1) {
                                    var detalle_error = $(this).find(":nth-child(" + (bey + 1) + ") .errmsg").html();
                                    if (detalle_error.length > 0) {
                                        beerror++;
                                        $("#myModal").html($("#myModal").html() + "<br>" + $(this).find(":nth-child(" + (bey) + ")").html() + ": 1");
                                    }
                                }
                                bey++;
                            }
                            bey = 0;
                        }
                    }
                });

                if (beerror > 0) {
                    $("#myModal").html('<title> </title> <form method="post" action="repDet.aspx" id="form1"> </div> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> ×</button> <h3 id="myModalLabel"> Detalle</h3> </div> <div id="myModalContent" class="modal-body"> <div class="cz-util-center-text"><p>Se han encontrado errores de carga en las filas: ' + $("#myModal").html() + '</p></div> </div> <div class="modal-footer"> </div> </form> ');
                    $('#myModal').modal('show');
                }


            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml("error", jQuery.parseJSON(xhr.responseText).Message));
                $('#myModal').modal('show');
            }
        });
    });



    $('.boton-borrar').live("click", function (e) {
        e.preventDefault();
        var listFiles = $('#file-uploader').find('.qq-upload-list li');

        var idclass = $(this).attr("idclass");
        var filename = $(this).attr("filename");
        var error = $(this).attr("error");

        var name = filename.substring(0, filename.lastIndexOf('.'));
        var ext = filename.substr(filename.lastIndexOf('.') + 1);

        var jsonFileName = name;
        var jsonFileExt = ext;

        listFiles.find("#fri-" + idclass).remove();
        if (error == "si")
        { listFiles.find("#fbd-" + idclass).append("<div class='file-load-icon' id='fld-" + idclass + "'><img src='../images/icons/loader/ico_loader_red.gif' /></div>"); }
        else
        { listFiles.find("#fbd-" + idclass).append("<div class='file-load-icon' id='fld-" + idclass + "'><img src='../images/icons/loader/ico_loader_lightorange.gif' /></div>"); }

        var strData = new Object();
        strData.filename = jsonFileName;
        strData.fileext = jsonFileExt;

        $.ajax({
            type: "POST",
            url: "Carga.aspx/borrarArchivo",
            data: JSON.stringify(strData),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            cache: false,
            success: function (msg) {

                listFiles.find("#fld-" + idclass).parent().parent().parent().parent().remove();
                if (listFiles.length > 1) {
                    $('.boton-ejecutar').show();
                }
                else {
                    $('.boton-ejecutar').hide();
                }

            },
            error: function (xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                listFiles.find("#fld-" + idclass).remove();
                listFiles.find("#fbd-" + idclass).addClass("file-red");
                listFiles.find("#fbd-" + idclass).append("<div class='file-result-icon'><img src='../images/icons/carga/ico_carga_error.png' /></div>");
                listFiles.find("#fbx-" + idclass).append("<div class='file-result-text file-red-font' id='frs-" + idclass + "'>" + err.Message + "</div>");
            }
        });
    });
}

function cargaSup(control) {
    var strperfil = $('#MddlPerfil option:selected').val();
    if (strperfil == 'V') {
        $('.filsup').show();
    }
    else {
        $('.filsup').hide();
    }
}

function cargaSup2() {
    var strperfil = $('#vddlPerfil option:selected').val();
    if (strperfil == 'V') {
        $('.filsup2').show();
    }
    else {
        $('.filsup2').hide();
    }
}



/*CHARTS*/

function drawChart() {

    $('.downFil').live('click', function (e) {
        e.preventDefault();
        if ($(this).hasClass('upFil')) {
            $(this).removeClass('upFil');
            $('#content_filtros').hide();
            $('#charGauge').css({ "margin-left": "5%" });
        }
        else {
            $(this).addClass('upFil');
            $('#content_filtros').show();
            $('#charGauge').css({ "margin-left": "19%" });
        }
    });

    $('#btnGauge').live('click', function (e) {
        e.preventDefault();


        $.ajax({
            type: 'POST',
            url: 'inicio.aspx/getActProgramdas',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            cache: false,
            data: JSON.stringify(getData()),
            success: function (data) {

                jObject = jQuery.parseJSON(data.d);
                $('#titleGauge').html(jObject.Titulo);
                $('#titleFooterGauge').html(jObject.LabelFoot);
                var dataGauge = new google.visualization.DataTable();
                dataGauge.addColumn('string', 'Label');
                dataGauge.addColumn('number', 'Value');
                dataGauge.addRows(1);
                dataGauge.setCell(0, 0, '');
                dataGauge.setCell(0, 1, jObject.Valor);


                var options = {
                    width: 400, height: 180,
                    redFrom: 0, redTo: 20,
                    yellowFrom: 20, yellowTo: 80,
                    greenFrom: 80, greenTo: 100,
                    minorTicks: 5
                };
                var chart = new google.visualization.Gauge(document.getElementById('charGauge'));
                chart.draw(dataGauge, options);

            },
            error: function (xhr, status, error) {
                addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "alertChart");

            }
        });


    });


    $('#btnObtener').live('click', function (e) {
        e.preventDefault();

        $('#btnGauge').trigger("click");

        $.ajax({
            type: 'POST',
            url: 'inicio.aspx/getActividadesRealizadas',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            cache: false,
            data: JSON.stringify(getData()),
            success: function (data) {

                jObject = jQuery.parseJSON(data.d);


                var dataBar = new google.visualization.DataTable();
                dataBar.addColumn('string', 'Actividades');
                dataBar.addColumn('number', 'Total');
                dataBar.addColumn({ type: 'string', role: 'style' });
                dataBar.addColumn({ type: 'string', role: 'annotation' });

                $.each(jObject.Valor, function (i, item) {
                    dataBar.addRows(1);
                    dataBar.setCell(i, 0, item.usuario);
                    dataBar.setCell(i, 1, item.total);
                    dataBar.setCell(i, 2, item.color);
                    dataBar.setCell(i, 3, item.total);

                });



                var options2 = {
                    width: 400, height: 180,
                    hAxis: { title: jObject.xAxisName, titleTextStyle: { color: 'black' } },
                    vAxis: { title: jObject.yAxisName, titleTextStyle: { color: 'black' } },
                    legend: { position: "none" }
                };


                var chart2 = new google.visualization.ColumnChart(document.getElementById('chartBar'));
                chart2.draw(dataBar, options2);


            },
            error: function (xhr, status, error) {
                addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "alertChart");

            }
        });


    });





}



function colorPicks(color) {
    $('.color').append('<div style="float: left;"><div  class="colorrest" style="float: left; width: 50px; height: 16px; background: ' + $('#hColor').val() + ';"></div></div>');

    var colors = '';
    $.each(color, function (i, col) {
        colors = colors + ' <div  class="colorpick" style="float: right; background-color: ' + col + '; width: 16px; height: 16px; margin-left: 5px;margin-bottom: 5px;"></div>';
    });

    $('.color').append(' <div style="float: right; width: 150px;">' + colors + '</div>');

    $('.colorpick').live('click', function (e) {


        var col = $(this).css('background-color');
        $('.colorrest').css('background-color', col);
        $('#hColor').val(rgb2hex(col));
    });
}


function rgb2hex(rgb) {
    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    function hex(x) {
        return ("0" + parseInt(x).toString(16)).slice(-2);
    }
    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}


function checkAllCampo(Campo, isChecked) {

    $(isChecked).parents('table').find('td .' + Campo + ' > input').attr('checked', isChecked.checked);
}


/*estado*/
function tipo(control) {

    var tr = $('#' + control.id).parent().parent();
    var elegido = '';
    $('#' + control.id).find('option:selected').each(function () {
        elegido = $(this).val();
    });

    if (elegido == "CHG" || elegido == "COM") {
        tr.find(".txtCodGrupo").remove();
        tr.find(".btnGrupo").remove();
        tr.find(".txtMax").remove();
        tr.find(".tdgroup").append("<input id='txtCodGrupo1' readonly class='txtCodGrupo cz-form-content-input-text' style='width: 100px;' disabled='disabled' type='text' /> <div class='cz-form-content-input-text-visible'><div class='cz-form-content-input-text-visible-button'></div></div>");
        tr.find(".cz-form-content-input-text-visible").remove();
        tr.find(".tdgroup").append("<input id='btnGrupo1' style='padding-left: 10px;padding-right: 10px;' class='cz-form-content-input-button btnGrupo' type='button' value='...' />");
    }
    else {
        tr.find(".txtCodGrupo").remove();
        tr.find(".cz-form-content-input-text-visible").remove();
        tr.find(".btnGrupo").remove();

        if (elegido == "ALF" || elegido == "NUM" || elegido == "DEC") {
            tr.find(".txtMax").remove();
            tr.find(".cz-form-content-input-text-visible").remove();
            tr.find(".tdMax").append("<input id='txtMax1' class='txtMax cz-form-content-input-text' onkeypress='fc_PermiteNumeros()' onblur='grabarTR(this);'   type='text' /> <div class='cz-form-content-input-text-visible'><div class='cz-form-content-input-text-visible-button'></div></div>");

        }
        else {
            tr.find(".txtMax").remove();
            tr.find(".cz-form-content-input-text-visible").remove();

        }
    }


}

function tipo2(control) {

    var tr = $('#' + control).parent().parent();
    var elegido = '';
    $('#' + control).find('option:selected').each(function () {
        elegido = $(this).val();
        $(".cz-form-content-input-select").change(function () {

            $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
        });

        $(".cz-form-content-input-select").each(function () {

            $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
        });
    });

    if (elegido == "CHG" || elegido == "COM") {
        tr.find(".txtCodGrupo").remove();
        tr.find(".btnGrupo").remove();
        tr.find(".txtMax").remove();
        tr.find(".tdgroup").append("<input id='txtCodGrupo1' class='txtCodGrupo  cz-form-content-input-text' disabled='disabled' type='text' style='width: 100px;'/> <div class='cz-form-content-input-text-visible'><div class='cz-form-content-input-text-visible-button'></div></div>");
        tr.find(".cz-form-content-input-text-visible").remove();
        tr.find(".tdgroup").append("<input id='btnGrupo1'  style='padding-left: 10px;padding-right: 10px;' class='cz-form-content-input-button btnGrupo' type='button' value='...' />");
    }
    else {
        tr.find(".txtCodGrupo").remove();
        tr.find(".btnGrupo").remove();

        if (elegido == "ALF" || elegido == "NUM" || elegido == "DEC") {
            tr.find(".txtMax").remove();
            tr.find(".cz-form-content-input-text-visible").remove();
            tr.find(".tdMax").append("<input id='txtMax1' class='txtMax cz-form-content-input-text' onkeypress='fc_PermiteNumeros()' onblur='grabarTR(this);'  type='text' /> <div class='cz-form-content-input-text-visible'><div class='cz-form-content-input-text-visible-button'></div></div>");

        }
        else {
            tr.find(".txtMax").remove();
            tr.find(".cz-form-content-input-text-visible").remove();
        }
    }


}

function grabarTR(control) {
    //console.log("Ingreso");
    var table = $('#' + control.id).parent().parent().parent().parent();
    var tr = $('#' + control.id).parent().parent();

    if ($(tr).hasClass('editable')) {
        //console.log("editable");
        var existe = false;
        var fotos = 1;
        var grupo = false;
        var max = false;
        $(table).find('.disable').find(".txtEtiqueta").each(function () {

            if ($(this).val() == tr.find(".txtEtiqueta").val()) {
                existe = true;
            }


        });
        if (tr.find(".cboTipo").val() == "FOT") {
            $(table).find('.disable').find(".cboTipo").each(function () {

                if ($(this).val() == "FOT") {
                    fotos = fotos + 1;
                }

            });
            if (fotos > 3) {
                existe = true;
            }
        }
        if (tr.find(".cboTipo").val() == "CHG" || tr.find(".cboTipo").val() == "COM") {
            if (tr.find(".txtCodGrupo").val() == "") {
                existe = true;
                grupo = true;
            }
        }
        if (tr.find(".cboTipo").val() == "ALF" || tr.find(".cboTipo").val() == "NUM" || tr.find(".cboTipo").val() == "DEC") {
            if (tr.find(".txtMax").val() == "") {
                existe = true;
                max = true;
            }
        }
        if (!existe) {
            if (tr.find(".txtEtiqueta").val() != "") {
                tr.find("input,select").attr('disabled', 'disabled');
                tr.addClass('disable').removeClass('nodrop').removeClass('nodrag');
                $(table).tableDnD();



            }
            else {
                addnotify("notify", "Debe ingresar la etiqueta", "registeruser");
            }
        }
        else {
            if (fotos > 3) {
                addnotify("notify", "Solo se puede agregar 3 controles de foto", "registeruser");

            }
            else {
                if (grupo) {
                    addnotify("notify", "Debe asignar un grupo", "registeruser");


                }
                else if (max) {
                    addnotify("notify", "Ingresar el valor de caracteres maximo", "registeruser");

                }
                else {
                    addnotify("notify", 'Ya existe un control con la etiqueta ' + tr.find(".txtEtiqueta").val(), "registeruser");

                }
            }
        }
    }
    else {
        //console.log("insert_new");
        var $trUlt = $(table).find("tbody tr:last");
        var existe = false;
        var fotos = 1;
        var grupo = false;
        var max = false;
        $(table).find('.disable').find(".txtEtiqueta").each(function () {

            if ($(this).val() == $trUlt.find(".txtEtiqueta").val()) {
                existe = true;
            }

        });

        if ($trUlt.find(".cboTipo").val() == "FOT") {
            $(table).find('.disable').find(".cboTipo").each(function () {

                if ($(this).val() == "FOT") {
                    fotos = fotos + 1;
                }

            });
            if (fotos > 3) {
                existe = true;
            }
        }

        if ($trUlt.find(".cboTipo").val() == "CHG" || $trUlt.find(".cboTipo").val() == "COM") {
            if ($trUlt.find(".txtCodGrupo").val() == "") {
                existe = true;
                grupo = true;
            }
        }

        if ($trUlt.find(".cboTipo").val() == "ALF" || $trUlt.find(".cboTipo").val() == "NUM" || $trUlt.find(".cboTipo").val() == "DEC") {
            if ($trUlt.find(".txtMax").val() == "") {
                existe = true;
                max = true;
            }
        }

        if (!existe) {

            if ($trUlt.find(".txtEtiqueta").val() != "") {
                var $tr = $(table).find("tbody tr:last").clone();
                $tr.find("input,select").attr("id", function () {
                    var parts = this.id.match(/(\D+)(\d+)$/);
                    return parts[1] + ++parts[2];
                });

                $tr.find("input[type='text']").val('');
                $tr.find("input[type='checkbox']").attr('checked', 'checked');

                $trUlt.find("input,select").attr('disabled', 'disabled');
                $trUlt.addClass('disable').removeClass('nodrop').removeClass('nodrag');

                $(table).find("tbody tr:last").after($tr);
                $(table).tableDnD();


                tipo2($tr.find('.cboTipo')[0].id);


                $('.divControl').animate({ scrollTop: $(".divControl").height() }, 1000);
            }
        }
        else {
            if (fotos > 3) {
                addnotify("notify", "Solo se puede agregar 3 controles de foto", "registeruser");

            }
            else {
                if (grupo) {
                    addnotify("notify", "Debe asignar un grupo", "registeruser");

                }
                else if (max) {
                    addnotify("notify", "Ingresar el valor de caracteres maximo", "registeruser");

                }
                else {
                    addnotify("notify", 'Ya existe un control con la etiqueta ' + $trUlt.find(".txtEtiqueta").val(), "registeruser");

                }
            }
        }
    }
}


function grabarTROPC(control) {
    var table = $('#' + control.id).parent().parent().parent().parent();
    var tr = $('#' + control.id).parent().parent();
    var nombre = false;
    if ($(tr).hasClass('editable')) {

        var existe = false;


        $(table).find('.disable').find(".txtOPNombre").each(function () {

            if ($(this).val() == tr.find(".txtOPNombre").val()) {
                existe = true;
                nombre = true;
            }

        });

        $(table).find('.disable').find(".txtOPCodigo").each(function () {

            if ($(this).val() == tr.find(".txtOPCodigo").val()) {
                existe = true;
                nombre = false
            }

        });


        if (!existe) {
            if (tr.find(".txtOPNombre").val() != "" || tr.find(".txtOPCodigo").val() != "") {
                tr.find("input").attr('disabled', 'disabled');
                tr.addClass('disable').removeClass('nodrop').removeClass('nodrag');
                $(table).tableDnD();
            }
            else {
                addnotify("notify", "Debe ingresar la etiqueta", "registeruser");

            }
        }
        else {
            if (nombre) {
                addnotify("notify", 'Ya existe una opcion con el nombre ' + tr.find(".txtOPNombre").val(), "registeruser");

            }
            else {
                addnotify("notify", 'Ya existe una de opcion con el codigo ' + tr.find(".txtOPCodigo").val(), "registeruser");


            }

        }
    }
    else {
        var $trUlt = $(table).find("tbody tr:last");
        var existe = false;


        $(table).find('.disable').find(".txtOPNombre").each(function () {

            if ($(this).val() == tr.find(".txtOPNombre").val()) {
                existe = true;
                nombre = true;
            }

        });

        $(table).find('.disable').find(".txtOPCodigo").each(function () {

            if ($(this).val() == tr.find(".txtOPCodigo").val()) {
                existe = true;
                nombre = false
            }

        });



        if (!existe) {
            if ($trUlt.find(".txtOPNombre").val() != "" && $trUlt.find(".txtOPCodigo").val() != "") {
                var $tr = $(table).find("tbody tr:last").clone();
                $tr.find("input,select").attr("id", function () {
                    var parts = this.id.match(/(\D+)(\d+)$/);
                    return parts[1] + ++parts[2];
                });

                $tr.find("input[type='text']").val('');
                $tr.find("input[type='checkbox']").attr('checked', false);

                $trUlt.find("input").attr('disabled', 'disabled');
                $trUlt.addClass('disable').removeClass('nodrop').removeClass('nodrag');

                $(table).find("tbody tr:last").after($tr);
                $(table).tableDnD();


            }
        }
        else {
            if (!existe) {
                if (tr.find(".txtOPNombre").val() != "" && tr.find(".txtOPCodigo").val() != "") {
                    tr.find("input").attr('disabled', 'disabled');
                    tr.addClass('disable').removeClass('nodrop').removeClass('nodrag');
                    $(table).tableDnD();
                }
                else {
                    addnotify("notify", 'Debe ingresar la etiqueta', "registeruser");

                }
            }
            else {
                if (nombre) {
                    addnotify("notify", 'Ya existe una opcion con el nombre ' + tr.find(".txtOPNombre").val(), "registeruser");

                }
                else {
                    addnotify("notify", 'Ya existe una de opcion con el codigo ' + tr.find(".txtOPCodigo").val(), "registeruser");

                }

            }
        }
    }
}

function setRadVal(id, value) {
    var radio = $("[id*=" + id + "] label:contains('" + value + "')").closest("td").find("input");
    radio.attr("checked", "checked");
}

function obtenerValorRB(id) {
    var list = document.getElementById(id);
    var inputs = list.getElementsByTagName("input");
    var selected;
    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].checked) {
            selected = inputs[i];
            break;
        }
    }

    return selected.value;
}

function irGrillaMonto(i) {

    var strData = new Object();
    strData.grupo = i;
    strData.fechaInicio = $("#txtFecInicio").val();
    strData.fechaFin = $("#txtFecFin").val();
    strData.tipo = 'M';

    $.ajax({
        type: 'POST',
        url: urlGrid,
        data: JSON.stringify(strData),
        success: function (data) {
            $("#myModal").html(data);
            $('#myModal').modal('show');
        },
        error: function (xhr, status, error) {
            $("#myModal").html(alertHtml('error', xhr.responseText));
            $('#myModal').modal('show');

        }
    });

}

function irGrillaCantidad(i) {
    var strData = new Object();
    strData.grupo = i;
    strData.fechaInicio = $("#txtFecInicio").val();
    strData.fechaFin = $("#txtFecFin").val();
    strData.tipo = 'C';

    $.ajax({
        type: 'POST',
        url: urlGrid,
        data: JSON.stringify(strData),
        success: function (data) {
            $("#myModal").html(data);
            $('#myModal').modal('show');
        },
        error: function (xhr, status, error) {
            $("#myModal").html(alertHtml('error', xhr.responseText));
            $('#myModal').modal('show');

        }
    });
}

function irGrilla(i) {
    var strData = new Object();
    strData.grupo = i;
    strData.fechaInicio = $("#txtFecInicio").val();
    strData.fechaFin = $("#txtFecFin").val();

    $.ajax({
        type: 'POST',
        url: urlGrid,
        data: JSON.stringify(strData),
        success: function (data) {
            $("#myModal").html(data);
            $('#myModal').modal('show');
        },
        error: function (xhr, status, error) {
            $("#myModal").html(alertHtml('error', xhr.responseText));
            $('#myModal').modal('show');

        }
    });
}

function regresarInicio() {
    mostrarEspera();
    $(location).attr('href', "../inicio.aspx" +
      "?fechaInicio=" + $("#txtFecInicio").val() +
      "&fechaFin=" + $("#txtFecFin").val()
      );
}


function refrescar() {
    //  mostrarEspera();
    $("#loadingimgtiempoefectivo").show();
    $("#loadingimgtopproductomonto").show();
    $("#loadingimgtopproductocantidad").show();
    $("#loadingimgtopclientemonto").show();
    $("#loadingimgtopclientecantidad").show();
    $("#loadingimgtopvendedormonto").show();
    $("#loadingimgtopvendedorcantidad").show();
    $("#loadingimgavanceventa").show();
    $("#loadingimgpedidoefectivo").show();
    $("#loadingimgfamiliamonto").show();
    $("#loadingimgfamiliacantidad").show();
    $("#loadingimgmarcamonto").show();
    $("#loadingimgmarcamonto").show();


    $(location).attr('href', urlRefrescar +
        "?fechaInicio=" + $("#txtFecInicio").val() +
        "&fechaFin=" + $("#txtFecFin").val()
        );
}


function mostrarEspera() {
    $("#myModal").html('<div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> ×</button> <h3 id="myModalLabel">Consulta</h3> </div> <div id="myModalContent" class="modal-body"> <div class="cz-util-center-text"><p>Consultando...</br>Espere un momento por favor...</p></div> </div> <div class="modal-footer"> </div>');
    $('#myModal').modal('show');
}

function quitarEspera() {
    $('#myModal').modal('hide');
    $("#myModal").html("");
}
