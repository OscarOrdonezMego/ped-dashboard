﻿<%@ WebHandler Language="C#" Class="anal" %>

using System;
using System.Web;
using System.Configuration;
using System.Text;

public class anal : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {
        if (ConfigurationManager.AppSettings["ID_ANALYTICS"] != null && !ConfigurationManager.AppSettings["ID_ANALYTICS"].Equals(String.Empty))
        {
            context.Response.ContentType = "text/javascript";
            StringBuilder sb = new StringBuilder();
            sb.Append("(function (i, s, o, g, r, a, m) {\n");
            sb.Append("i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {\n");
            sb.Append("(i[r].q = i[r].q || []).push(arguments)\n");
            sb.Append("}, i[r].l = 1 * new Date(); a = s.createElement(o),\n");
            sb.Append("m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)\n");
            sb.Append("})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');\n");
            sb.Append("ga('create', '");
            sb.Append(ConfigurationManager.AppSettings["ID_ANALYTICS"]);
            sb.Append("', { 'cookieDomain': '");
            //sb.Append("auto");
            sb.Append(context.Request.Url.Host);
            sb.Append("' });\n");
            sb.Append("ga('send', 'pageview');");
            context.Response.Write(sb.ToString());
        }
    }
 
    public bool IsReusable {
        get {
            return true;
        }
    }

}