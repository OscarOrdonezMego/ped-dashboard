﻿; (function ($) {
    $.fn.colorPickerAgain = function ($$options) {
        // valores por defecto
        var $defaults = {
            color: new Array(
				"#CC3333", "#DD4477", "#994499", "#6633CC", "#336699", "#3366CC", "#22AA99",
				"#329262", "#109618", "#66AA00", "#AAAA11", "#D6AE00", "#EE8800", "#DD5511",
				"#A87070", "#8C6D8C", "#627487", "#7083A8", "#5C8D87", "#898951", "#B08B59"
			),
            defaultColor: 0,
            columns: 0,
            height: '80px',
            valor: '',
            click: function ($color) { }
        };

        var $settings = $.extend({}, $defaults, $$options);

        // iterate and reformat each matched element
        return this.each(function () {
            $this = $(this);

            // build element specific options
            var o = $.meta ? $.extend({}, $settings, $this.data()) : $settings;
            var $$oldIndex = typeof (o.defaultColor) == 'number' ? o.defaultColor : -1;

            var _html = "";
            for (i = 0; i < o.color.length; i++) {
                _html += '<div class="cz-form-content-input-text-visible bkcolor " bk="'+ o.color[i] +'" style="margin-top: 0px;width:20px; float:left;margin-right: 5px;margin-bottom:5px; background:#'+o.color[i]+';"></div>\n';
               
            }

            $this.html('<div class="cz-form-content" style="height: '+o.height+';">Color<div  class="cz-form-content-input-text-visible bkColorInput" style="margin-top: 0px;"><p class="cz-form-content-input-select-visible-text"></p></div></div >  <div class="cz-form-content" style="height: '+o.height+';"><div style="margin-top: 14px;" >' + _html + '</div></div>');
            
            
            
          


              
              
            // CSS: Buenas practicas >> Cuidado con el Width!
            // http://www.lawebera.es/comunidad/articulos/diseno-web/css/css-buenas-practicas.php
          //  $this.html('<div class="jColorSelect">' + _html + '</div>');
            //obtenemos los contenedores de los colores
            //$color = $this.children('.jColorSelect').children('div'); //no me funciono un solo children ( $this.children('.jColorSelect div') )
            $color=$this.find('.bkcolor');
            // se obtiene el tamaño del DIV que contiene un color ( al usar la funcion "width" toma de referencia el width del primero ).
            // $color.width()+2+2 //se le suma el padding y margin de cada lado horizontal.
            //var w = ($color.width() + 2 + 2) * (o.columns > 0 ? o.columns : o.color.length);
            //$this.children('.jColorSelect').css('width', w);

            // Se usarán los indices de los DIV indicar el color dentro del array "o.color".
            // Nota: Use "each" para lograr obtener el index de los DIV, si existe otra manera por favor avisenme.
            $color.each(function (i) {
                $(this).click(function () {
                    // Evitar que vuelva a dar click a uno seleccionado
                    if ($$oldIndex == i) return;

                    if ($$oldIndex > -1) {
                        // Si existe un color YA seleccionado se removera la clase check.
                        if ($color.eq($$oldIndex).hasClass('check')) $color.eq($$oldIndex).removeClass('check');
                    }
                    // Se guarda el Index del color seleccionado
                    $$oldIndex = i;

                    // se le agrega el "Check"
                    $(this).addClass('check');
                    // Se invoca a la funcion click definida por el usuario
                    o.click(o.color[i]);
                });
            });

            // Simula un click al color por defecto (defaultColor).

            
            if($(o.valor).val()!='')
            {
                for(i=0;i<o.color.length;i++){
                    if(o.color[i]==$(o.valor).val())
                    {
                        _tmp = i;
                        $$oldIndex = -1;
                        $color.eq(_tmp).trigger('click');
                    }
                }
                
            }
            else{
            _tmp = $$oldIndex;
            $$oldIndex = -1;
            $color.eq(_tmp).trigger('click');
            
            }
        });

        return this;
    };
    $.fn.colorPickerAgain.clear = function(){
        $('.bkcolor').eq(0).trigger('click');
    }

})(jQuery);