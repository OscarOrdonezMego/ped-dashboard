﻿/*************************************************************************************
Descripcion : Obtener y agregar items a un jqgrid temporalmente
Autor		: DSB MOBILE SAC
Fecha/hora	: 25/08/2014
Empresa		: Nextel del Perú S.A.
*************************************************************************************/

var datosJSonSucursal = new Array();
var imgRemove = "<img src='../../images/icons/habilitado.png' alt='Habilitado' />";
var imgRestore = "<img src='../../images/icons/deshabilitado.png' alt='Deshabilitado' />";

// Obtener datos de un JSON
function fc_GetDatosJSon() {
    var datosJSonSucursal;
    var strData = getDataGrilla();//Parámetros de la grilla

    $.ajax({
        type: 'POST',

        data: JSON.stringify(strData), 
        dataType: 'json',
        url: urlGrilla, //Dirección URL del método
        contentType: 'application/json; charset=utf-8',
        async: false,
        success: function (data, textStatus) {
            if (textStatus == "success") {
                datosJSonSucursal = JSON.parse(data.d);
            }
            else
                addnotify("notify", "ERROR SUCCESS: " + JSON.parse(jsondata.responseText).Message, "registeruser");
        },
        error: function (request, status, error) {
                addnotify("notify", "ERROR SUCCESS: " + JSON.parse(jsondata.responseText).Message, "registeruser");
                ErrorJquery(request, status);
        }
    });
    return datosJSonSucursal;
}


//Agregar registros temporalmente
function addRegTemp() {

    datosJSonSucursal = fc_GetDatosJSon();

    $('.addRegTemp').click(function (e) {
       
        var id = (datosJSonSucursal.length) * -1;
        var IdSucursal = '0';
        var Descripcion = $('#MtxtDescripcionSucursal').val();
        var Direccion = $('#MtxtDireccionSucursal').val();
        var FlgHabilitado = 'T';
        var flgUsarEstadosGenerales = $("#chkGenerales").prop('checked') == true ? "T" : "F";

        var obj = { IdSucursal: IdSucursal, Descripcion: Descripcion, Direccion: Direccion, FlgHabilitado: FlgHabilitado, flgUsarEstadosGenerales: flgUsarEstadosGenerales };

        //Validar si existe el código
        if (findAndItem(datosJSonSucursal, 'Descripcion', Descripcion) < 0 && Descripcion != "" && Direccion != "") {
            datosJSonSucursal.push(obj);
            $(grilla).jqGrid('addRowData', id, obj);
            $('#MtxtDescripcionSucursal').val('');
            $('#MtxtDireccionSucursal').val('');
            $('#MtxtDescripcionSucursal').focus();
        } else {
            if (Descripcion == "")
                addnotify("notify", "Ingresar descripción sucursal", "registeruser");
            else if (Direccion == "")
                addnotify("notify", "Ingresar dirección sucursal", "registeruser");
        }
    });
}

//Eliminar registros temporales
function delRegTemp() {
    $('.delRegTemp').click(function (e) {
        var myGrid = $(grilla);
        var selRowId = myGrid.jqGrid('getGridParam', 'selrow');

        if (selRowId == null) {
            addnotify("notify", "Seleccione una sucursal", "registeruser");
        }

        rowData = $(grilla).jqGrid('getRowData', selRowId).FlgHabilitado;
        
        if (rowData == "T") {

            var obj = { FlgHabilitado: 'F' };
            setData(selRowId, 3, 'F');
            jQuery(grilla).jqGrid('setRowData', selRowId, obj);
            jQuery(grilla).jqGrid('setRowData', selRowId, { img: imgRestore });
            $("#delOpcion").val("Restaurar");

        } else if (rowData == "F") {

            var obj = { FlgHabilitado: 'T' };
            setData(selRowId, 3, 'T');
            jQuery(grilla).jqGrid('setRowData', selRowId, obj);
            jQuery(grilla).jqGrid('setRowData', selRowId, { img: imgRemove });
            $("#delOpcion").val("Eliminar");

        }

        showDelReg();
    });
}

//Editar registros temporales
function modRegTemp() {
    $('.modRegTemp').click(function (e) {

        var Descripcion = $('#MtxtDescripcionSucursal').val();
        var Direccion = $('#MtxtDireccionSucursal').val();
        var flgUsarEstadosGenerales = $("#chkGenerales").prop('checked') == true ? "T" : "F";
        var myGrid = $(grilla);
        var selRowId = myGrid.jqGrid('getGridParam', 'selrow');


        var rowData = jQuery(grilla).jqGrid('getRowData', selRowId);
        var repetido = false;
        var allRowsInGrid = jQuery(grilla).jqGrid('getRowData');
        for (i = 0; i < allRowsInGrid.length; i++) {

            if (allRowsInGrid[i].IdSucursal != rowData.IdSucursal) {
                if (allRowsInGrid[i].Descripcion == Descripcion) {
                    repetido = true;
                }
            }
            pid = allRowsInGrid[i].Descripcion;

        }


        if (selRowId == null) {
            addnotify("notify", "Seleccione una sucursal", "registeruser");
        }

        if (Descripcion == "" || Direccion == "") {

            addnotify("notify", "No se puede actualizar campos vacíos", "registeruser");

        }
        else if (repetido) {
            addnotify("notify", "La descripción ya existe en otro registro", "registeruser");
        } /* else if (rowData.Descripcion != Descripcion) {

            addnotify("notify", "La descripción no corresponde al seleccionado", "registeruser");

        } */
        else {

            setData(selRowId, 1, Descripcion);
            jQuery(grilla).jqGrid('setRowData', selRowId, { Descripcion: Descripcion });
            setData(selRowId, 2, Direccion);
            jQuery(grilla).jqGrid('setRowData', selRowId, { Direccion: Direccion });

            setData(selRowId, 4, flgUsarEstadosGenerales);
            jQuery(grilla).jqGrid('setRowData', selRowId, { flgUsarEstadosGenerales: flgUsarEstadosGenerales });




            if (flgUsarEstadosGenerales == "F") {


                jQuery(grilla).jqGrid('setRowData', selRowId, { imgEstado: imgRestore });


            } else if (flgUsarEstadosGenerales == "T") {


                jQuery(grilla).jqGrid('setRowData', selRowId, { imgEstado: imgRemove });

            }


        }

    });
}

//Cancelar Registro
function cancelRegTemp() {
    $('.cancelRegTemp').click(function (e) {

        $(grilla).jqGrid('resetSelection');
        $('#MtxtDescripcionSucursal').val('');
        $('#MtxtDescripcionSucursal').removeAttr("disabled"); 
        $('#MtxtDireccionSucursal').val('');

        $('#addOpcion').css({ display: "inline" });
        $('#modOpcion').css({ display: "none" });
        $('#delOpcion').css({ display: "none" });
        $('#cancelOpcion').css({ display: "none" });
    });
}

//Cargar los datos a la grilla
function fc_LoadDatosJSon() {

    var gridData;
    var datosJSonSucursal = fc_GetDatosJSon();//Datos obtenidos del JSON
    gridData = datosJSonSucursal;

    var theGrid = $(grilla);
    numberTemplate = { formatter: 'number', align: 'right', sorttype: 'number' };
    theGrid.jqGrid({
        datatype: 'local',
        data: gridData,
        //async: false,
        loadtext: "Cargando...",
        colNames: colnames, //Nombre de las columnas
        colModel: colmodel, //Datos de las columnas

        autowidth: true,
        gridview: true,
        rownumbers: false,
        rowNum: 100,
        rowList: [5, 10, 15],
        loadonce: true,
        sortname: 'Descripcion,IdSucursal',
        sortorder: "asc",
        //pager: '#TheGridPager_Sucursal',
        jsonReader: { cell: "" },
        pgtext: 'Pág: {0} de {1}',
        viewrecords: true,
        recordtext: "{0} - {1} de {2} elementos",
        emptyrecords: 'No hay resultados',
        multiselect: false,

        caption: '',
        height: '100%',
        gridComplete: function () {
            var rowIDs = jQuery(grilla).jqGrid('getDataIDs');
            for (var i = 0; i < rowIDs.length; i++) {
                var rowID = rowIDs[i];
                var rowData = jQuery(grilla).jqGrid('getRowData', rowID);

                if (rowData.FlgHabilitado == 'T') {
                    jQuery(grilla).jqGrid('setRowData', rowID, { img: imgRemove });
                }
                else {
                    jQuery(grilla).jqGrid('setRowData', rowID, { img: imgRestore });
                }


                if (rowData.flgUsarEstadosGenerales == 'T') {
                    jQuery(grilla).jqGrid('setRowData', rowID, { imgEstado: imgRemove });
                }
                else {
                    jQuery(grilla).jqGrid('setRowData', rowID, { imgEstado: imgRestore });
                }


            }
        },
        onSelectRow: function (rowid) {
            fc_onClick_Fila(rowid);
        }
    });
}

/*************************************************************************************
Descripcion : Funciones de busqueda JSON
Autor		: DSB MOBILE SAC
Fecha/hora	: 18/09/2014
Empresa		: Nextel del Perú S.A.
*************************************************************************************/
//Funcion buscar objetos similares
function findAndItem(array, property, value) {
    if (array.length <= 0) {
        return -1;
    }
    else {
        var res = -1;
        $.each(array, function (index, result) {
            var txct = result[property];
            if (txct == value) {
                res = index;
                return false;
            }
        });

        //if (res > 0) {
        if (res > -1) {
            addnotify("notify", "Descripción repetida: " + value, "registeruser");
        }
        return res;
    }
}

//Funcion onClick sobre el jqgrid 
function fc_onClick_Fila(rowid) {
    var rowDataDescripcion = $(grilla).jqGrid('getRowData', rowid).Descripcion;
    var rowDataDireccion = $(grilla).jqGrid('getRowData', rowid).Direccion;
    var rowDataFlgHabilitado = $(grilla).jqGrid('getRowData', rowid).FlgHabilitado;
    var flgUsarEstadosGenerales = $(grilla).jqGrid('getRowData', rowid).flgUsarEstadosGenerales;

    if (rowDataFlgHabilitado == "F") {
        $("#delOpcion").val("Restaurar");
    } else { $("#delOpcion").val("Eliminar"); }

    $('#MtxtDescripcionSucursal').val(rowDataDescripcion);
 //   $('#MtxtDescripcionSucursal').attr("disabled", "disabled");
    $('#MtxtDireccionSucursal').val(rowDataDireccion);
    $('#MtxtDireccionSucursal')

    if(flgUsarEstadosGenerales=="T")
    {
        $('#chkGenerales').attr('checked','checked');
    }
    else{
       $('#chkGenerales').attr('checked',null);
    }
    $('#addOpcion').css({ display: "none" });
    $('#modOpcion').css({ display: "inline" });
    $('#delOpcion').css({ display: "inline" });
    $('#cancelOpcion').css({ display: "inline" });
}

//Funcion mostrar registros eliminados
function showDelReg() {
    if ($("#chk").attr('checked')) {
        var rowIDs = jQuery(grilla).jqGrid('getDataIDs');
        for (var i = 0; i < rowIDs.length; i++) {
            var rowID = rowIDs[i];
            var rowData = jQuery(grilla).jqGrid('getRowData', rowID);

            if (rowData.FlgHabilitado == "F") {
                $("#" + rowID, grilla).css({ display: "" });
            }
        }
    } else {
        var rowIDs = jQuery(grilla).jqGrid('getDataIDs');
        for (var i = 0; i < rowIDs.length; i++) {
            var rowID = rowIDs[i];
            var rowData = jQuery(grilla).jqGrid('getRowData', rowID);

            if (rowData.FlgHabilitado == "F") {
                $("#" + rowID, grilla).css({ display: "none" });
            }
        }
    }
}

//Funcion actualizar flag
function setData(id, column, newData) {
    for (var i = 0; i < datosJSonSucursal.length; i++) {
        if (i == (id-1)) {
            switch(column) {
                case 1:
                datosJSonSucursal[i].Descripcion = newData;
                break;
            case 2:
                datosJSonSucursal[i].Direccion = newData;
                break;
            case 3:
                 datosJSonSucursal[i].FlgHabilitado = newData;
                break;
            case 4:
                datosJSonSucursal[i].flgUsarEstadosGenerales = newData;
                break;
            } 
            return;
        }
    }
}