﻿using Controller;
using Model.bean;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.SessionState;

namespace server_reportes.functions
{

    public class DibujoLinear : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {

            string fechaInicio = context.Request["fechaInicio"];
            string fechaFin = context.Request["fechaFin"];
            string usuario = context.Session["lgn_id"].ToString();

            StringBuilder builder = new StringBuilder();
            List<LinearGraficoBean> list = AvanceController.obtenerAvanceVentas(fechaInicio, fechaFin, usuario);

            if (list.Count > 0)
            {
                foreach (LinearGraficoBean detalle in list)
                {
                    builder.Append(detalle.fecha + "#" + detalle.valor + "#" + detalle.valor2 + "|");
                }

                builder.Remove(builder.Length - 1, 1);
            }

            context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(builder.ToString()));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}

