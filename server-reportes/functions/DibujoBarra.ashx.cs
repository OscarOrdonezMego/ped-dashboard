﻿using Controller;
using Model.bean;
using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;

namespace server_reportes.functions
{

    public class DibujoBarra : IHttpHandler,  IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            string fechaInicio = context.Request["fechaInicio"];
            string fechaFin = context.Request["fechaFin"];
            string usuario = context.Session["lgn_id"].ToString();

            List<BarraGraficoItemBean> items = EfectivoController.obtenerPedidosEfectivos(fechaInicio, fechaFin, usuario);

            BarraGraficoBean grafico = new BarraGraficoBean();
            grafico.items = items;

            context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(grafico));

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}