﻿using Controller;
using Model.bean;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace server_reportes.functions
{
    /// <summary>
    /// Summary description for DibujoTopProducto
    /// </summary>
    public class DibujoTopProducto : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            string fechaInicio = context.Request["fechaInicio"];
            string fechaFin = context.Request["fechaFin"];
            string usuario = context.Session["lgn_id"].ToString();
            string tipo = context.Request["tipo"].ToString();

            List<FilaBean> items = ProductoController.obtenerTopProductos(fechaInicio, fechaFin, tipo, usuario);

            FilaBean grafico = new FilaBean();
            grafico.item = items;

            context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(grafico));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}