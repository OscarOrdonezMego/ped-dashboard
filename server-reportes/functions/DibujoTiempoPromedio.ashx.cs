﻿using Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace server_reportes.functions
{
    /// <summary>
    /// Summary description for DibujoTiempoPromedio
    /// </summary>
    public class DibujoTiempoPromedio : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            string fechaInicio = context.Request["fechaInicio"];
            string fechaFin = context.Request["fechaFin"];
            string usuario = context.Session["lgn_id"].ToString();
 
           string tiempo = PromedioController.obtenerTiempoPromediosinhtml(fechaInicio, fechaFin, usuario);

            context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(tiempo));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}