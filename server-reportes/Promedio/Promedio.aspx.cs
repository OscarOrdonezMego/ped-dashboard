﻿using Controller;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace server_reportes.Promedio
{
    public partial class Promedio : System.Web.UI.Page
    {
        public string fechaInicio;
        public string fechaFin;
        public string idUsuario;
        public string perfil;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                iniciarDatos();
                dibujarGraficos();

            }
            catch (Exception ex)
            {
                string myScript = "parent.document.location.href = '../default.aspx?acc=EXT';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, true);
            }
        }

        private void iniciarDatos()
        {
            idUsuario = Session["lgn_id"].ToString();
            perfil = Session["lgn_perfil"].ToString();

            if (!Page.IsPostBack)
            {
                fechaInicio = Request.QueryString["fechaInicio"];
                fechaFin = Request.QueryString["fechaFin"];
                txtFecInicio.Text = Request.QueryString["fechaInicio"];
                txtFecFin.Text = Request.QueryString["fechaFin"];
            }
        }


        private void dibujarGraficos()
        {
            // ltrPromedio.Text = PromedioController.dibujarReporteTiempoPromedioGrupo(fechaInicio, fechaFin, idUsuario, perfil);
            StringBuilder builder = new StringBuilder();
            List<GrupoBean> list = UsuarioController.obtenerGrupos(idUsuario, perfil);
            StringBuilder builderScript = new StringBuilder();

            for (int i = 0; i < list.Count; i++)
            {
                GrupoBean grupo = list[i];
                string divdinamico = ReporteController.generarDivDinamicoTiempoPromedio(grupo.id, grupo, i);
                string tiempo = PromedioController.obtenerTiempoPromedioGruposinhtml(fechaInicio, fechaFin, grupo.id);
                string strDiv = ReporteController.dibujarReportePromedioHighchart(i, tiempo);
                builder.Append(divdinamico);
                builderScript.Append(strDiv);
            }

            ltrPromedio.Text = builder.ToString();
            Page.ClientScript.RegisterStartupScript(GetType(), "close panel", builderScript.ToString(), true);
        }
    }
}