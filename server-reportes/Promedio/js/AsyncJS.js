﻿
    function attachInfoWindow(marker, number, latLongPoint, noReferencial) {
        var arrDatos = ("" + datosInfo[number]).split('|');
        var nextel = arrDatos[0];
        var nombre = arrDatos[1];
        var fechaCelular = arrDatos[2];
        var velocidad = arrDatos[3];
        var existePush = arrDatos[4];
        var latitud = arrDatos[5];
        var longitud = arrDatos[6];
        var existePush3G = arrDatos[7];
        var message;
        var direccion = "Cargando...";

        message = crearMensajeInfoWindow(nextel, nombre, fechaCelular, velocidad, direccion, existePush, noReferencial, latitud, longitud, existePush3G);
        var infowindow = new google.maps.InfoWindow({
                content: message,
                size: new google.maps.Size(50,50)
            });		
        google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(map,marker);
                $.ajax({
			          url: '../Servicio/DireccionMapInfo.aspx/DireccionMapInfo',
			          type: 'POST',
			          data: '{"lat" : '+latitud+', "lon": '+longitud+'}' ,
			          contentType: 'application/json;charset=utf-8',
			          dataType: 'json',
			          cache:'false',
			          async: true,
			          timeout: 20000,
			          error: function(xhr, ajaxOptions, thrownError){		      
	                    direccion = "Error: " + " " + xhr.responseText;
	                    message = crearMensajeInfoWindow(nextel, nombre, fechaCelular, velocidad, direccion, existePush, noReferencial, latitud, longitud, existePush3G);
	                    actualizarInfoWindow(message, nextel, latitud, longitud);
			          },
			          success: function(text){
			            direccion = text.d;
			            message = crearMensajeInfoWindow(nextel, nombre, fechaCelular, velocidad, direccion, existePush, noReferencial, latitud, longitud, existePush3G);
			            actualizarInfoWindow(message, nextel, latitud, longitud);
                      }
				 }); 
            });
    }
    
    
    
    function actualizarInfoWindow(message, nextel, latitud, longitud){
        document.getElementById("puntoActual"+nextel+latitud+longitud).innerHTML = message;   
    }

	function fnEnviarPush(nextel){
		$.ajax({
			      url: '../Servicio/EnvioPush.aspx/javapush',
			      type: 'POST',
			      data: '{"nextel" : '+nextel+'}' ,
			      contentType: 'application/json;charset=utf-8',
			      dataType: 'json',
			      cache:'false',
			      async: true,
			      timeout: 20000,
			      error: function(response, textStatus){				      
	                alert('Error al enviar push: ' + textStatus );
			      },
			      success: function(text){
			      alert(text);
                  }
				 });
	}      
	
	