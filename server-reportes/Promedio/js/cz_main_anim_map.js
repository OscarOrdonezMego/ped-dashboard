﻿
function inicializarEventosMenuLateral() {

    $("#cz-util-hidden-left").click(function () {
        if ($("#cz-menu-lateral-left").width() != 0) {
            $("#cz-menu-lateral-left").animate({ width: "0px" }, 500);
            $(this).animate({ left: "0px" }, 500);
            $("#cz-box-content").animate({ left: "0px" }, 500);
        } else {
            $("#cz-menu-lateral-left").animate({ width: "350px" }, 500);
            $(this).animate({ left: "350px" }, 500);
            $("#cz-box-content").animate({ left: "270px" }, 500);
        }
    });

}

$(document).ready(function () {
    inicializarEventosMenuLateral();
});

