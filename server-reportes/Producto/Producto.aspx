﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Producto.aspx.cs" Inherits="server_reportes.Producto.Producto" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<head id="Head1" runat="server">
    <script src="../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../js/Validacion.js" type="text/javascript"></script>
    <script src="../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/jsmodule.js" type="text/javascript"></script>
    <link href="../css/jquery.autocomplete.css" type="text/css" rel="stylesheet" />
    <script src="../js/jquery.autocomplete.js" type="text/javascript"></script>
    <link href="../css/PopupCalendar.css" type="text/css" rel="stylesheet" />
    <script src="../js/PopupCalendar.js" type="text/javascript"></script>
    <link href="../css/jquery.timepicker.css" type="text/css" rel="stylesheet" />
    <script src="../js/jquery.timepicker.js" type="text/javascript"></script>
    <link href="../css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="../js/cz_main.js" type="text/javascript"></script>
    <script src="../js/highcharts.js"></script>
     <script src="../js/highcharts-drilldown.js"></script>
          <script src="../js/exporting.js"></script>
    <script>
        var urlGrid = "Grid.aspx";
        var urlRefrescar = "Producto.aspx";
    </script>

</head>
<body class="formularyW">
    <form id="form1" runat="server">
        <div class="cz-submain cz-submain-form-background">
            <div id="cz-form-box">
                <div class="cz-form-box-content">



                    <div class="cz-form-content">
                        <p>Fecha Inicio</p>
                        <asp:TextBox ID="txtFecInicio" class="cz-form-content-input-calendar" MaxLength="10"
                            onkeypress="javascript:fc_Slash(this.id, '/');" onblur="javascript:fc_ValidaFechaOnblur(this.id);"
                            runat="server"></asp:TextBox>
                        <div class="cz-form-content-input-calendar-visible">
                            <div class="cz-form-content-input-calendar-visible-button">
                                <img alt="<>" id="Img1" name="fecini-img" class="form-input-date-image cz-form-content-input-text-calendar"
                                    src="../images/icons/calendar.png" />
                            </div>
                        </div>
                    </div>

                    <div class="cz-form-content">
                        <p>Fecha Fin</p>
                        <asp:TextBox ID="txtFecFin" class="cz-form-content-input-calendar" MaxLength="10"
                            onkeypress="javascript:fc_Slash(this.id, '/');" onblur="javascript:fc_ValidaFechaOnblur(this.id);"
                            runat="server"></asp:TextBox>
                        <div class="cz-form-content-input-calendar-visible">
                            <div class="cz-form-content-input-calendar-visible-button">
                                <img alt="<>" id="Img1" name="fecini-img" class="form-input-date-image cz-form-content-input-text-calendar"
                                    src="../images/icons/calendar.png" />
                            </div>
                        </div>
                    </div>


                    <div class="cz-form-content cz-util-right cz-util-right-text" style="width: 70px">
                        <input type="button" id="btnAtras" class="cz-form-content-input-button form-button" onclick="regresarInicio()"
                            value="Atras" />
                    </div>

                    <div class="cz-form-content cz-util-right cz-util-right-text" style="width: 70px">
                        <input type="button" id="btnBuscar" class="cz-form-content-input-button form-button" onclick="refrescar()"
                            value="Actualizar" />
                    </div>

                </div>

                <asp:Literal ID="ltrProducto" runat="server"></asp:Literal>


            </div>
        </div>
        <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" runat="server"
            aria-hidden="true">
        </div>
        <div id="calendarwindow" class="calendar-window">
        </div>
    </form>
</body>
</html>
