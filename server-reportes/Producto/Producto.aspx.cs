﻿using Controller;
using System;
using System.Collections.Generic;
using System.Text;
using Model.bean;
using Model;

namespace server_reportes.Producto
{
    public partial class Producto : System.Web.UI.Page
    {

        public string fechaInicio;
        public string fechaFin;
        public string idUsuario;
        public string perfil;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                iniciarDatos();
                dibujarGraficos();

            }
            catch (Exception ex)
            {
                string myScript = "parent.document.location.href = '../default.aspx?acc=EXT';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, true);
            }
        }

        private void iniciarDatos()
        {
            idUsuario = Session["lgn_id"].ToString();
            perfil = Session["lgn_perfil"].ToString();

            if (!Page.IsPostBack)
            {
                fechaInicio = Request.QueryString["fechaInicio"];
                fechaFin = Request.QueryString["fechaFin"];
                txtFecInicio.Text = Request.QueryString["fechaInicio"];
                txtFecFin.Text = Request.QueryString["fechaFin"];
            }
        }


        private void dibujarGraficos()
        {
             //ltrProducto.Text = ProductoController.dibujarReporteTopProductoGrupo(fechaInicio,fechaFin,idUsuario,perfil);
            StringBuilder builder = new StringBuilder();
            StringBuilder builderScript = new StringBuilder();

            List<GrupoBean> list = UsuarioController.obtenerGrupos(idUsuario, perfil);

            for (int i = 0; i < list.Count; i++)
            {
                GrupoBean grupo = list[i];
                //FALTA
                List<FilaBean> filasM = ProductoController.obtenerTopProductosGrupo(fechaInicio, fechaFin, "M", grupo.id);
                List<FilaBean> filasC = ProductoController.obtenerTopProductosGrupo(fechaInicio, fechaFin, "C", grupo.id);
                string nombreDivMonto = "divMonto" + i;
                string radioButtonMonto = "rbMonto" + i;
                string nombreDivCantidad = "divCantidad" + i;
                string radioButtoCantidad = "rbCantidad" + i;

                string strDiv = ReporteController.dibujarReporteColumnGrupo("Producto", grupo, filasM, filasC, nombreDivMonto, nombreDivCantidad, radioButtonMonto, radioButtoCantidad, "Monto total de Venta", "Cantidad de items", i);

                string strScript = ReporteController.dibujarColumnGrupoHighcharts(i, filasM, filasC);

                builder.Append(strDiv);
                builderScript.Append(strScript);

            }

            ltrProducto.Text = builder.ToString();
            Page.ClientScript.RegisterStartupScript(GetType(), "close panel", builderScript.ToString(), true);

         
        }
    }
}