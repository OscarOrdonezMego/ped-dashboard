﻿using Controller;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;

namespace server_reportes.Producto
{
    public partial class Grid : System.Web.UI.Page
    {
        string tipo = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();
            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

            string grupo = dataJSON["grupo"];
            string fechaInicio = dataJSON["fechaInicio"];
            string fechaFin = dataJSON["fechaFin"];
            tipo = dataJSON["tipo"];

            DataTable dt = ProductoController.obtenerProductosGrupo(fechaInicio, fechaFin, tipo, grupo);

            if (dt.Rows.Count > 0)
            {
                grdHistorial.DataSource = dt;
                grdHistorial.DataBind();
            }
            else
            {
                litNoData.Text = "No hay datos";
            }
        }

        protected void grdHistorial_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow row = e.Row;

            if (row.RowType == DataControlRowType.Header)
            {
                if (tipo.Equals("M"))
                {
                    e.Row.Cells[1].Text = "Monto Soles";
                    e.Row.Cells[2].Visible = true;
                    e.Row.Cells[2].Text = "Monto Dólares";
                }
                else if (tipo.Equals("C"))
                {
                    e.Row.Cells[1].Text = "Cantidad";
                    e.Row.Cells[2].Visible = false;
                }

            }
            else if (row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[1].HorizontalAlign = HorizontalAlign.Center;
                e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Center;
                if (tipo.Equals("M"))
                {
                    e.Row.Cells[2].Visible = true;
                }
                else if (tipo.Equals("C"))
                {
                    e.Row.Cells[2].Visible = false;
                }
            }
        }
    }
}