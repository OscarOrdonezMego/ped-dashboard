﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="server_reportes.Main" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Pedidos - Dashboard</title>
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <link rel="shortcut icon" href="images/icons/shortcuticon0.ico" />
    <script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/jquery-ui.min.js"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <link href="css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="js/cz_main.js" type="text/javascript"></script>

    <script src="js/cz_main_anim_all.js" type="text/javascript"></script>
    <script src="js/Validacion.js" type="text/javascript"></script>
    <link href="css/PopupCalendar.css" type="text/css" rel="stylesheet" />
    <script src="js/PopupCalendar.js" type="text/javascript"></script>
    <link href="css/ui.dropdownchecklist.standalone.css" type="text/css" rel="stylesheet" />
    <script src="js/jquery.cookie.js" type="text/javascript"></script>

    <script src="js/jquery.illuminate.0.7.min.js" type="text/javascript"></script>


</head>
<body>
    <form id="form1" runat="server" class="cz-main">
        <div class="cz-submain">
            <div id="cz-box-header2">
                <div id="cz-barr-top">
                    <div id="cz-control-top" class="cz-box-center-width">
                        <div id="img_logo_top"></div>
                        <div id="cz-control-options">
                            <div id="cz-control-user">
                                <div id="cz-control-user-avatar" class="cz-control-user-option"></div>
                                <div class="cz-control-user-option">
                                    <asp:Label ID="lbNomUsuario" runat="server" Text=""></asp:Label>
                                </div>
                                <div id="cz-control-menu" class="cz-control-user-option">
                                    <div id="cz-control-menu-arrow"></div>
                                    <div id="cz-control-menu-options">
                                        <div class="cz-control-menu-option parent"><a href="main.aspx">Inicio</a></div>
                                        <div class="cz-control-menu-option parent">
                                            <asp:LinkButton ID="opcSalir" runat="server" OnClick="opcSalir_Click">Salir</asp:LinkButton>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="cz-box-body2">

                <div id="cz-box-content">
                    <iframe name="centerFrame" id="centerFrame" width="100%" height="100%" scrolling="yes" frameborder="0"></iframe>
                </div>
            </div>
            <div id="cz-box-footer2">
                Soluciones de datos / Pedido - Reportes
                © Entel Perú. Derechos Reservados.
                Por favor, sirvase leer nuestro convenio para el uso y privacidad del sitio.
            </div>
        </div>
        <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>
        <div id="cz-background"></div>

    </form>
    <div id="calendarwindow" style="z-index: 9000;" class="calendar-window"></div>
</body>
</html>
