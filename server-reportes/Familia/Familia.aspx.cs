﻿ using Controller;
using Model;
using Model.bean;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;

namespace server_reportes.Familia
{
    public partial class Familia : System.Web.UI.Page
    {
        public string fechaInicio;
        public string fechaFin;
        public string idUsuario;
        public string perfil;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                iniciarDatos();
                dibujarGraficos();

            }
            catch (Exception ex)
            {
                string myScript = "parent.document.location.href = '../default.aspx?acc=EXT';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, true);
            }
        }

        private void iniciarDatos()
        {
            idUsuario = Session["lgn_id"].ToString();
            perfil = Session["lgn_perfil"].ToString();

            if (!Page.IsPostBack)
            {
                fechaInicio = Request.QueryString["fechaInicio"];
                fechaFin = Request.QueryString["fechaFin"];
                txtFecInicio.Text = Request.QueryString["fechaInicio"];
                txtFecFin.Text = Request.QueryString["fechaFin"];
            }
        }


        private void dibujarGraficos()
        {
         //     ltrAvance.Text = FamiliaController.dibujarReporteFamiliaGrupo(fechaInicio, fechaFin, idUsuario, perfil);
            StringBuilder builder = new StringBuilder();
            StringBuilder builderScript = new StringBuilder();
          
            List<GrupoBean> list = UsuarioController.obtenerGrupos(idUsuario, perfil);

            for (int i = 0; i < list.Count; i++)
            {
                GrupoBean grupo = list[i];

                List<PieBeanItem> filasM = FamiliaController.obtenerTopFamiliaGrupo(fechaInicio, fechaFin, "M", grupo.id, "S");
                List<PieBeanItem> filasM_d = FamiliaController.obtenerTopFamiliaGrupo(fechaInicio, fechaFin, "M", grupo.id, "D");
                List<PieBeanItem> filasC = FamiliaController.obtenerTopFamiliaGrupo(fechaInicio, fechaFin, "C", grupo.id, string.Empty);
                string nombreDivMonto = "divMonto" + i;
                string radioButtonMonto = "rbMonto" + i;
                string nombreDivCantidad = "divCantidad" + i;
                string radioButtoCantidad = "rbCantidad" + i;
                string nombreDivMonto_d = "divMonto_d" + i;
                string radioButtonMonto_d = "rbMonto_d" + i;

                string strDiv = ReporteController.dibujarReporteFamiliaGrupo("Producto", grupo, filasM, filasC, filasM_d, nombreDivMonto, nombreDivCantidad, nombreDivMonto_d, radioButtonMonto, radioButtoCantidad, radioButtonMonto_d, "Monto total de Venta", "Cantidad de items", i);

               string strScript = ReporteController.dibujarFamiliaGrupoHighcharts(i, filasM, filasC, filasM_d);

                builder.Append(strDiv);
              builderScript.Append(strScript);

            }

            ltrAvance.Text = builder.ToString();
           Page.ClientScript.RegisterStartupScript(GetType(), "close panel", builderScript.ToString(), true);


        }
    }
}