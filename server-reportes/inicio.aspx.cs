﻿using Controller;
using Model.bean;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;

namespace server_reportes
{
    public partial class inicio : Page
    {
        public string fechaInicio;
        public string fechaFin;
        public string idUsuario;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                iniciarDatos();
                //      dibujarGraficos();
                validarMarcaFamiliExiste();
            }
            catch (Exception ex)
            {
                string myScript = "parent.document.location.href = '../default.aspx?acc=EXT';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, true);
            }

            
        }

        private void iniciarDatos()
        {

            idUsuario = Session["lgn_id"].ToString();

            if (!Page.IsPostBack)
            {

                rbVendedor1.SelectedValue = "M";
                rbVendedor2.SelectedValue = "C";
                rbCliente1.SelectedValue = "M";
                rbCliente2.SelectedValue = "C";
                rbProducto1.SelectedValue = "M";
                rbProducto2.SelectedValue = "C";
                rbFamilia1.SelectedValue = "M";
                rbFamilia2.SelectedValue = "C";
                rbFamilia3.SelectedValue = "M";
                rbFamiliaMoneda1.SelectedValue = "S";
                rbFamiliaMoneda2.SelectedValue = "D";
                rbMarca1.SelectedValue = "M";
                rbMarca2.SelectedValue = "C";
                rbMarca3.SelectedValue = "M";
                rbMarcaMoneda1.SelectedValue = "S";
                rbMarcaMoneda2.SelectedValue = "D";
                rbvendedorh1.SelectedValue = "M";
                rbvendedorh2.SelectedValue = "C";
                rbclienteh2.SelectedValue = "C";
                rbclienteh1.SelectedValue = "M";
                rbProductoh1.SelectedValue = "M";
                rbProductoh2.SelectedValue = "C";

                if (Request.QueryString["fechaInicio"] == null)
                {
                    string sFecha = DateTime.Now.ToString("dd/MM/yyyy");
                    txtFecInicio.Text = sFecha;
                    txtFecFin.Text = sFecha;
                    fechaInicio = sFecha;
                    fechaFin = sFecha;
                  
                }
                else
                {
                    txtFecInicio.Text = Request.QueryString["fechaInicio"];
                    txtFecFin.Text = Request.QueryString["fechaFin"];
                    fechaInicio = Request.QueryString["fechaInicio"];
                    fechaFin = Request.QueryString["fechaFin"];

                }
             

            }
        }

        private void dibujarGraficos()
        {

            List<FilaBean> topVendedores1 = VendedorController.obtenerTopVendedores(fechaInicio, fechaFin, "M", idUsuario);
            List<FilaBean> topVendedores2 = VendedorController.obtenerTopVendedores(fechaInicio, fechaFin, "C", idUsuario);

            ltrVendedor1.Text = construirGrilla(topVendedores1, "Vendedor", "Monto");
            ltrVendedor2.Text = construirGrilla(topVendedores2, "Vendedor", "Cantidad");

            List<FilaBean> topClientes1 = ClienteController.obtenerTopClientes(fechaInicio, fechaFin, "M", idUsuario);
            List<FilaBean> topClientes2 = ClienteController.obtenerTopClientes(fechaInicio, fechaFin, "C", idUsuario);
            ltrCliente1.Text = construirGrilla(topClientes1, "Cliente", "Monto");
            ltrCliente2.Text = construirGrilla(topClientes2, "Cliente", "Cantidad");

            List<FilaBean> topProductos1 = ProductoController.obtenerTopProductos(fechaInicio, fechaFin, "M", idUsuario);
            List<FilaBean> topProductos2 = ProductoController.obtenerTopProductos(fechaInicio, fechaFin, "C", idUsuario);

            ltrProducto1.Text = construirGrilla(topProductos1, "Producto", "Monto");
            ltrProducto2.Text = construirGrilla(topProductos2, "Producto", "Cantidad");

            ltPromedio.Text =  PromedioController.obtenerTiempoPromedio(fechaInicio, fechaFin, idUsuario);



        }

        private static string construirGrilla(List<FilaBean> lista, string nombre, string valor)
        {
            StringBuilder sb = new StringBuilder();

            if (lista.Count > 0)
            {


                sb.Append("<table class='grilla table table-bordered table-striped' style='overflow:auto'>");
                sb.Append("<tbody>");
                sb.Append("<tr>");
                sb.Append("<th>" + nombre + "</th>");
                sb.Append("<th>" + valor + "</th>");
                sb.Append("</tr>");

                foreach (FilaBean fila in lista)
                {
                    sb.Append("<tr>");
                    sb.Append("<td align=\"center\">" + fila.nombre + "</td>");
                    sb.Append("<td align=\"center\">" + fila.valor + "</td>");
                    sb.Append("</tr>");
                }

                sb.Append("</tbody>");
                sb.Append("</table>");

            }
            else
            {
                sb.Append("<div class='gridNoData'>" +
                                    "<p style='color:red'>No se encontraron datos para mostrar</p>" +
                                    "</div>");
            }


            return sb.ToString();
        }
        public void validarMarcaFamiliExiste()
        {

            bool familia = FamiliaController.existeFamilia();
            bool marca = MarcaController.existeMarca();
            if (familia == false)
            {
                divFamilia1.Style.Add("display", "none");
                divFamilia2.Style.Add("display", "none");
                divFamilia3.Style.Add("display", "none");
            }
            if (marca == false)
            {
                divMarca1.Style.Add("display", "none");
                divMarca2.Style.Add("display", "none");
                divMarca3.Style.Add("display", "none");
            }

        }


    }


}