﻿using Model.functions;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Model.bean;
using System;
using System.Collections.Generic;

namespace Model
{
    public class VendedorModel
    {
        public static DataTable obtenerTopVendedores(string fechaInicio, string fechaFin, string tipo, string usuario)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@fechaInicio", SqlDbType.VarChar, 100);
            parameter.Value = fechaInicio;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@fechaFin", SqlDbType.VarChar, 100);
            parameter.Value = fechaFin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@tipo", SqlDbType.VarChar, 100);
            parameter.Value = tipo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@usuario", SqlDbType.VarChar, 100);
            parameter.Value = usuario;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spR_RepSelTopVendedor", alParameters);
        }

        public static DataTable obtenerUltimosPedidos(string pk, string fechaFin, string tipo)
        {
            ArrayList alParameters = new ArrayList();

            SqlParameter parameter = new SqlParameter("@fechaFin", SqlDbType.VarChar, 100);
            parameter.Value = fechaFin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@tipo", SqlDbType.VarChar, 100);
            parameter.Value = tipo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@pk", SqlDbType.VarChar, 100);
            parameter.Value = pk;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spR_UltimasVentasxVendedor", alParameters);
        }

        public static DataTable obtenerTopVendedoresGrupo(string fechaInicio, string fechaFin, string tipo, string grupo)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@fechaInicio", SqlDbType.VarChar, 100);
            parameter.Value = fechaInicio;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@fechaFin", SqlDbType.VarChar, 100);
            parameter.Value = fechaFin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@tipo", SqlDbType.VarChar, 100);
            parameter.Value = tipo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@grupo", SqlDbType.VarChar, 100);
            parameter.Value = grupo;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spR_RepSelTopVendedorGrupo", alParameters);
        }

        public static DataTable obtenerVendedoresGrupo(string fechaInicio, string fechaFin, string tipo, string grupo)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@fechaInicio", SqlDbType.VarChar, 100);
            parameter.Value = fechaInicio;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@fechaFin", SqlDbType.VarChar, 100);
            parameter.Value = fechaFin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@tipo", SqlDbType.VarChar, 100);
            parameter.Value = tipo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@grupo", SqlDbType.VarChar, 100);
            parameter.Value = grupo;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spR_RepSelVendedorGrupo", alParameters);
        }
    }
}
