﻿using Model.functions;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace Model
{
    public class UsuarioModel
    {
        public static DataTable validarUsuario(string login, string clave)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@login", SqlDbType.VarChar, 20);
            parameter.Value = login;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@clave", SqlDbType.VarChar, 100);
            parameter.Value = clave;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spR_Login", alParameters);
        }

        public static DataTable obtenerGrupos(string usuario)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@usuario", SqlDbType.VarChar, 20);
            parameter.Value = usuario;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spR_RepSelGrupo", alParameters);
        }
    }
}
