﻿using Model.functions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Model
{
   public class MarcaModel
    {
        public static DataTable obtenerReporteMarca(string fechaInicio, string fechaFin, string tipo, string usuario, string moneda)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@fechaInicio", SqlDbType.VarChar, 100);
            parameter.Value = fechaInicio;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@fechaFin", SqlDbType.VarChar, 100);
            parameter.Value = fechaFin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@usuario", SqlDbType.VarChar, 100);
            parameter.Value = usuario;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@tipo", SqlDbType.VarChar, 100);
            parameter.Value = tipo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@moneda", SqlDbType.VarChar, 100);
            parameter.Value = moneda;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spR_RepSelReporteMarca", alParameters);
        }
        public static DataTable obtenerTopMarcaGrupo(string fechaInicio, string fechaFin, string tipo, string grupo, string moneda)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@fechaInicio", SqlDbType.VarChar, 100);
            parameter.Value = fechaInicio;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@fechaFin", SqlDbType.VarChar, 100);
            parameter.Value = fechaFin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@tipo", SqlDbType.VarChar, 100);
            parameter.Value = tipo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@grupo", SqlDbType.VarChar, 100);
            parameter.Value = grupo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@moneda", SqlDbType.VarChar, 100);
            parameter.Value = moneda;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spR_RepSelTopMarca", alParameters);
        }

        public static DataTable obtenerUltimasVentas(string pk, string fechaInicio, string fechaFin, string tipo, string moneda)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@fechainicio", SqlDbType.VarChar, 100);
            parameter.Value = fechaInicio;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@fechaFin", SqlDbType.VarChar, 100);
            parameter.Value = fechaFin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@tipo", SqlDbType.VarChar, 100);
            parameter.Value = tipo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@moneda", SqlDbType.VarChar, 100);
            parameter.Value = moneda;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@pk", SqlDbType.VarChar, 100);
            parameter.Value = pk;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spR_UltimasVentasxMarca", alParameters);
        }

        public static DataTable obtenerUltimasVentasxGrupo(string pk, string fechaInicio, string fechaFin, string tipo, string grupo, string moneda)
        {
            ArrayList alParameters = new ArrayList();

            SqlParameter parameter = new SqlParameter("@fechainicio", SqlDbType.VarChar, 100);
            parameter.Value = fechaInicio;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@fechaFin", SqlDbType.VarChar, 100);
            parameter.Value = fechaFin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@tipo", SqlDbType.VarChar, 100);
            parameter.Value = tipo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@pk", SqlDbType.VarChar, 100);
            parameter.Value = pk;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@grupo", SqlDbType.VarChar, 100);
            parameter.Value = grupo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@moneda", SqlDbType.VarChar, 100);
            parameter.Value = moneda;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spR_UltimasVentasxMarcaxGrupo", alParameters);
        }

        public static DataTable obtenerMarcaexiste()
        {
            return SqlConnector.getDataTable("spR_ExisteMarca");
        }
    }
}
