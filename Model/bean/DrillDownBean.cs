﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
   public class DrillDownBean
    {
        public string name { get; set; }
        public int y { get; set; }
        public string drilldown { get; set; }

    }
    public class DrillDownBeanSeries
    {
        public string id { get; set; }
        public List<ElementosLista>[] data { get; set; }
    }
    public class ElementosLista
    {
        public string texto { get; set; }
         public int valor { get; set; }
    }


}
