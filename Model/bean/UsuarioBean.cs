﻿namespace Model.bean
{
    public class UsuarioBean
    {
        public string id { get; set; }
        public string codigo { get; set; }
        public string nombre { get; set; }
        public string perfil { get; set; }
    }
}
