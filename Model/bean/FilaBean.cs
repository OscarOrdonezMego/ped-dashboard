﻿using System.Collections.Generic;

namespace Model.bean
{
    public class FilaBean
{
        public List<FilaBeanItem> items { get; set; }
        public string nombre {get; set;}
        public string valor { get; set; }
        public string valor_Soles { get; set; }
        public string valor_Dolares { get; set; }
        public string fecha { get; set; }
        public string valorxfecha { get; set; }
        public string pk { get; set; }
        public List<FilaBean> item { get; set; }
        public List<FilaBeanUltimasTransaccionesFecha> ultimastransacciones { get; set; }
    }

    public class FilaBeanItem
    {
        public string nombre { get; set; }
        public string valor { get; set; }

    }
    public class FilaBeanUltimasTransaccionesFecha
    {
        public string fecha { get; set; }
        public string valorxfecha { get; set; }
        public string valorxfecha_Soles { get; set; }
        public string valorxfecha_Dolares { get; set; }
        public string pk { get; set; }
    }
}

