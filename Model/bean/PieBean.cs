﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
   public class PieBean
    {
        public List<PieBeanItem> items { get; set; }
      
    }

    public class PieBeanItem
    {
        public string name { get; set; }
        public decimal y { get; set; }
        public float montocantidadtotal { get; set; }
        public string pk { get; set; }
        public List<PieBeanUltimasTransaccionesFecha> ultimastransacciones { get; set; }
    }
    public class PieBeanUltimasTransaccionesFecha
    {
        public string nombre { get; set; }
        public string valor { get; set; }
        public string pk { get; set; }
    }
}
