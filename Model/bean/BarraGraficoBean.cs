﻿using System.Collections.Generic;

namespace Model.bean
{
    public class BarraGraficoBean
    {
        public List<BarraGraficoItemBean> items { get; set; }
    }

    public class BarraGraficoItemBean
    {
        public string name { get; set; }
        public decimal y { get; set; }
    }
}
