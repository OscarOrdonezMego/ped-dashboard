﻿using Model.functions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Model
{
    public class ProductoModel
    {
        public static DataTable obtenerTopProductos(string fechaInicio, string fechaFin, string tipo, string usuario)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@fechaInicio", SqlDbType.VarChar, 100);
            parameter.Value = fechaInicio;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@fechaFin", SqlDbType.VarChar, 100);
            parameter.Value = fechaFin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@tipo", SqlDbType.VarChar, 100);
            parameter.Value = tipo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@usuario", SqlDbType.VarChar, 100);
            parameter.Value = usuario;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spR_RepSelTopProducto", alParameters);
        }

        public static DataTable obtenerUltimosProductos(string pk, string fechaFin, string tipo)
        {
            ArrayList alParameters = new ArrayList();

            SqlParameter parameter = new SqlParameter("@fechaFin", SqlDbType.VarChar, 100);
            parameter.Value = fechaFin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@tipo", SqlDbType.VarChar, 100);
            parameter.Value = tipo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@pk", SqlDbType.VarChar, 100);
            parameter.Value = pk;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spR_UltimasVentasxProducto", alParameters);
        }
        public static DataTable obtenerUltimosProductosxGrupo(string pk, string fechaFin, string tipo, string grupo)
        {
            ArrayList alParameters = new ArrayList();

            SqlParameter parameter = new SqlParameter("@fechaFin", SqlDbType.VarChar, 100);
            parameter.Value = fechaFin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@tipo", SqlDbType.VarChar, 100);
            parameter.Value = tipo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@pk", SqlDbType.VarChar, 100);
            parameter.Value = pk;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@grupo", SqlDbType.VarChar, 100);
            parameter.Value = grupo;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("spR_UltimasVentasxProductoxGrupo", alParameters);
        }
        public static DataTable obtenerTopProductosGrupo(string fechaInicio, string fechaFin, string tipo, string grupo)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@fechaInicio", SqlDbType.VarChar, 100);
            parameter.Value = fechaInicio;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@fechaFin", SqlDbType.VarChar, 100);
            parameter.Value = fechaFin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@tipo", SqlDbType.VarChar, 100);
            parameter.Value = tipo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@grupo", SqlDbType.VarChar, 100);
            parameter.Value = grupo;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spR_RepSelTopProductoGrupo", alParameters);
        }
        public static DataTable obtenerTopFamiliaGrupo(string fechaInicio, string fechaFin, string tipo, string grupo, string moneda)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@fechaInicio", SqlDbType.VarChar, 100);
            parameter.Value = fechaInicio;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@fechaFin", SqlDbType.VarChar, 100);
            parameter.Value = fechaFin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@tipo", SqlDbType.VarChar, 100);
            parameter.Value = tipo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@grupo", SqlDbType.VarChar, 100);
            parameter.Value = grupo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@moneda", SqlDbType.VarChar, 100);
            parameter.Value = moneda;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spR_RepSelTopFamilia", alParameters);
        }

        public static DataTable obtenerProductosGrupo(string fechaInicio, string fechaFin, string tipo, string grupo)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@fechaInicio", SqlDbType.VarChar, 100);
            parameter.Value = fechaInicio;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@fechaFin", SqlDbType.VarChar, 100);
            parameter.Value = fechaFin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@tipo", SqlDbType.VarChar, 100);
            parameter.Value = tipo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@grupo", SqlDbType.VarChar, 100);
            parameter.Value = grupo;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spR_RepSelProductoGrupo", alParameters);
        }
    }
}
