﻿using Model.functions;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System;

namespace Model
{
    public class ClienteModel
    {
        public static DataTable obtenerTopClientes(string fechaInicio, string fechaFin, string tipo, string usuario)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@fechaInicio", SqlDbType.VarChar, 100);
            parameter.Value = fechaInicio;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@fechaFin", SqlDbType.VarChar, 100);
            parameter.Value = fechaFin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@tipo", SqlDbType.VarChar, 100);
            parameter.Value = tipo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@usuario", SqlDbType.VarChar, 100);
            parameter.Value = usuario;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spR_RepSelTopCliente", alParameters);
        }

        public static DataTable obtenerUltimosPedidos(string pk, string fechaFin, string tipo)
        {
            ArrayList alParameters = new ArrayList();

            SqlParameter parameter = new SqlParameter("@fechaFin", SqlDbType.VarChar, 100);
            parameter.Value = fechaFin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@tipo", SqlDbType.VarChar, 100);
            parameter.Value = tipo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@pk", SqlDbType.VarChar, 100);
            parameter.Value = pk;
            alParameters.Add(parameter);


            return SqlConnector.getDataTable("spR_UltimasVentasxCliente", alParameters);
        }

        public static DataTable obtenerUltimosPedidosxGrupo(string pk, string fechaFin, string tipo, string grupo)
        {
            ArrayList alParameters = new ArrayList();

            SqlParameter parameter = new SqlParameter("@fechaFin", SqlDbType.VarChar, 100);
            parameter.Value = fechaFin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@tipo", SqlDbType.VarChar, 100);
            parameter.Value = tipo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@pk", SqlDbType.VarChar, 100);
            parameter.Value = pk;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@grupo", SqlDbType.VarChar, 100);
            parameter.Value = grupo;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spR_UltimasVentasxClientexGrupo", alParameters);
        }

        public static DataTable obtenerTopClienteGrupo(string fechaInicio, string fechaFin, string tipo, string grupo)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@fechaInicio", SqlDbType.VarChar, 100);
            parameter.Value = fechaInicio;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@fechaFin", SqlDbType.VarChar, 100);
            parameter.Value = fechaFin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@tipo", SqlDbType.VarChar, 100);
            parameter.Value = tipo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@grupo", SqlDbType.VarChar, 100);
            parameter.Value = grupo;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spR_RepSelTopClienteGrupo", alParameters);
        }

        public static DataTable obtenerClienteGrupo(string fechaInicio, string fechaFin, string tipo, string grupo)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@fechaInicio", SqlDbType.VarChar, 100);
            parameter.Value = fechaInicio;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@fechaFin", SqlDbType.VarChar, 100);
            parameter.Value = fechaFin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@tipo", SqlDbType.VarChar, 100);
            parameter.Value = tipo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@grupo", SqlDbType.VarChar, 100);
            parameter.Value = grupo;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spR_RepSelClienteGrupo", alParameters);
        }
    }
}
