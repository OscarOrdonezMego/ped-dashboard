﻿using Model.functions;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace Model
{
    public class PromedioModel
    {
        public static DataTable obtenerTiempoPromedio(string fechaInicio, string fechaFin, string usuario)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@fechaInicio", SqlDbType.VarChar, 100);
            parameter.Value = fechaInicio;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@fechaFin", SqlDbType.VarChar, 100);
            parameter.Value = fechaFin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@usuario", SqlDbType.VarChar, 100);
            parameter.Value = usuario;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spR_RepSelTiempoPromedio", alParameters);
        }

        public static DataTable obtenerTiempoPromedioGrupo(string fechaInicio, string fechaFin, string grupo)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@fechaInicio", SqlDbType.VarChar, 100);
            parameter.Value = fechaInicio;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@fechaFin", SqlDbType.VarChar, 100);
            parameter.Value = fechaFin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@grupo", SqlDbType.VarChar, 100);
            parameter.Value = grupo;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spR_RepSelTiempoPromedioGrupo", alParameters);
        }

        public static DataTable obtenerTiempoPromedioGrupoXVendedor(string fechaInicio, string fechaFin, string grupo)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@fechaInicio", SqlDbType.VarChar, 100);
            parameter.Value = fechaInicio;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@fechaFin", SqlDbType.VarChar, 100);
            parameter.Value = fechaFin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@grupo", SqlDbType.VarChar, 100);
            parameter.Value = grupo;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spR_RepSelTiempoPromedioGrupoXVendedor", alParameters);
        }

    }
}
